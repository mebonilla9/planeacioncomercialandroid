package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppMenu implements Serializable {

    private Long idMenu;
    private String nombreMenu;
    private String estado;
    private Long menuPadre;
    private List<AppMenu> subMenu;

    public AppMenu() {
    }

    public AppMenu(Long idMenu) {
        this.idMenu = idMenu;
    }

    /**
     * @return the idMenu
     */
    public Long getIdMenu() {
        return idMenu;
    }

    /**
     * @param idMenu the idMenu to set
     */
    public void setIdMenu(Long idMenu) {
        this.idMenu = idMenu;
    }

    /**
     * @return the nombreMenu
     */
    public String getNombreMenu() {
        return nombreMenu;
    }

    /**
     * @param nombreMenu the nombreMenu to set
     */
    public void setNombreMenu(String nombreMenu) {
        this.nombreMenu = nombreMenu;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the menuPadre
     */
    public Long getMenuPadre() {
        return menuPadre;
    }

    /**
     * @param menuPadre the menuPadre to set
     */
    public void setMenuPadre(Long menuPadre) {
        this.menuPadre = menuPadre;
    }

    /**
     * @return the subMenu
     */
    public List<AppMenu> getSubMenu() {
        return subMenu;
    }

    /**
     * @param subMenu the subMenu to set
     */
    public void setSubMenu(List<AppMenu> subMenu) {
        this.subMenu = subMenu;
    }

}
