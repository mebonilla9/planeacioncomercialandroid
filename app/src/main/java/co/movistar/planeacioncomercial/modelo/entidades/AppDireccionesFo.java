package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

public class AppDireccionesFo implements Serializable {
    private String complemento;
    private String conjunto;
    private String direccionCompleta;
    private Long idDireccion;
    private String municipio;
    private String tipo;

    public Long getIdDireccion() {
        return this.idDireccion;
    }

    public void setIdDireccion(Long idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getConjunto() {
        return this.conjunto;
    }

    public void setConjunto(String conjunto) {
        this.conjunto = conjunto;
    }

    public String getComplemento() {
        return this.complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getDireccionCompleta() {
        return this.direccionCompleta;
    }

    public void setDireccionCompleta(String direccionCompleta) {
        this.direccionCompleta = direccionCompleta;
    }
}
