/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.entidades.AppAltas;

/**
 *
 * @author Lord_Nightmare
 */
public class GeneracionAltasDTO {
    
    private AppAltas appAltas;
    private Long meta;
    private Double cumplimiento;

    /**
     * @return the appAltas
     */
    public AppAltas getAppAltas() {
        return appAltas;
    }

    /**
     * @param appAltas the appAltas to set
     */
    public void setAppAltas(AppAltas appAltas) {
        this.appAltas = appAltas;
    }

    /**
     * @return the meta
     */
    public Long getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(Long meta) {
        this.meta = meta;
    }

    /**
     * @return the cumplimientoProyectado
     */
    public Double getCumplimiento() {
        return cumplimiento;
    }

    /**
     * @param cumplimiento the cumplimientoProyectado to set
     */
    public void setCumplimiento(Double cumplimiento) {
        this.cumplimiento = cumplimiento;
    }
    
    
}
