package co.movistar.planeacioncomercial.modelo.servicios.call;

import co.movistar.planeacioncomercial.modelo.entidades.AppNotificacion;
import co.movistar.planeacioncomercial.modelo.entidades.AppSegmentacion;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by lord_nightmare on 14/08/17.
 */

public interface AppSegmentacionCall {

    @GET("consultanit/consultar/{nit}/{idAgente}")
    Call<AppSegmentacion> consultaInformacion(@Path("nit") String nit, @Path("idAgente") String idAgente, @Header("Authorization") String token);
}
