package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppAltas;
import co.movistar.planeacioncomercial.modelo.entidades.AppProyeccion;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppProyeccionCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by lord_nightmare on 3/05/17.
 */

public class AppProyeccionService extends GenericoService {

    private AppProyeccionCall service;

    public AppProyeccionService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppProyeccionCall.class);
    }

    public void listaProyeccionProductos(String producto, Callback<List<AppProyeccion>> callback){
        Call<List<AppProyeccion>> respuesta = this.service.listaProyeccionProductos(producto, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }


    public void listaProductosDisponibles(Callback<List<String>> callback){
        Call<List<String>> respuesta = this.service.listaProductosDisponibles(PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }
}
