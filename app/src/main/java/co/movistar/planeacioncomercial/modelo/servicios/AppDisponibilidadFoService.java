package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidadFo;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppDisponibilidadFoCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 26/02/18.
 */

public class AppDisponibilidadFoService extends GenericoService {

    private AppDisponibilidadFoCall service;

    public AppDisponibilidadFoService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppDisponibilidadFoCall.class);
    }

    public void consultarPorDireccion(AppDisponibilidadFo disFibra, Callback<AppDisponibilidadFo> callback) {
        Call<AppDisponibilidadFo> respuesta = this.service.consultarPorDireccion(disFibra, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarNoParametrizada(AppDisponibilidadFo disFibra, Callback<AppDisponibilidadFo> callback) {
        Call<AppDisponibilidadFo> respuesta = this.service.consultarNoParametrizada(disFibra, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarDireccionesNoParametrizada(AppDisponibilidadFo disFibra, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarDireccionesNoParametrizada(disFibra, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarRegionales(Boolean param, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarRegionales(param, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarDepartamentos(String regional, Boolean param, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarDepartamentos(regional, param, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarLocalidades(String departamento, Boolean param, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarLocalidades(departamento, param, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarBarrios(String localidad, Boolean param, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarBarrios(localidad, param, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarPrincipal(String localidad, String barrio, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarPrincipal(localidad, barrio, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarReferenciaPrincipal(String localidad, String barrio, String principal, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarReferenciaPrincipal(localidad, barrio, principal, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarCruce(String localidad, String barrio, String principal, String referenciaPrincipal, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarCruce(localidad, barrio, principal, referenciaPrincipal, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarReferenciaCruce(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarReferenciaCruce(localidad, barrio, principal, referenciaPrincipal, cruce, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarPlaca(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarPlaca(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarComplemento(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce, String placa, Callback<List<AppDisponibilidadFo>> callback) {
        Call<List<AppDisponibilidadFo>> respuesta = this.service.consultarComplemento(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce, placa, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }
}
