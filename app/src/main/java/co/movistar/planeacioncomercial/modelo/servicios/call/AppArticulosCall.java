package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppArticulos;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public interface AppArticulosCall {

    @GET("articulos/consultar/{id}")
    Call<List<AppArticulos>> obtenerArticulosPunto(@Path("id") String id, @Header("Authorization") String token);
}
