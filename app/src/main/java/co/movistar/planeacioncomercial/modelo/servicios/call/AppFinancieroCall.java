package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.dto.InformacionComisionesDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by lord_nightmare on 20/10/17.
 */

public interface AppFinancieroCall {

    @GET("financiero/periodo")
    Call<List<String>> consultarPeriodos(@Header("Authorization") String token);

    @GET("financiero/productos/{periodo}")
    Call<List<String>> listaProductosDisponibles(@Path("periodo") String periodo, @Header("Authorization") String token);

    @GET("financiero/consultar/{producto}/{periodo}")
    Call<InformacionComisionesDTO> listaCostoxAltaProductos(@Path("periodo") String periodo, @Path("producto") String producto, @Header("Authorization") String token);
}
