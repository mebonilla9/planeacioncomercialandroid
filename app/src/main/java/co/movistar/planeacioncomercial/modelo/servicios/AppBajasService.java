package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppBajas;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppBajasCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 13/12/17.
 */
public class AppBajasService extends GenericoService {

    private AppBajasCall service;

    public AppBajasService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppBajasCall.class);
    }

    public void consultarProductos(Callback<List<AppBajas>> callback) {
        Call<List<AppBajas>> respuesta = this.service.consultarProductos(PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }

    public void consultarRegionalCanal(Callback<List<AppBajas>> callback) {
        Call<List<AppBajas>> respuesta = this.service.consultarRegionalCanal(PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }

    public void consultarBajas(String producto, String regionalCanal, Callback<List<AppBajas>> callback) {
        Call<List<AppBajas>> respuesta = this.service.consultarBajas(producto, regionalCanal, PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }
}
