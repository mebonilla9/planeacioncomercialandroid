package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppMenuCargo implements Serializable {

    private Long idMenuCargo;
    private AppCargo cargo;
    private AppMenu menu;

    public AppMenuCargo() {
    }

    public AppMenuCargo(Long idMenuCargo) {
        this.idMenuCargo = idMenuCargo;
    }

    /**
     * @return the idMenuCargo
     */
    public Long getIdMenuCargo() {
        return idMenuCargo;
    }

    /**
     * @param idMenuCargo the idMenuCargo to set
     */
    public void setIdMenuCargo(Long idMenuCargo) {
        this.idMenuCargo = idMenuCargo;
    }

    /**
     * @return the cargo
     */
    public AppCargo getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(AppCargo cargo) {
        this.cargo = cargo;
    }

    /**
     * @return the menu
     */
    public AppMenu getMenu() {
        return menu;
    }

    /**
     * @param menu the menu to set
     */
    public void setMenu(AppMenu menu) {
        this.menu = menu;
    }
    
}
