package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppUsuarios;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 18/11/16.
 */

public interface AppUsuariosCall {

    @POST("usuario/reestablecer")
    Call<ResponseBody> reestablecerContrasena(@Body String content, @Header("Authorization") String token);

    @GET("usuario/subordinados")
    Call<List<AppUsuarios>> consultarSubordinados(@Header("Authorization") String token);

    @GET("usuario/reestablecer/{correo}")
    Call<ResponseBody> reestablecerContrasenaCorreo(@Path("correo") String correo);

}
