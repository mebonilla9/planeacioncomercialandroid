package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import co.movistar.planeacioncomercial.negocio.interceptor.ConnectivityInterceptor;
import co.movistar.planeacioncomercial.negocio.utilidades.DotNetDateConverter;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public class GenericoService {

    private final static String BASE_URL = "http://186.116.14.117:8080/planeacioncomercial/rest/";
    //private final static String BASE_URL = "http://192.168.1.105:8080/planeacioncomercial/rest/";
    //private final static String BASE_URL = "https://planeacioncomercial.co:8080/planeacioncomercial/rest/";


    protected final Context contexto;
    protected final Retrofit retrofit;

    public GenericoService(Context contexto) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DotNetDateConverter());
        this.contexto = contexto;
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(new OkHttpClient.Builder().addInterceptor(new ConnectivityInterceptor(contexto)).readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS).build())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build();
    }
}
