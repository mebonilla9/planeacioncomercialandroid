package co.movistar.planeacioncomercial.modelo.servicios.call;

import co.movistar.planeacioncomercial.modelo.entidades.AppLlaves;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;

/**
 * Created by Lord_Nightmare on 28/12/16.
 */

public interface AppLlavesCall {

    @PUT("llaves/insertar")
    Call<ResponseBody> insertarLlaves(@Body AppLlaves content);
}
