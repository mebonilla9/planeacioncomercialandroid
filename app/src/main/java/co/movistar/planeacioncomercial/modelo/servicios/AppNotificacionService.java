package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import co.movistar.planeacioncomercial.modelo.entidades.AppNotificacion;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppNotificacionCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 12/07/17.
 */

public class AppNotificacionService extends GenericoService {

    private AppNotificacionCall service;

    public AppNotificacionService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppNotificacionCall.class);
    }

    public void consultarUltimaNotificacion(Callback<AppNotificacion> callback){
        Call<AppNotificacion> respuesta = this.service.consultarUltimaNotificacion(PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

}
