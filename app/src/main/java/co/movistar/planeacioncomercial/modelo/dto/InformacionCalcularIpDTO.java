/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import java.io.Serializable;
import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppCabecera;
import co.movistar.planeacioncomercial.modelo.entidades.AppGestionado;
import co.movistar.planeacioncomercial.modelo.entidades.AppNoGestionado;

/**
 *
 * @author Lord_Nightmare
 */
public class InformacionCalcularIpDTO implements Serializable{
    
    private AppCabecera appCabecera;
    private List<AppGestionado> listaAppGestionado;
    private List<AppNoGestionado> listaAppNoGestionado;

    /**
     * @return the appCabecera
     */
    public AppCabecera getAppCabecera() {
        return appCabecera;
    }

    /**
     * @param appCabecera the appCabecera to set
     */
    public void setAppCabecera(AppCabecera appCabecera) {
        this.appCabecera = appCabecera;
    }

    /**
     * @return the listaAppGestionado
     */
    public List<AppGestionado> getListaAppGestionado() {
        return listaAppGestionado;
    }

    /**
     * @param listaAppGestionado the listaAppGestionado to set
     */
    public void setListaAppGestionado(List<AppGestionado> listaAppGestionado) {
        this.listaAppGestionado = listaAppGestionado;
    }

    /**
     * @return the listaAppNoGestionado
     */
    public List<AppNoGestionado> getListaAppNoGestionado() {
        return listaAppNoGestionado;
    }

    /**
     * @param listaAppNoGestionado the listaAppNoGestionado to set
     */
    public void setListaAppNoGestionado(List<AppNoGestionado> listaAppNoGestionado) {
        this.listaAppNoGestionado = listaAppNoGestionado;
    }
    
    
    
}
