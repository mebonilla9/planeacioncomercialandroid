package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppBajas;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface AppBajasCall {
    @GET("bajas/consultar/{producto}/{regionalcanal}")
    Call<List<AppBajas>> consultarBajas(@Path("producto") String producto, @Path("regionalcanal") String regionalCanal, @Header("Authorization") String token);

    @GET("bajas/producto")
    Call<List<AppBajas>> consultarProductos(@Header("Authorization") String token);

    @GET("bajas/regionalcanal")
    Call<List<AppBajas>> consultarRegionalCanal(@Header("Authorization") String token);
}
