package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppLiquidacionHist;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 27/02/17.
 */

public interface AppLiquidacionHistCall {

    @GET("liquidacion/resumen/{periodo}")
    Call<AppLiquidacionHist> obtenerResumenLiquidacion(@Path("periodo") String periodo, @Header("Authorization") String token);

    @GET("liquidacion/categoria")
    Call<List<AppLiquidacionHist>> consultarCategoria(@Header("Authorization") String token);

    @GET("liquidacion/periodo")
    Call<List<AppLiquidacionHist>> consultarPeriodos(@Header("Authorization") String token);

    @GET("liquidacion/detalle/{categoria}/{periodo}")
    Call<List<AppLiquidacionHist>> consultarDetalleLiquidacion(@Header("Authorization") String token, @Path("categoria") String categoria, @Path("periodo") String periodo);
}
