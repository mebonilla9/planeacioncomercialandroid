package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppServicio;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by lord_nightmare on 19/04/17.
 */

public interface AppServicioCall {

    @GET("servicio/consultar")
    Call<List<AppServicio>> consultarServicios(@Header("Authorization") String token);
}
