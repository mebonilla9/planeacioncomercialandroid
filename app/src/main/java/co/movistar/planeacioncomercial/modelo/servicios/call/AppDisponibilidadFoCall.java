package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidadFo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 26/02/18.
 */

public interface AppDisponibilidadFoCall {

    @POST("disponibilidadfibra/consultar")
    Call<AppDisponibilidadFo> consultarPorDireccion(@Body AppDisponibilidadFo disFibra, @Header("Authorization") String token);

    @POST("disponibilidadfibra/direccion/noparam")
    Call<List<AppDisponibilidadFo>> consultarDireccionesNoParametrizada(@Body AppDisponibilidadFo disFibra, @Header("Authorization") String token);

    @POST("disponibilidadfibra/consultar/noparam")
    Call<AppDisponibilidadFo> consultarNoParametrizada(@Body AppDisponibilidadFo disFibra, @Header("Authorization") String token);

    @GET("disponibilidadfibra/regional/{param}")
    Call<List<AppDisponibilidadFo>> consultarRegionales(@Path("param") Boolean param, @Header("Authorization") String token);

    @GET("disponibilidadfibra/departamento/{regional}/{param}")
    Call<List<AppDisponibilidadFo>> consultarDepartamentos(@Path("regional") String regional, @Path("param") Boolean param,  @Header("Authorization") String token);

    @GET("disponibilidadfibra/localidad/{departamento}/{param}")
    Call<List<AppDisponibilidadFo>> consultarLocalidades(@Path("departamento") String departamento, @Path("param") Boolean param,  @Header("Authorization") String token);

    @GET("disponibilidadfibra/barrio/{localidad}/{param}")
    Call<List<AppDisponibilidadFo>> consultarBarrios(@Path("localidad") String localidad, @Path("param") Boolean param,  @Header("Authorization") String token);

    @GET("disponibilidadfibra/principal/{localidad}/{barrio}")
    Call<List<AppDisponibilidadFo>> consultarPrincipal(@Path("localidad") String localidad, @Path("barrio") String barrio, @Header("Authorization") String token);

    @GET("disponibilidadfibra/principal/{localidad}/{barrio}/{principal}")
    Call<List<AppDisponibilidadFo>> consultarReferenciaPrincipal(@Path("localidad") String localidad, @Path("barrio") String barrio, @Path("principal") String principal, @Header("Authorization") String token);

    @GET("disponibilidadfibra/principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}")
    Call<List<AppDisponibilidadFo>> consultarCruce(@Path("localidad") String localidad, @Path("barrio") String barrio, @Path("principal") String principal, @Path("referenciaPrincipal") String referenciaPrincipal, @Header("Authorization") String token);

    @GET("disponibilidadfibra/principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}/{cruce}")
    Call<List<AppDisponibilidadFo>> consultarReferenciaCruce(@Path("localidad") String localidad, @Path("barrio") String barrio, @Path("principal") String principal, @Path("referenciaPrincipal") String referenciaPrincipal, @Path("cruce") String cruce, @Header("Authorization") String token);

    @GET("disponibilidadfibra/principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}/{cruce}/{referenciaCruce}")
    Call<List<AppDisponibilidadFo>> consultarPlaca(@Path("localidad") String localidad, @Path("barrio") String barrio, @Path("principal") String principal, @Path("referenciaPrincipal") String referenciaPrincipal, @Path("cruce") String cruce, @Path("referenciaCruce") String referenciaCruce, @Header("Authorization") String token);

    @GET("disponibilidadfibra/principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}/{cruce}/{referenciaCruce}/{placa}")
    Call<List<AppDisponibilidadFo>> consultarComplemento(@Path("localidad") String localidad, @Path("barrio") String barrio, @Path("principal") String principal, @Path("referenciaPrincipal") String referenciaPrincipal, @Path("cruce") String cruce, @Path("referenciaCruce") String referenciaCruce, @Path("placa") String placa, @Header("Authorization") String token);

}