package co.movistar.planeacioncomercial.modelo.dto;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppPorcentajeCorte;

/**
 * Created by Lord_Nightmare on 18/11/16.
 */

public class InformacionAltasGeneralDTO {

    private AppPorcentajeCorte porcentajeCorte;
    private List<List<GeneracionAltasDTO>> listaAltas;

    /**
     * @return the porcentajeCorte
     */
    public AppPorcentajeCorte getPorcentajeCorte() {
        return porcentajeCorte;
    }

    /**
     * @param porcentajeCorte the porcentajeCorte to set
     */
    public void setPorcentajeCorte(AppPorcentajeCorte porcentajeCorte) {
        this.porcentajeCorte = porcentajeCorte;
    }

    /**
     * @return the listaAltas
     */
    public List<List<GeneracionAltasDTO>> getListaAltas() {
        return listaAltas;
    }

    /**
     * @param listaAltas the listaAltas to set
     */
    public void setListaAltas(List<List<GeneracionAltasDTO>> listaAltas) {
        this.listaAltas = listaAltas;
    }
}
