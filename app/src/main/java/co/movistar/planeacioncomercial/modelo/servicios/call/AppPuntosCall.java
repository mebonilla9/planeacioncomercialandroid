package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppPuntos;
import co.movistar.planeacioncomercial.modelo.entidades.AppUsuarios;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public interface AppPuntosCall {

    @GET("puntos/consultar/{id}")
    Call<List<AppPuntos>> obtenerPuntosPorUsuario(@Path("id") String id, @Header("Authorization") String token);

    @GET("puntos/subordinados")
    Call<List<AppUsuarios>> consultarSubordinados(@Header("Authorization") String token);

}
