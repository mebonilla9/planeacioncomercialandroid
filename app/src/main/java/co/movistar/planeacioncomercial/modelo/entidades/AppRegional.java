package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppRegional implements Serializable {

    private Long idRegional;
    private String nombreRegional;
    private AppEstados estado;

    public AppRegional() {
    }

    public AppRegional(Long idRegional) {
        this.idRegional = idRegional;
    }

    /**
     * @return the idRegional
     */
    public Long getIdRegional() {
        return idRegional;
    }

    /**
     * @param idRegional the idRegional to set
     */
    public void setIdRegional(Long idRegional) {
        this.idRegional = idRegional;
    }

    /**
     * @return the nombreRegional
     */
    public String getNombreRegional() {
        return nombreRegional;
    }

    /**
     * @param nombreRegional the nombreRegional to set
     */
    public void setNombreRegional(String nombreRegional) {
        this.nombreRegional = nombreRegional;
    }

    /**
     * @return the estado
     */
    public AppEstados getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(AppEstados estado) {
        this.estado = estado;
    }
    
}
