package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.dto.DireccionDTO;
import co.movistar.planeacioncomercial.modelo.dto.InfoDisponibilidadDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppDisponibilidadCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 5/12/16.
 */

public class AppDisponibilidadService extends GenericoService {

    private AppDisponibilidadCall service;

    public AppDisponibilidadService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppDisponibilidadCall.class);
    }

    public void consultarInformacionDisponibilidad(String content, Callback<InfoDisponibilidadDTO> callback){
        Call<InfoDisponibilidadDTO> respuesta = this.service.consultarInformacionDisponibilidad(content, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarInformacionDepartamentos(Callback<List<AppDisponibilidad>> callback){
        Call<List<AppDisponibilidad>> respuesta = this.service.consultarInformacionDepartamentos(PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarInformacionLocalidades(String departamento, Callback<List<AppDisponibilidad>> callback){
        Call<List<AppDisponibilidad>> respuesta = this.service.consultarInformacionLocalidades(departamento, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarInformacionDistritos(String departamento, String localidad, Callback<List<AppDisponibilidad>> callback){
        Call<List<AppDisponibilidad>> respuesta = this.service.consultarInformacionDistritos(departamento, localidad, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarInformacionArmarios(String distrito, Callback<List<AppDisponibilidad>> callback){
        Call<List<AppDisponibilidad>> respuesta = this.service.consultarInformacionArmarios(distrito, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarInformacionCajas(String distrito, String armario, Callback<List<AppDisponibilidad>> callback){
        Call<List<AppDisponibilidad>> respuesta = this.service.consultarInformacionCajas(distrito, armario, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarInformacionDirecciones(DireccionDTO direccion, Callback<InfoDisponibilidadDTO> callback){
        Call<InfoDisponibilidadDTO> respuesta = this.service.consultarInformacionDirecciones(direccion, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarInformacionLocalidadesCoordenadas(String departamento, Callback<List<AppDisponibilidad>> callback){
        Call<List<AppDisponibilidad>> respuesta = this.service.consultarInformacionLocalidadesCoordenadas(departamento, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }
}
