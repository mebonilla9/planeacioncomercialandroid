package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppPuntos;
import co.movistar.planeacioncomercial.modelo.entidades.AppUsuarios;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppPuntosCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public class AppPuntosService extends GenericoService {

    private AppPuntosCall service;

    public AppPuntosService(Context contexto) {
        super(contexto);
        service = this.retrofit.create(AppPuntosCall.class);
    }

    public void obtenerPuntosPorUsuario(String id, Callback<List<AppPuntos>> callback){
        Call<List<AppPuntos>> respuesta = this.service.obtenerPuntosPorUsuario(id, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void consultarSubordinados(Callback<List<AppUsuarios>> callback){
        Call<List<AppUsuarios>> respuesta = this.service.consultarSubordinados(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }
}
