package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public class AppPuntos implements Serializable {

    private String idPunto;
    private String nombrePunto;
    private String direccion;
    private String ciudad;
    private String departamento;
    private String regional;
    private Long idUsuario;
    private Long idCoordinador;

    public AppPuntos() {
    }

    /**
     * @return the idPunto
     */
    public String getIdPunto() {
        return idPunto;
    }

    /**
     * @param idPunto the idPunto to set
     */
    public void setIdPunto(String idPunto) {
        this.idPunto = idPunto;
    }

    /**
     * @return the nombrePunto
     */
    public String getNombrePunto() {
        return nombrePunto;
    }

    /**
     * @param nombrePunto the nombrePunto to set
     */
    public void setNombrePunto(String nombrePunto) {
        this.nombrePunto = nombrePunto;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the regional
     */
    public String getRegional() {
        return regional;
    }

    /**
     * @param regional the regional to set
     */
    public void setRegional(String regional) {
        this.regional = regional;
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the idCoordinador
     */
    public Long getIdCoordinador() {
        return idCoordinador;
    }

    /**
     * @param idCoordinador the idCoordinador to set
     */
    public void setIdCoordinador(Long idCoordinador) {
        this.idCoordinador = idCoordinador;
    }

    @Override
    public String toString() {
        return getNombrePunto();
    }
}
