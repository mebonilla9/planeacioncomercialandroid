package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppAltas implements Serializable {

    private Long idAltas;
    private AppUsuarios usuario;
    private String nomUsuario;
    private String producto;
    private String regional;
    private String jefatura;
    private String canal;
    private String departamento;
    private String segmento;
    private Long presupuesto;
    private Long altas;
    private Double proyeccion;

    public AppAltas() {
    }

    /**
     * @return the idAltas
     */
    public Long getIdAltas() {
        return idAltas;
    }

    /**
     * @param idAltas the idAltas to set
     */
    public void setIdAltas(Long idAltas) {
        this.idAltas = idAltas;
    }

    /**
     * @return the idUsuario
     */
    public AppUsuarios getUsuario() {
        return usuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setUsuario(AppUsuarios usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the nomUsuario
     */
    public String getNomUsuario() {
        return nomUsuario;
    }

    /**
     * @param nomUsuario the nomUsuario to set
     */
    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    /**
     * @return the producto
     */
    public String getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(String producto) {
        this.producto = producto;
    }

    /**
     * @return the regional
     */
    public String getRegional() {
        return regional;
    }

    /**
     * @param regional the regional to set
     */
    public void setRegional(String regional) {
        this.regional = regional;
    }

    /**
     * @return the jefatura
     */
    public String getJefatura() {
        return jefatura;
    }

    /**
     * @param jefatura the jefatura to set
     */
    public void setJefatura(String jefatura) {
        this.jefatura = jefatura;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the segmento
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * @param segmento the segmento to set
     */
    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    /**
     * @return the presupuesto
     */
    public Long getPresupuesto() {
        return presupuesto;
    }

    /**
     * @param presupuesto the presupuesto to set
     */
    public void setPresupuesto(Long presupuesto) {
        this.presupuesto = presupuesto;
    }

    /**
     * @return the altas
     */
    public Long getAltas() {
        return altas;
    }

    /**
     * @param altas the altas to set
     */
    public void setAltas(Long altas) {
        this.altas = altas;
    }

    /**
     * @return the proyeccion
     */
    public Double getProyeccion() {
        return proyeccion;
    }

    /**
     * @param proyeccion the proyeccion to set
     */
    public void setProyeccion(Double proyeccion) {
        this.proyeccion = proyeccion;
    }

}
