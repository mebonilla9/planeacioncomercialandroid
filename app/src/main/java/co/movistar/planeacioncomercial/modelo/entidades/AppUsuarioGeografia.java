package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuarioGeografia implements Serializable {

    private Long idUsuarioGeografia;
    private AppCanales canal;
    private AppJefatura jefatura;
    private AppRegional regional;
    private AppUsuarios usuario;

    public AppUsuarioGeografia() {
    }

    public AppUsuarioGeografia(Long idUsuarioGeografia) {
        this.idUsuarioGeografia = idUsuarioGeografia;
    }

    /**
     * @return the idUsuarioGeografia
     */
    public Long getIdUsuarioGeografia() {
        return idUsuarioGeografia;
    }

    /**
     * @param idUsuarioGeografia the idUsuarioGeografia to set
     */
    public void setIdUsuarioGeografia(Long idUsuarioGeografia) {
        this.idUsuarioGeografia = idUsuarioGeografia;
    }

    /**
     * @return the canal
     */
    public AppCanales getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(AppCanales canal) {
        this.canal = canal;
    }

    /**
     * @return the jefatura
     */
    public AppJefatura getJefatura() {
        return jefatura;
    }

    /**
     * @param jefatura the jefatura to set
     */
    public void setJefatura(AppJefatura jefatura) {
        this.jefatura = jefatura;
    }

    /**
     * @return the regional
     */
    public AppRegional getRegional() {
        return regional;
    }

    /**
     * @param regional the regional to set
     */
    public void setRegional(AppRegional regional) {
        this.regional = regional;
    }

    /**
     * @return the usuario
     */
    public AppUsuarios getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(AppUsuarios usuario) {
        this.usuario = usuario;
    }
    
}
