package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import co.movistar.planeacioncomercial.modelo.entidades.AppCanales;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppCanalesCall;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public class AppCanalesService extends GenericoService {

    private AppCanalesCall service;

    public AppCanalesService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppCanalesCall.class);
    }

    public void insertarCanales(AppCanales canal){
        Call<ResponseBody> respuesta = this.service.insertarCanales(canal, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AlertaUtil.mostrarAlerta("Registro de canal",response.body().toString(),null,null,contexto);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AlertaUtil.mostrarAlerta("Registro de canal",t.getMessage(),null,null,contexto);
            }
        });
    }
}
