package co.movistar.planeacioncomercial.modelo.entidades;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by lord_nightmare on 14/08/17.
 */

public class AppSegmentacion implements Serializable {

    private Long idNit;
    private String nit;
    private String nombreCliente;
    private String segmento;
    private String tipo;
    private Date ingreso;
    private String conMarco;
    private Long indicador;

    public AppSegmentacion() {
    }

    /**
     * @return the idNit
     */
    public Long getIdNit() {
        return idNit;
    }

    /**
     * @param idNit the idNit to set
     */
    public void setIdNit(Long idNit) {
        this.idNit = idNit;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the nombreCliente
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * @param nombreCliente the nombreCliente to set
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * @return the segmento
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * @param segmento the segmento to set
     */
    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the ingreso
     */
    public Date getIngreso() {
        return ingreso;
    }

    /**
     * @param ingreso the ingreso to set
     */
    public void setIngreso(Date ingreso) {
        this.ingreso = ingreso;
    }

    /**
     * @return the conMarco
     */
    public String getConMarco() {
        return conMarco;
    }

    /**
     * @param conMarco the conMarco to set
     */
    public void setConMarco(String conMarco) {
        this.conMarco = conMarco;
    }

    /**
     * @return the indicador
     */
    public Long getIndicador() {
        return indicador;
    }

    /**
     * @param indicador the indicador to set
     */
    public void setIndicador(Long indicador) {
        this.indicador = indicador;
    }

}
