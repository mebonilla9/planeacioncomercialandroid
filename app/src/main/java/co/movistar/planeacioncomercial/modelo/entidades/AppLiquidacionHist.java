package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 * Created by Lord_Nightmare on 27/02/17.
 */

public class AppLiquidacionHist implements Serializable {

    private Long idConsLiquidacion;
    private String periodo;
    private Long cedula;
    private String variable;
    private Long meta;
    private Double ejecutado;
    private Double cumplimiento;
    private Double pesoVariable;
    private Double pesoCat;
    private Double pesoGestion;
    private Double comIP;
    private Double ipEjecucionSinAcel;
    private Double ipLiquidar;
    private Double totalAcel;
    private Double curvaAcelerador;
    private Double variable100;
    private Double valorMes;
    private Double ajusteMesesAnteriores;
    private Double garantizado;
    private Double garantizadoNvosCanales;
    private String acelTeUFVDPrepago;
    private String acelPortacionesFVDPrepago;
    private Double valorAPagar;
    private String categoria;
    private Long idUsuario;

    public AppLiquidacionHist() {
    }

    /**
     * @return the idConsLiquidacion
     */
    public Long getIdConsLiquidacion() {
        return idConsLiquidacion;
    }

    /**
     * @param idConsLiquidacion the idConsLiquidacion to set
     */
    public void setIdConsLiquidacion(Long idConsLiquidacion) {
        this.idConsLiquidacion = idConsLiquidacion;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the cedula
     */
    public Long getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the variable
     */
    public String getVariable() {
        return variable;
    }

    /**
     * @param variable the variable to set
     */
    public void setVariable(String variable) {
        this.variable = variable;
    }

    /**
     * @return the meta
     */
    public Long getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(Long meta) {
        this.meta = meta;
    }

    /**
     * @return the ejecutado
     */
    public Double getEjecutado() {
        return ejecutado;
    }

    /**
     * @param ejecutado the ejecutado to set
     */
    public void setEjecutado(Double ejecutado) {
        this.ejecutado = ejecutado;
    }

    /**
     * @return the cumplimiento
     */
    public Double getCumplimiento() {
        return cumplimiento;
    }

    /**
     * @param cumplimiento the cumplimiento to set
     */
    public void setCumplimiento(Double cumplimiento) {
        this.cumplimiento = cumplimiento;
    }

    /**
     * @return the pesoVariable
     */
    public Double getPesoVariable() {
        return pesoVariable;
    }

    /**
     * @param pesoVariable the pesoVariable to set
     */
    public void setPesoVariable(Double pesoVariable) {
        this.pesoVariable = pesoVariable;
    }

    /**
     * @return the pesoCat
     */
    public Double getPesoCat() {
        return pesoCat;
    }

    /**
     * @param pesoCat the pesoCat to set
     */
    public void setPesoCat(Double pesoCat) {
        this.pesoCat = pesoCat;
    }

    /**
     * @return the pesoGestion
     */
    public Double getPesoGestion() {
        return pesoGestion;
    }

    /**
     * @param pesoGestion the pesoGestion to set
     */
    public void setPesoGestion(Double pesoGestion) {
        this.pesoGestion = pesoGestion;
    }

    /**
     * @return the comIP
     */
    public Double getComIP() {
        return comIP;
    }

    /**
     * @param comIP the comIP to set
     */
    public void setComIP(Double comIP) {
        this.comIP = comIP;
    }

    /**
     * @return the ipEjecucionSinAcel
     */
    public Double getIpEjecucionSinAcel() {
        return ipEjecucionSinAcel;
    }

    /**
     * @param ipEjecucionSinAcel the ipEjecucionSinAcel to set
     */
    public void setIpEjecucionSinAcel(Double ipEjecucionSinAcel) {
        this.ipEjecucionSinAcel = ipEjecucionSinAcel;
    }

    /**
     * @return the ipLiquidar
     */
    public Double getIpLiquidar() {
        return ipLiquidar;
    }

    /**
     * @param ipLiquidar the ipLiquidar to set
     */
    public void setIpLiquidar(Double ipLiquidar) {
        this.ipLiquidar = ipLiquidar;
    }

    /**
     * @return the totalAcel
     */
    public Double getTotalAcel() {
        return totalAcel;
    }

    /**
     * @param totalAcel the totalAcel to set
     */
    public void setTotalAcel(Double totalAcel) {
        this.totalAcel = totalAcel;
    }

    /**
     * @return the curvaAcelerador
     */
    public Double getCurvaAcelerador() {
        return curvaAcelerador;
    }

    /**
     * @param curvaAcelerador the curvaAcelerador to set
     */
    public void setCurvaAcelerador(Double curvaAcelerador) {
        this.curvaAcelerador = curvaAcelerador;
    }

    /**
     * @return the variable100
     */
    public Double getVariable100() {
        return variable100;
    }

    /**
     * @param variable100 the variable100 to set
     */
    public void setVariable100(Double variable100) {
        this.variable100 = variable100;
    }

    /**
     * @return the valorMes
     */
    public Double getValorMes() {
        return valorMes;
    }

    /**
     * @param valorMes the valorMes to set
     */
    public void setValorMes(Double valorMes) {
        this.valorMes = valorMes;
    }

    /**
     * @return the ajusteMesesAnteriores
     */
    public Double getAjusteMesesAnteriores() {
        return ajusteMesesAnteriores;
    }

    /**
     * @param ajusteMesesAnteriores the ajusteMesesAnteriores to set
     */
    public void setAjusteMesesAnteriores(Double ajusteMesesAnteriores) {
        this.ajusteMesesAnteriores = ajusteMesesAnteriores;
    }

    /**
     * @return the garantizado
     */
    public Double getGarantizado() {
        return garantizado;
    }

    /**
     * @param garantizado the garantizado to set
     */
    public void setGarantizado(Double garantizado) {
        this.garantizado = garantizado;
    }

    /**
     * @return the garantizadoNvosCanales
     */
    public Double getGarantizadoNvosCanales() {
        return garantizadoNvosCanales;
    }

    /**
     * @param garantizadoNvosCanales the garantizadoNvosCanales to set
     */
    public void setGarantizadoNvosCanales(Double garantizadoNvosCanales) {
        this.garantizadoNvosCanales = garantizadoNvosCanales;
    }

    /**
     * @return the acelTeUFVDPrepago
     */
    public String getAcelTeUFVDPrepago() {
        return acelTeUFVDPrepago;
    }

    /**
     * @param acelTeUFVDPrepago the acelTeUFVDPrepago to set
     */
    public void setAcelTeUFVDPrepago(String acelTeUFVDPrepago) {
        this.acelTeUFVDPrepago = acelTeUFVDPrepago;
    }

    /**
     * @return the acelPortacionesFVDPrepago
     */
    public String getAcelPortacionesFVDPrepago() {
        return acelPortacionesFVDPrepago;
    }

    /**
     * @param acelPortacionesFVDPrepago the acelPortacionesFVDPrepago to set
     */
    public void setAcelPortacionesFVDPrepago(String acelPortacionesFVDPrepago) {
        this.acelPortacionesFVDPrepago = acelPortacionesFVDPrepago;
    }

    /**
     * @return the valorAPagar
     */
    public Double getValorAPagar() {
        return valorAPagar;
    }

    /**
     * @param valorAPagar the valorAPagar to set
     */
    public void setValorAPagar(Double valorAPagar) {
        this.valorAPagar = valorAPagar;
    }

    /**
     * @return the categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }


}
