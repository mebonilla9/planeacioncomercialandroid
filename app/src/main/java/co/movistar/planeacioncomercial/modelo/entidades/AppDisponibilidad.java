package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDisponibilidad implements Serializable {

    private String llave;
    private String divipola;
    private String mdf;
    private String armario;
    private Long plantainternalibre;
    private String zonaBa;
    private Long puertosbalibre;
    private Long primariatotal;
    private Long primarialibre;
    private Long secundariatotal;
    private Long secundarialibre;
    private String tipored;
    private String caja;
    private String tipocaja;
    private String direccioncaja;
    private Long pareslibrescaja;
    private String sistema;
    private String categoria;
    private String ofertar;
    private String velocidad;
    private String nombrepop;
    private String tipoequipo;
    private String departamento;
    private String nombreMunicipio;
    private String localidad;
    private String distrito;
    private String regional;
    private String qOferta;

    public AppDisponibilidad() {
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public String getDivipola() {
        return divipola;
    }

    public void setDivipola(String divipola) {
        this.divipola = divipola;
    }

    public String getMDF() {
        return mdf;
    }

    public void setMDF(String mdf) {
        this.mdf = mdf;
    }

    public String getArmario() {
        return armario;
    }

    public void setArmario(String armario) {
        this.armario = armario;
    }

    public Long getPlantainternalibre() {
        return plantainternalibre;
    }

    public void setPlantainternalibre(Long plantainternalibre) {
        this.plantainternalibre = plantainternalibre;
    }

    public String getZonaBa() {
        return zonaBa;
    }

    public void setZonaBa(String zonaBa) {
        this.zonaBa = zonaBa;
    }

    public Long getPuertosbalibre() {
        return puertosbalibre;
    }

    public void setPuertosbalibre(Long puertosbalibre) {
        this.puertosbalibre = puertosbalibre;
    }

    public Long getPrimariatotal() {
        return primariatotal;
    }

    public void setPrimariatotal(Long primariatotal) {
        this.primariatotal = primariatotal;
    }

    public Long getPrimarialibre() {
        return primarialibre;
    }

    public void setPrimarialibre(Long primarialibre) {
        this.primarialibre = primarialibre;
    }

    public Long getSecundariatotal() {
        return secundariatotal;
    }

    public void setSecundariatotal(Long secundariatotal) {
        this.secundariatotal = secundariatotal;
    }

    public Long getSecundarialibre() {
        return secundarialibre;
    }

    public void setSecundarialibre(Long secundarialibre) {
        this.secundarialibre = secundarialibre;
    }

    public String getTipored() {
        return tipored;
    }

    public void setTipored(String Tipored) {
        this.tipored = Tipored;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public String getTipocaja() {
        return tipocaja;
    }

    public void setTipocaja(String tipocaja) {
        this.tipocaja = tipocaja;
    }

    public String getDireccioncaja() {
        return direccioncaja;
    }

    public void setDireccioncaja(String direccioncaja) {
        this.direccioncaja = direccioncaja;
    }

    public Long getPareslibrescaja() {
        return pareslibrescaja;
    }

    public void setPareslibrescaja(Long pareslibrescaja) {
        this.pareslibrescaja = pareslibrescaja;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String Sistema) {
        this.sistema = Sistema;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getOfertar() {
        return ofertar;
    }

    public void setOfertar(String Ofertar) {
        this.ofertar = Ofertar;
    }

    public String getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(String velocidad) {
        this.velocidad = velocidad;
    }

    public String getNombrepop() {
        return nombrepop;
    }

    public void setNombrepop(String nombrepop) {
        this.nombrepop = nombrepop;
    }

    public String getTipoequipo() {
        return tipoequipo;
    }

    public void setTipoequipo(String tipoequipo) {
        this.tipoequipo = tipoequipo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public String getqOferta() {
        return qOferta;
    }

    public void setqOferta(String qOferta) {
        this.qOferta = qOferta;
    }
}
