package co.movistar.planeacioncomercial.modelo.servicios.call;

import co.movistar.planeacioncomercial.modelo.dto.InformacionUsuarioDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by lord_nightmare on 18/04/17.
 */

public interface AppMenuCall {

    @GET("menu/consultar")
    Call<InformacionUsuarioDTO> consultarMenu(@Header("Authorization") String token);
}
