package co.movistar.planeacioncomercial.modelo.dto;

/**
 * Created by Lord_Nightmare on 5/03/18.
 */

public class DireccionFibraDTO extends DireccionDTO {

    private String cruceVialSecundario;

    @Override
    public String toString() {
        StringBuilder armador = new StringBuilder();
        armador.append(this.getCruceVial());
        armador.append(" ");
        armador.append(this.getNumeroViaPrincipal());
        armador.append(" ");
        armador.append(" ");
        armador.append(this.getCruceVialSecundario());
        armador.append(" ");
        armador.append(this.getNumeroViaGeneradora());
        armador.append(" - ");
        armador.append(this.getSeccionFinal());
        armador.append(" ");
        return armador.toString();
    }

    public String getCruceVialSecundario() {
        return cruceVialSecundario;
    }

    public void setCruceVialSecundario(String cruceVialSecundario) {
        this.cruceVialSecundario = cruceVialSecundario;
    }
}
