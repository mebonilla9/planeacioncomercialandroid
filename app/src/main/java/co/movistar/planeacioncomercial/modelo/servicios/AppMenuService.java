package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import co.movistar.planeacioncomercial.modelo.dto.InformacionUsuarioDTO;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppMenuCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 18/04/17.
 */

public class AppMenuService extends GenericoService {

    private AppMenuCall service;

    public AppMenuService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppMenuCall.class);
    }

    public void consultarMenu(Callback<InformacionUsuarioDTO> callback) {
        Call<InformacionUsuarioDTO> respuesta = this.service.consultarMenu(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }
}
