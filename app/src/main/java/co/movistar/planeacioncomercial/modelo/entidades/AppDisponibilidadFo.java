package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 * Created by Lord_Nightmare on 26/02/18.
 */

public class AppDisponibilidadFo implements Serializable {

    private Long idDisponibilidadFo;
    private String barrio;
    private String localidad;
    private String departamento;
    private String regional;
    private String xLibreCto;
    private String cto;
    private String principal;
    private String referenciaPrincipal;
    private String cruce;
    private String referenciaCruce;
    private String placa;
    private String complemento;
    private String direccion;
    private Long paresLibres;

    public AppDisponibilidadFo() {
    }

    /**
     * @return the idDisponibilidadFo
     */
    public Long getIdDisponibilidadFo() {
        return idDisponibilidadFo;
    }

    /**
     * @param idDisponibilidadFo the idDisponibilidadFo to set
     */
    public void setIdDisponibilidadFo(Long idDisponibilidadFo) {
        this.idDisponibilidadFo = idDisponibilidadFo;
    }

    /**
     * @return the barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * @param barrio the barrio to set
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * @return the localidad
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * @param localidad the localidad to set
     */
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the regional
     */
    public String getRegional() {
        return regional;
    }

    /**
     * @param regional the regional to set
     */
    public void setRegional(String regional) {
        this.regional = regional;
    }

    /**
     * @return the xLibreCto
     */
    public String getxLibreCto() {
        return xLibreCto;
    }

    /**
     * @param xLibreCto the xLibreCto to set
     */
    public void setxLibreCto(String xLibreCto) {
        this.xLibreCto = xLibreCto;
    }

    /**
     * @return the cto
     */
    public String getCto() {
        return cto;
    }

    /**
     * @param cto the cto to set
     */
    public void setCto(String cto) {
        this.cto = cto;
    }

    /**
     * @return the principal
     */
    public String getPrincipal() {
        return principal;
    }

    /**
     * @param principal the principal to set
     */
    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    /**
     * @return the referenciaPrincipal
     */
    public String getReferenciaPrincipal() {
        return referenciaPrincipal;
    }

    /**
     * @param referenciaPrincipal the referenciaPrincipal to set
     */
    public void setReferenciaPrincipal(String referenciaPrincipal) {
        this.referenciaPrincipal = referenciaPrincipal;
    }

    /**
     * @return the cruce
     */
    public String getCruce() {
        return cruce;
    }

    /**
     * @param cruce the cruce to set
     */
    public void setCruce(String cruce) {
        this.cruce = cruce;
    }

    /**
     * @return the referenciaCruce
     */
    public String getReferenciaCruce() {
        return referenciaCruce;
    }

    /**
     * @param referenciaCruce the referenciaCruce to set
     */
    public void setReferenciaCruce(String referenciaCruce) {
        this.referenciaCruce = referenciaCruce;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the complemento
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * @param complemento the complemento to set
     */
    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the paresLibres
     */
    public Long getParesLibres() {
        return paresLibres;
    }

    /**
     * @param paresLibres the paresLibres to set
     */
    public void setParesLibres(Long paresLibres) {
        this.paresLibres = paresLibres;
    }

}
