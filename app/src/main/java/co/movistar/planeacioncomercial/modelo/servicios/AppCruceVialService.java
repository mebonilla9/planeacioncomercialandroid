package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppCruceVial;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppCruceVialCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 17/08/17.
 */

public class AppCruceVialService extends GenericoService{

    private AppCruceVialCall service;

    public AppCruceVialService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppCruceVialCall.class);
    }

    public void consultarVias(Callback<List<AppCruceVial>> callback){
        Call<List<AppCruceVial>> respuesta = this.service.consultarCruceVial(PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);

    }

}
