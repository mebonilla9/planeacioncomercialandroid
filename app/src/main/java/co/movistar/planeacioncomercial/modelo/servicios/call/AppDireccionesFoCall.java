package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface AppDireccionesFoCall {
    @GET("fibraoptica/complementos/{conjunto}/{tipo}")
    Call<List<String>> consultarComplementos(@Path("conjunto") String conjunto, @Path("tipo") String tipo, @Header("Authorization") String token);

    @GET("fibraoptica/conjuntos/{municipio}/{tipo}")
    Call<List<String>> consultarConjuntos(@Path("municipio") String municipio, @Path("tipo") String tipo, @Header("Authorization") String token);

    @GET("fibraoptica/direccion/{complemento}/{tipo}")
    Call<ResponseBody> consultarDireccionComplemento(@Path("complemento") String complemento, @Path("tipo") String tipo, @Header("Authorization") String token);

    @GET("fibraoptica/direccioncomp/{conjunto}/{tipo}")
    Call<List<String>> consultarDireccionConjunto(@Path("conjunto") String conjunto, @Path("tipo") String tipo, @Header("Authorization") String token);

    @GET("fibraoptica/municipios/{tipo}")
    Call<List<String>> consultarMunicipios(@Path("tipo") String tipo, @Header("Authorization") String token);
}
