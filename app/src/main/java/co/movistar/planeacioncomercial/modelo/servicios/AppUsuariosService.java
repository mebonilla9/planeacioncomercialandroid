package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppUsuarios;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppUsuariosCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 23/01/17.
 */

public class AppUsuariosService extends GenericoService {

    private AppUsuariosCall service;

    public AppUsuariosService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppUsuariosCall.class);
    }

    public void reestablecerContrasena(String contrasena, Callback<ResponseBody> callback) {
        Call<ResponseBody> respuesta = this.service.reestablecerContrasena(contrasena, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarSubordinados(Callback<List<AppUsuarios>> callback){
        Call<List<AppUsuarios>> respuesta = this.service.consultarSubordinados(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void reestablecerContrasenaCorreo(String correo, Callback<ResponseBody> callback) {
        this.service.reestablecerContrasenaCorreo(correo).enqueue(callback);
    }
}
