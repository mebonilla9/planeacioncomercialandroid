package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.dto.InformacionComisionesDTO;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppFinancieroCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 20/10/17.
 */

public class AppFinancieroService extends GenericoService {

    private AppFinancieroCall service;

    public AppFinancieroService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppFinancieroCall.class);
    }

    public void consultarPeriodos(Callback<List<String>> callback){
        Call<List<String>> respuesta = this.service.consultarPeriodos(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void listaProductosDisponibles(String periodo, Callback<List<String>> callback){
        Call<List<String>> respuesta = this.service.listaProductosDisponibles(periodo, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);

    }

    public void listaCostoxAltaProductos(String periodo, String producto, Callback<InformacionComisionesDTO> callback){
        Call<InformacionComisionesDTO> respuesta = this.service.listaCostoxAltaProductos(periodo, producto, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }
}
