package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppCanales;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public interface AppCanalesCall {

    @PUT("canal/insertar")
    Call<ResponseBody> insertarCanales(@Body AppCanales content, @Header("Authorization") String token);

    @PUT("canal/modificar")
    Call<ResponseBody> modificarCanales(@Body AppCanales content, @Header("Authorization") String token);

    @GET("canal/consultar")
    Call<List<AppCanales>> consultarCanales(@Header("Authorization") String token);

    @GET("canal/consultar/{id}")
    Call<List<AppCanales>> consultarCanales(@Path("id") long id, @Header("Authorization") String token);


}
