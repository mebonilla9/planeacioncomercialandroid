package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppLiquidacionHist;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppLiquidacionHistCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 27/02/17.
 */

public class AppLiquidacionHistService extends GenericoService {

    private AppLiquidacionHistCall service;

    public AppLiquidacionHistService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppLiquidacionHistCall.class);
    }

    public void obtenerResumenLiquidacion(String periodo, Callback<AppLiquidacionHist> callback) {
        Call<AppLiquidacionHist> respuesta = this.service.obtenerResumenLiquidacion(periodo, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarCategoria(Callback<List<AppLiquidacionHist>> callback) {
        Call<List<AppLiquidacionHist>> respuesta = this.service.consultarCategoria(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarPeriodos(Callback<List<AppLiquidacionHist>> callback) {
        Call<List<AppLiquidacionHist>> respuesta = this.service.consultarPeriodos(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarDetalleLiquidacion(String categoria, String periodo, Callback<List<AppLiquidacionHist>> callback) {
        Call<List<AppLiquidacionHist>> respuesta = this.service.consultarDetalleLiquidacion(PreferenciasUtil.obtenerPreferencias("auth-token", contexto), categoria, periodo);
        respuesta.enqueue(callback);
    }
}
