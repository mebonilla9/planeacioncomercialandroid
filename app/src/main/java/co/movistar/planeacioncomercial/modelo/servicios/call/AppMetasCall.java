package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppPresupuestoComercial;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 15/03/17.
 */

public interface AppMetasCall {

    @GET("metas/categorias")
    Call<List<AppPresupuestoComercial>> consultarCategoriasUsuario(@Header("Authorization") String token);

    @GET("metas/periodos")
    Call<List<AppPresupuestoComercial>> consultarPeriodosUsuario(@Header("Authorization") String token);

    @GET("metas/consultar/{categoria}/{periodo}")
    Call<List<AppPresupuestoComercial>> consultarMetaUsuario(@Path("categoria") String categoria, @Path("periodo") String periodo, @Header("Authorization") String token);

}
