package co.movistar.planeacioncomercial.modelo.servicios.autoridad;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.util.Date;

import co.movistar.planeacioncomercial.modelo.servicios.GenericoService;
import co.movistar.planeacioncomercial.modelo.servicios.call.AutenticacionCall;
import co.movistar.planeacioncomercial.negocio.actividades.LoginActivity;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.MacAddressUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.RetrofitErrorUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.TokenUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public class AutenticacionService extends GenericoService {

    private AutenticacionCall service;

    public AutenticacionService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AutenticacionCall.class);
    }

    public void autenticarUsuario(String correo, String contrasena, Callback<ResponseBody> callback) {
        Call<ResponseBody> respuesta = this.service.autenticarUsuario(correo, contrasena, MacAddressUtil.getMacAddress());
        respuesta.enqueue(callback);
    }

    public void obtenerFechaToken() {
        try {
            String token = TokenUtil.obtenerTokenLocal(contexto);
            Call<ResponseBody> respuesta = this.service.obtenerFechaToken(token);
            respuesta.enqueue(new Callback<ResponseBody>() {

                private DialogInterface.OnClickListener regresarLogin() {
                    return new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            contexto.startActivity(new Intent(contexto, LoginActivity.class));
                        }
                    };
                }

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()) {
                            Date fechaExpiracion = new Date(Long.parseLong(response.body().string()));
                            TokenUtil.verificarFechaToken(contexto, fechaExpiracion);
                            return;
                        }
                        throw RetrofitErrorUtil.convertirError(retrofit, response, EMensajes.ERROR_EXPIRACION_TOKEN);

                    } catch (PlaneacionComercialException | IOException e) {
                        if (e instanceof PlaneacionComercialException) {
                            AlertaUtil.mostrarAlerta("Error Autenticacion",
                                    EMensajes.ERROR_EXPIRACION_TOKEN.getDescripcion(),
                                    regresarLogin(),
                                    regresarLogin(),
                                    contexto);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("FALLA", call.toString());
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
