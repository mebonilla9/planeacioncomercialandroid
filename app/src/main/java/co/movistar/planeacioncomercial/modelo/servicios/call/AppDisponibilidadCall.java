package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.dto.DireccionDTO;
import co.movistar.planeacioncomercial.modelo.dto.InfoDisponibilidadDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 5/12/16.
 */

public interface AppDisponibilidadCall {

    @POST("disponibilidad/consultar")
    Call<InfoDisponibilidadDTO> consultarInformacionDisponibilidad(@Body String content, @Header("Authorization") String token);

    @GET("disponibilidad/departamentos")
    Call<List<AppDisponibilidad>> consultarInformacionDepartamentos(@Header("Authorization") String token);

    @POST("disponibilidad/localidades/{departamento}")
    Call<List<AppDisponibilidad>> consultarInformacionLocalidades(@Path("departamento") String departamento, @Header("Authorization") String token);

    @GET("disponibilidad/distritos/{departamento}/{localidad}")
    Call<List<AppDisponibilidad>> consultarInformacionDistritos(@Path("departamento") String departamento, @Path("localidad") String localidad, @Header("Authorization") String token);

    @GET("disponibilidad/armarios/{distrito}")
    Call<List<AppDisponibilidad>> consultarInformacionArmarios(@Path("distrito") String distrito, @Header("Authorization") String token);

    @GET("disponibilidad/cajas/{distrito}/{armario}")
    Call<List<AppDisponibilidad>> consultarInformacionCajas(@Path("distrito") String distrito, @Path("armario") String armario, @Header("Authorization") String token);

    @POST("disponibilidad/cajas/direccion")
    Call<InfoDisponibilidadDTO> consultarInformacionDirecciones(@Body DireccionDTO direccion, @Header("Authorization") String token);

    @POST("disponibilidad/localidades/{departamento}")
    Call<List<AppDisponibilidad>> consultarInformacionLocalidadesCoordenadas(@Path("departamento") String departamento, @Header("Authorization") String token);
}