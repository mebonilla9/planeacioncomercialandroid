package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppPorcentajeCorte implements Serializable {

    private Long idPorcentajeCorte;
    private String deberiamosIrAl;
    private String altasAlDia;

    public AppPorcentajeCorte() {
    }

    /**
     * @return the idPorcentajeCorte
     */
    public Long getIdPorcentajeCorte() {
        return idPorcentajeCorte;
    }

    /**
     * @param idPorcentajeCorte the idPorcentajeCorte to set
     */
    public void setIdPorcentajeCorte(Long idPorcentajeCorte) {
        this.idPorcentajeCorte = idPorcentajeCorte;
    }

    /**
     * @return the deberiamosIrAl
     */
    public String getDeberiamosIrAl() {
        return deberiamosIrAl;
    }

    /**
     * @param deberiamosIrAl the deberiamosIrAl to set
     */
    public void setDeberiamosIrAl(String deberiamosIrAl) {
        this.deberiamosIrAl = deberiamosIrAl;
    }

    /**
     * @return the altasAlDia
     */
    public String getAltasAlDia() {
        return altasAlDia;
    }

    /**
     * @param altasAlDia the altasAlDia to set
     */
    public void setAltasAlDia(String altasAlDia) {
        this.altasAlDia = altasAlDia;
    }

    

}
