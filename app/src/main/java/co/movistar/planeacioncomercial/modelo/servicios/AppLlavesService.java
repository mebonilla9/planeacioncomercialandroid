package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import co.movistar.planeacioncomercial.modelo.entidades.AppLlaves;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppLlavesCall;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 28/12/16.
 */

public class AppLlavesService extends GenericoService{

    private AppLlavesCall service;

    public AppLlavesService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppLlavesCall.class);
    }

    public void insertarLlave(AppLlaves llave, Callback<ResponseBody> callback){
        Call<ResponseBody> respuesta = this.service.insertarLlaves(llave);
        respuesta.enqueue(callback);
    }
}
