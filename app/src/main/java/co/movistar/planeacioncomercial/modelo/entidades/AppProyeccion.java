package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 * Created by lord_nightmare on 3/05/17.
 */

public class AppProyeccion implements Serializable {
    private Long idUsuario;
    private String producto;
    private String segmento;
    private Long altas;
    private Long presupuesto;
    private Long alcorte;

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the producto
     */
    public String getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(String producto) {
        this.producto = producto;
    }

    /**
     * @return the segmento
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * @param segmento the segmento to set
     */
    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    /**
     * @return the altas
     */
    public Long getAltas() {
        return altas;
    }

    /**
     * @param altas the altas to set
     */
    public void setAltas(Long altas) {
        this.altas = altas;
    }

    /**
     * @return the presupuesto
     */
    public Long getPresupuesto() {
        return presupuesto;
    }

    /**
     * @param presupuesto the presupuesto to set
     */
    public void setPresupuesto(Long presupuesto) {
        this.presupuesto = presupuesto;
    }

    /**
     * @return the alcorte
     */
    public Long getAlcorte() {
        return alcorte;
    }

    /**
     * @param alcorte the alcorte to set
     */
    public void setAlcorte(Long alcorte) {
        this.alcorte = alcorte;
    }
}
