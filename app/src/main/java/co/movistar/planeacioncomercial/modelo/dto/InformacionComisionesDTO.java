package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.entidades.AppCostoxAlta;
import java.util.List;

public class InformacionComisionesDTO {
    private List<List<AppCostoxAlta>> listaComisiones;

    public List<List<AppCostoxAlta>> getListaComisiones() {
        return this.listaComisiones;
    }

    public void setListaComisiones(List<List<AppCostoxAlta>> listaComisiones) {
        this.listaComisiones = listaComisiones;
    }
}
