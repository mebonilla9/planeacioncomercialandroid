package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import co.movistar.planeacioncomercial.modelo.dto.InformacionCalcularIpDTO;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppCalcularIpCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 24/02/17.
 */

public class AppCalcularIpService extends GenericoService {
    private AppCalcularIpCall service;

    public AppCalcularIpService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppCalcularIpCall.class);
    }

    public void consultarInformacionCalculador(Callback<InformacionCalcularIpDTO> callback){
        Call<InformacionCalcularIpDTO> respuesta = this.service.consultarInformacionCalculador(PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void actualizarInformacionCalculador(InformacionCalcularIpDTO infoCalcular, Callback<InformacionCalcularIpDTO> callback){
        Call<InformacionCalcularIpDTO> respuesta = this.service.actualizarInformacionCalculador(infoCalcular, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }
}
