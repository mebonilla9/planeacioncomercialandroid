package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppJefatura implements Serializable {

    private Long idJefatura;
    private String nombreJefatura;
    private AppEstados estado;

    public AppJefatura() {
    }

    public AppJefatura(Long idJefatura) {
        this.idJefatura = idJefatura;
    }

    /**
     * @return the idJefatura
     */
    public Long getIdJefatura() {
        return idJefatura;
    }

    /**
     * @param idJefatura the idJefatura to set
     */
    public void setIdJefatura(Long idJefatura) {
        this.idJefatura = idJefatura;
    }

    /**
     * @return the nombreJefatura
     */
    public String getNombreJefatura() {
        return nombreJefatura;
    }

    /**
     * @param nombreJefatura the nombreJefatura to set
     */
    public void setNombreJefatura(String nombreJefatura) {
        this.nombreJefatura = nombreJefatura;
    }

    /**
     * @return the estado
     */
    public AppEstados getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(AppEstados estado) {
        this.estado = estado;
    }

}
