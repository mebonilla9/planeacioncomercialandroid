package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppPresupuestoComercial;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppMetasCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 15/03/17.
 */

public class AppMetasService extends GenericoService {

    private AppMetasCall service;

    public AppMetasService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppMetasCall.class);
    }

    public void consultarCategoriasUsuario(Callback<List<AppPresupuestoComercial>> callback) {
        Call<List<AppPresupuestoComercial>> respuesta = this.service.consultarCategoriasUsuario(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarPeriodosUsuario(Callback<List<AppPresupuestoComercial>> callback){
        Call<List<AppPresupuestoComercial>> respuesta = this.service.consultarPeriodosUsuario(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarMetaUsuario(String categoria, String periodo, Callback<List<AppPresupuestoComercial>> callback) {
        Call<List<AppPresupuestoComercial>> respuesta = this.service.consultarMetaUsuario(categoria, periodo, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

}
