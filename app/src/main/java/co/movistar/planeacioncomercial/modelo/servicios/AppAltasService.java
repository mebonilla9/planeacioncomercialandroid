package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.dto.InformacionAltasGeneralDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppAltas;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppAltasCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 18/11/16.
 */

public class AppAltasService extends GenericoService {

    private AppAltasCall service;

    public AppAltasService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppAltasCall.class);
    }

    public void consultarInformacionAltas(String tipo, String idUsuario, Callback<InformacionAltasGeneralDTO> callback){
        Call<InformacionAltasGeneralDTO> respuesta = this.service.consultarInformacionAltas(tipo, idUsuario, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }

    public void listaProductosDisponibles(String idUsuario, Callback<List<String>> callback){
        Call<List<String>> respuesta = this.service.listaProductosDisponibles(idUsuario, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }
}
