package co.movistar.planeacioncomercial.modelo.servicios.call;

import co.movistar.planeacioncomercial.modelo.entidades.AppNotificacion;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by lord_nightmare on 12/07/17.
 */

public interface AppNotificacionCall {

    @GET("notificacion/consultar")
    Call<AppNotificacion> consultarUltimaNotificacion(@Header("Authorization") String token);
}
