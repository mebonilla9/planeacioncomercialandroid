package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCanales implements Serializable {

    private Long idCanal;
    private String nombreCanal;
    private AppEstados estado;

    public AppCanales() {
    }

    public AppCanales(Long idCanal) {
        this.idCanal = idCanal;
    }

    /**
     * @return the idCanal
     */
    public Long getIdCanal() {
        return idCanal;
    }

    /**
     * @param idCanal the idCanal to set
     */
    public void setIdCanal(Long idCanal) {
        this.idCanal = idCanal;
    }

    /**
     * @return the nombreCanal
     */
    public String getNombreCanal() {
        return nombreCanal;
    }

    /**
     * @param nombreCanal the nombreCanal to set
     */
    public void setNombreCanal(String nombreCanal) {
        this.nombreCanal = nombreCanal;
    }

    /**
     * @return the estado
     */
    public AppEstados getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(AppEstados estado) {
        this.estado = estado;
    }
    
}
