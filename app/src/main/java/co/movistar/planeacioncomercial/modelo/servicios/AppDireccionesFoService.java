package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.servicios.call.AppDireccionesFoCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class AppDireccionesFoService extends GenericoService {
    private AppDireccionesFoCall service;

    public AppDireccionesFoService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppDireccionesFoCall.class);
    }

    public void consultarMunicipios(Callback<List<String>> callback, String tipo) {
        Call<List<String>> respuesta = this.service.consultarMunicipios(tipo, PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }

    public void consultarConjuntos(Callback<List<String>> callback, String municipio, String tipo) {
        Call<List<String>> respuesta = this.service.consultarConjuntos(municipio, tipo, PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }

    public void consultarComplementos(Callback<List<String>> callback, String conjunto, String tipo) {
        Call<List<String>> respuesta = this.service.consultarComplementos(conjunto, tipo, PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }

    public void consultarDireccionConjunto(Callback<List<String>> callback, String conjunto, String tipo) {
        Call<List<String>> respuesta = this.service.consultarDireccionConjunto(conjunto, tipo, PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }

    public void consultarDireccionComplemento(Callback<ResponseBody> callback, String complemento, String tipo) {
        Call<ResponseBody> respuesta = this.service.consultarDireccionComplemento(complemento, tipo, PreferenciasUtil.obtenerPreferencias("auth-token", this.contexto));
        respuesta.enqueue(callback);
    }
}
