package co.movistar.planeacioncomercial.modelo.servicios.call;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public interface AutenticacionCall {

    @POST("autenticacion")
    @FormUrlEncoded
    Call<ResponseBody> autenticarUsuario(@Field("correo") String correo, @Field("contrasena") String contrasena, @Field("mac_address") String macAddress);

    @GET("autenticacion")
    Call<ResponseBody> obtenerFechaToken(@Header("Authorization") String token);
}
