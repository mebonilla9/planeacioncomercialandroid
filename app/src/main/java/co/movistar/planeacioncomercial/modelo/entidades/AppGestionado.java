package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

public class AppGestionado implements Serializable {

    private Long idGestionado;
    private Long idUsuario;
    private Long cedula;
    private String correo;
    private String nVariable;
    private String variable;
    private String segmento;
    private Double meta;
    private Long ejecucion;
    private Double cumplimiento;
    private Double pesoEsquema;
    private Double ipVariable;

    public AppGestionado() {
    }

    /**
     * @return the idGestionado
     */
    public Long getIdGestionado() {
        return idGestionado;
    }

    /**
     * @param idGestionado the idGestionado to set
     */
    public void setIdGestionado(Long idGestionado) {
        this.idGestionado = idGestionado;
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the cedula
     */
    public Long getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * @return the nVariable
     */
    public String getnVariable() {
        return nVariable;
    }

    /**
     * @param nVariable the nVariable to set
     */
    public void setnVariable(String nVariable) {
        this.nVariable = nVariable;
    }

    /**
     * @return the variable
     */
    public String getVariable() {
        return variable;
    }

    /**
     * @param variable the variable to set
     */
    public void setVariable(String variable) {
        this.variable = variable;
    }

    /**
     * @return the segmento
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * @param segmento the segmento to set
     */
    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    /**
     * @return the meta
     */
    public Double getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(Double meta) {
        this.meta = meta;
    }

    /**
     * @return the ejecucion
     */
    public Long getEjecucion() {
        return ejecucion;
    }

    /**
     * @param ejecucion the ejecucion to set
     */
    public void setEjecucion(Long ejecucion) {
        this.ejecucion = ejecucion;
    }

    /**
     * @return the cumplimiento
     */
    public Double getCumplimiento() {
        return cumplimiento;
    }

    /**
     * @param cumplimiento the cumplimiento to set
     */
    public void setCumplimiento(Double cumplimiento) {
        this.cumplimiento = cumplimiento;
    }

    /**
     * @return the pesoEsquema
     */
    public Double getPesoEsquema() {
        return pesoEsquema;
    }

    /**
     * @param pesoEsquema the pesoEsquema to set
     */
    public void setPesoEsquema(Double pesoEsquema) {
        this.pesoEsquema = pesoEsquema;
    }

    /**
     * @return the ipVariable
     */
    public Double getIpVariable() {
        return ipVariable;
    }

    /**
     * @param ipVariable the ipVariable to set
     */
    public void setIpVariable(Double ipVariable) {
        this.ipVariable = ipVariable;
    }

}
