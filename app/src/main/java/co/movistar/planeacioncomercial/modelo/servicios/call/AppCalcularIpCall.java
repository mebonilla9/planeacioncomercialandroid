package co.movistar.planeacioncomercial.modelo.servicios.call;

import co.movistar.planeacioncomercial.modelo.dto.InformacionCalcularIpDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Lord_Nightmare on 13/02/17.
 */

public interface AppCalcularIpCall {

    @GET("calculadorip/consultar/")
    Call<InformacionCalcularIpDTO> consultarInformacionCalculador(@Header("Authorization") String token);

    @POST("calculadorip/actualizar")
    Call<InformacionCalcularIpDTO> actualizarInformacionCalculador(@Body InformacionCalcularIpDTO infoCalcular, @Header("Authorization") String token);
}
