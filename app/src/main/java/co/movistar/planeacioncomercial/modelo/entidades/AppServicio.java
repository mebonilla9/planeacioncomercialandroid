/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppServicio implements Serializable {

    private Long idServicio;
    private String canal;
    private String regional;
    private Double nivelServicio;
    private Long visitas;
    private String tma;
    private Long idUsuario;

    /**
     * @return the idServicio
     */
    public Long getIdServicio() {
        return idServicio;
    }

    /**
     * @param idServicio the idServicio to set
     */
    public void setIdServicio(Long idServicio) {
        this.idServicio = idServicio;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * @return the regional
     */
    public String getRegional() {
        return regional;
    }

    /**
     * @param regional the regional to set
     */
    public void setRegional(String regional) {
        this.regional = regional;
    }

    /**
     * @return the nivelServicio
     */
    public Double getNivelServicio() {
        return nivelServicio;
    }

    /**
     * @param nivelServicio the nivelServicio to set
     */
    public void setNivelServicio(Double nivelServicio) {
        this.nivelServicio = nivelServicio;
    }

    /**
     * @return the visitas
     */
    public Long getVisitas() {
        return visitas;
    }

    /**
     * @param visitas the visitas to set
     */
    public void setVisitas(Long visitas) {
        this.visitas = visitas;
    }

    /**
     * @return the tma
     */
    public String getTma() {
        return tma;
    }

    /**
     * @param tma the tma to set
     */
    public void setTma(String tma) {
        this.tma = tma;
    }

    /**
     * @return the idUsuario
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

}
