package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AppBajas implements Serializable {
    private Long fechaVenta;
    private Long idBaja;
    private String paisProm;
    private Map<String, String> periodos = new HashMap();
    private String producto;
    private String promedio;
    private String regional;

    public Long getIdBaja() {
        return this.idBaja;
    }

    public void setIdBaja(Long idBaja) {
        this.idBaja = idBaja;
    }

    public String getProducto() {
        return this.producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getRegional() {
        return this.regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public Long getFechaVenta() {
        return this.fechaVenta;
    }

    public void setFechaVenta(Long fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public String getPromedio() {
        return this.promedio;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }

    public String getPaisProm() {
        return this.paisProm;
    }

    public void setPaisProm(String paisProm) {
        this.paisProm = paisProm;
    }

    public Map<String, String> getPeriodos() {
        return this.periodos;
    }

    public void setPeriodos(Map<String, String> periodos) {
        this.periodos = periodos;
    }
}
