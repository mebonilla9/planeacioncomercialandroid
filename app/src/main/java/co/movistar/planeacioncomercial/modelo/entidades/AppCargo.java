package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCargo implements Serializable {

    private Long idCargo;
    private AppEstados estado;
    private String nomCargo;

    public AppCargo() {
    }

    public AppCargo(Long idCargo) {
        this.idCargo = idCargo;
    }

    /**
     * @return the idCargo
     */
    public Long getIdCargo() {
        return idCargo;
    }

    /**
     * @param idCargo the idCargo to set
     */
    public void setIdCargo(Long idCargo) {
        this.idCargo = idCargo;
    }

    /**
     * @return the estado
     */
    public AppEstados getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(AppEstados estado) {
        this.estado = estado;
    }

    /**
     * @return the nomCargo
     */
    public String getNomCargo() {
        return nomCargo;
    }

    /**
     * @param nomCargo the nomCargo to set
     */
    public void setNomCargo(String nomCargo) {
        this.nomCargo = nomCargo;
    }
    
}
