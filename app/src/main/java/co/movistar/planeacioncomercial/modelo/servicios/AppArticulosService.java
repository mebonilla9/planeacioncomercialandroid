package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppArticulos;
import co.movistar.planeacioncomercial.modelo.servicios.GenericoService;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppArticulosCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public class AppArticulosService extends GenericoService {

    private AppArticulosCall service;

    public AppArticulosService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppArticulosCall.class);
    }

    public void obtenerArticulosPunto(String id, Callback<List<AppArticulos>> callback){
        Call<List<AppArticulos>> respuesta = this.service.obtenerArticulosPunto(id, PreferenciasUtil.obtenerPreferencias("auth-token",contexto));
        respuesta.enqueue(callback);
    }
}
