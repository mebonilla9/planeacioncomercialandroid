package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppArchivos;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 15/03/17.
 */

public interface AppArchivosCall {

    @GET("archivos/consultar/{modulo}")
    Call<List<String>> consultarCategoriasModulo(@Path("modulo") String modulo, @Header("Authorization") String token);

    @GET("archivos/consultar/{modulo}/{categoria}")
    Call<List<AppArchivos>> consultarArchivosModuloCategoria(@Path("modulo") String modulo, @Path("categoria") String categoria, @Header("Authorization") String token);

}
