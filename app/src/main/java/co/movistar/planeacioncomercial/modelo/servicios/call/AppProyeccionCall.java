package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppProyeccion;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by lord_nightmare on 3/05/17.
 */

public interface AppProyeccionCall {

    @GET("proyeccion/consultar/{producto}")
    Call<List<AppProyeccion>> listaProyeccionProductos(@Path("producto") String producto, @Header("Authorization") String token);

    @GET("proyeccion/productos")
    Call<List<String>>listaProductosDisponibles(@Header("Authorization") String token);
}
