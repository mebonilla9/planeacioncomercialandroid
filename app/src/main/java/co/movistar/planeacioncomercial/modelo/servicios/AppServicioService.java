package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppServicio;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppServicioCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 19/04/17.
 */

public class AppServicioService extends GenericoService {

    private AppServicioCall service;

    public AppServicioService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppServicioCall.class);
    }

    public void consultarServicios (Callback<List<AppServicio>> callback){
        Call<List<AppServicio>> respuesta = this.service.consultarServicios(PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }
}
