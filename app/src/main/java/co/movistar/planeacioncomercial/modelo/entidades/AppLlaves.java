package co.movistar.planeacioncomercial.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppLlaves implements Serializable {

    private Long idLlave;
    private String llave;

    public AppLlaves() {
    }

    public Long getIdLlave() {
        return idLlave;
    }

    public void setIdLlave(Long idLlave) {
        this.idLlave = idLlave;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

}
