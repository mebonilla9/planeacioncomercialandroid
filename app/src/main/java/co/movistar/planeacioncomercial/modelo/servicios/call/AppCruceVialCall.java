package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppCruceVial;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by lord_nightmare on 17/08/17.
 */

public interface AppCruceVialCall {

    @GET("crucevial/consultar")
    Call<List<AppCruceVial>> consultarCruceVial(@Header("Authorization") String token);

}
