package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppArchivos;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppArchivosCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Lord_Nightmare on 15/03/17.
 */

public class AppArchivosService extends GenericoService {

    private AppArchivosCall service;

    public AppArchivosService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppArchivosCall.class);
    }

    public void consultarCategoriasModulo(String modulo, Callback<List<String>> callback) {
        Call<List<String>> respuesta = this.service.consultarCategoriasModulo(modulo, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }

    public void consultarArchivosModuloCategoria(String modulo, String categoria, Callback<List<AppArchivos>> callback) {
        Call<List<AppArchivos>> respuesta = this.service.consultarArchivosModuloCategoria(modulo, categoria, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }
}
