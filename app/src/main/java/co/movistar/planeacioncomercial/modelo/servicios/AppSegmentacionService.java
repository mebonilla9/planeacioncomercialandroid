package co.movistar.planeacioncomercial.modelo.servicios;

import android.content.Context;

import co.movistar.planeacioncomercial.modelo.entidades.AppSegmentacion;
import co.movistar.planeacioncomercial.modelo.servicios.call.AppSegmentacionCall;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by lord_nightmare on 14/08/17.
 */

public class AppSegmentacionService extends GenericoService {

    private AppSegmentacionCall service;

    public AppSegmentacionService(Context contexto) {
        super(contexto);
        this.service = this.retrofit.create(AppSegmentacionCall.class);
    }

    public void consultarInformacionDisponibilidad(String nit, String idAgente, Callback<AppSegmentacion> callback) {
        Call<AppSegmentacion> respuesta = this.service.consultaInformacion(nit, idAgente, PreferenciasUtil.obtenerPreferencias("auth-token", contexto));
        respuesta.enqueue(callback);
    }
}
