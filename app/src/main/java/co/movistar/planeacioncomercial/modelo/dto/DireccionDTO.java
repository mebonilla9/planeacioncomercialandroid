package co.movistar.planeacioncomercial.modelo.dto;

import java.io.Serializable;

/**
 * Created by lord_nightmare on 17/07/17.
 */

public class DireccionDTO implements Serializable {

    private String cruceVial;
    private String numeroViaPrincipal;
    private Boolean estePrincipal;
    private Boolean surPrincipal;
    private String numeroViaGeneradora;
    private Long seccionFinal;
    private Boolean esteGeneradora;
    private Boolean surGeneradora;
    private String ciudad;

    @Override
    public String toString() {
        StringBuilder armador = new StringBuilder();
        armador.append(this.getCruceVial());
        armador.append(" ");
        armador.append(this.getNumeroViaPrincipal());
        armador.append(" ");
        if (this.getEstePrincipal()) {
            armador.append("Este");
        }
        else if (this.getSurPrincipal()) {
            armador.append("Sur");
        }
        armador.append(" # ");
        armador.append(this.getNumeroViaGeneradora());
        armador.append(" - ");
        armador.append(this.getSeccionFinal());
        armador.append(" ");
        if (this.getEsteGeneradora()) {
            armador.append("Este");
        }
        else if (this.getSurGeneradora()) {
            armador.append("Sur");
        }
        armador.append(", ");
        armador.append(this.getCiudad());
        return armador.toString();
    }

    /**
     * @return the cruceVial
     */
    public String getCruceVial() {
        return cruceVial;
    }

    /**
     * @param cruceVial the cruceVial to set
     */
    public void setCruceVial(String cruceVial) {
        this.cruceVial = cruceVial;
    }

    /**
     * @return the numeroViaPrincipal
     */
    public String getNumeroViaPrincipal() {
        return numeroViaPrincipal;
    }

    /**
     * @param numeroViaPrincipal the numeroViaPrincipal to set
     */
    public void setNumeroViaPrincipal(String numeroViaPrincipal) {
        this.numeroViaPrincipal = numeroViaPrincipal;
    }

    /**
     * @return the estePrincipal
     */
    public Boolean getEstePrincipal() {
        return estePrincipal;
    }

    /**
     * @param estePrincipal the estePrincipal to set
     */
    public void setEstePrincipal(Boolean estePrincipal) {
        this.estePrincipal = estePrincipal;
    }

    /**
     * @return the surPrincipal
     */
    public Boolean getSurPrincipal() {
        return surPrincipal;
    }

    /**
     * @param surPrincipal the surPrincipal to set
     */
    public void setSurPrincipal(Boolean surPrincipal) {
        this.surPrincipal = surPrincipal;
    }

    /**
     * @return the numeroViaGeneradora
     */
    public String getNumeroViaGeneradora() {
        return numeroViaGeneradora;
    }

    /**
     * @param numeroViaGeneradora the numeroViaGeneradora to set
     */
    public void setNumeroViaGeneradora(String numeroViaGeneradora) {
        this.numeroViaGeneradora = numeroViaGeneradora;
    }

    /**
     * @return the seccionFinal
     */
    public Long getSeccionFinal() {
        return seccionFinal;
    }

    /**
     * @param seccionFinal the seccionFinal to set
     */
    public void setSeccionFinal(Long seccionFinal) {
        this.seccionFinal = seccionFinal;
    }

    /**
     * @return the esteGeneradora
     */
    public Boolean getEsteGeneradora() {
        return esteGeneradora;
    }

    /**
     * @param esteGeneradora the esteGeneradora to set
     */
    public void setEsteGeneradora(Boolean esteGeneradora) {
        this.esteGeneradora = esteGeneradora;
    }

    /**
     * @return the surGeneradora
     */
    public Boolean getSurGeneradora() {
        return surGeneradora;
    }

    /**
     * @param surGeneradora the surGeneradora to set
     */
    public void setSurGeneradora(Boolean surGeneradora) {
        this.surGeneradora = surGeneradora;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
