package co.movistar.planeacioncomercial.modelo.servicios.call;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.dto.InformacionAltasGeneralDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppAltas;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by Lord_Nightmare on 18/11/16.
 */

public interface AppAltasCall {

    @GET("altas/consultar/{tipo}/{idUsuario}")
    Call<InformacionAltasGeneralDTO> consultarInformacionAltas(@Path("tipo") String tipo, @Path("idUsuario") String idUsuario, @Header("Authorization") String token);

    @GET("altas/productos/{idUsuario}")
    Call<List<String>>listaProductosDisponibles(@Path("idUsuario") String idUsuario, @Header("Authorization") String token);
}
