package co.movistar.planeacioncomercial.negocio.utilidades;

import java.io.IOException;
import java.lang.annotation.Annotation;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Lord_Nightmare on 25/11/16.
 */

public class RetrofitErrorUtil {

    public static PlaneacionComercialException convertirError(Retrofit retrofit, Response<?> response, EMensajes mensaje) {
        Converter<ResponseBody, PlaneacionComercialException> convertidor = retrofit.responseBodyConverter(PlaneacionComercialException.class, new Annotation[0]);
        PlaneacionComercialException planeacionComercialException;
        try {
            planeacionComercialException = convertidor.convert(response.errorBody());
        } catch (IOException e) {
            planeacionComercialException = new PlaneacionComercialException(mensaje);
        }
        return planeacionComercialException;
    }
}
