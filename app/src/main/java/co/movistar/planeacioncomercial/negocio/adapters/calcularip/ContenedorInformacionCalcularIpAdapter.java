package co.movistar.planeacioncomercial.negocio.adapters.calcularip;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppGestionado;
import co.movistar.planeacioncomercial.modelo.entidades.AppNoGestionado;
import co.movistar.planeacioncomercial.negocio.constantes.ETipoCalcularIp;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by Lord_Nightmare on 10/02/17.
 */

public class ContenedorInformacionCalcularIpAdapter extends RecyclerView.Adapter<ContenedorInformacionCalcularIpAdapter.ViewHolder> {

    private List<AppGestionado> dataSetGestionado;
    private List<AppNoGestionado> dataSetNoGestionado;
    private Context context;
    private ETipoCalcularIp tipo;

    private SparseBooleanArray expandState = new SparseBooleanArray();

    public ContenedorInformacionCalcularIpAdapter(Context context, ETipoCalcularIp tipo) {
        this.context = context;
        this.tipo = tipo;
        switch (this.tipo) {
            case TIPO_APP_GESTIONADO:
                dataSetGestionado = new ArrayList<>();
                break;
            case TIPO_APP_NO_GESTIONADO:
                dataSetNoGestionado = new ArrayList<>();
                break;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calcular_ip, parent, false));
        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (tipo) {
            case TIPO_APP_GESTIONADO:
                generarValoresGestionado(holder, position);
                break;
            case TIPO_APP_NO_GESTIONADO:
                generarValoresNoGestionado(holder, position);
                break;
        }
        holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.primary_dark));
        holder.rlTituloExpandir.setBackgroundColor(ContextCompat.getColor(context, R.color.primary));
        holder.btnExpandir.setRotation(expandState.get(position) ? 180f : 0f);
        asignarEventosHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return obtenerTamanoDataSetActivo();
    }

    private void generarValoresGestionado(ViewHolder holder, int position) {
        AppGestionado appGestionado = this.dataSetGestionado.get(position);
        holder.txtSegmento.setText(appGestionado.getSegmento() + " - " + appGestionado.getVariable());
        holder.txtNvariable.setVisibility(View.VISIBLE);
        holder.txtNvariable.setText(appGestionado.getnVariable());
        holder.txtVariable.setVisibility(View.VISIBLE);
        holder.txtVariable.setText(appGestionado.getVariable());
        holder.txtMetasMes.setText(appGestionado.getMeta() == null ? "" : FormularioUtil.convertirFormatoMiles(appGestionado.getMeta().longValue()+""));
        holder.txtCumplimiento.setText(appGestionado.getCumplimiento() == null ? "" : FormularioUtil.convertirCumplimientoPorcentaje(appGestionado.getCumplimiento()));
        holder.txtPesoEsquema.setText(appGestionado.getPesoEsquema() == null ? "" : FormularioUtil.convertirCumplimientoPorcentaje(appGestionado.getPesoEsquema()));
        holder.txtIpVariable.setText(appGestionado.getIpVariable() == null ? "" : FormularioUtil.convertirCumplimientoPorcentaje(appGestionado.getIpVariable()));
        holder.txtPdcVentas.setText(appGestionado.getEjecucion() == null ? "" : appGestionado.getEjecucion().toString());

        if (appGestionado.getVariable().equals("Cambios de Plan")) {
            holder.txtPdcVentas.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        }
    }

    private void generarValoresNoGestionado(ViewHolder holder, int position) {
        AppNoGestionado appNoGestionado = this.dataSetNoGestionado.get(position);
        holder.txtSegmento.setText(appNoGestionado.getVariable());
        holder.txtNvariable.setText("");
        holder.txtVariable.setText("");
        holder.txtMetasMes.setText(appNoGestionado.getMeta() == null ? "" : FormularioUtil.convertirFormatoMiles(appNoGestionado.getMeta().toString()));
        holder.txtCumplimiento.setText(appNoGestionado.getCumplimiento() == null ? "" : FormularioUtil.convertirCumplimientoPorcentaje(appNoGestionado.getCumplimiento()));
        holder.txtPesoEsquema.setText(appNoGestionado.getPesoEsquema() == null ? "" : FormularioUtil.convertirCumplimientoPorcentaje(appNoGestionado.getPesoEsquema()));
        holder.txtIpVariable.setText(appNoGestionado.getIpVariable() == null ? "" : FormularioUtil.convertirCumplimientoPorcentaje(appNoGestionado.getIpVariable()));
        holder.txtPdcVentas.setText(appNoGestionado.getEjecucion() == null ? "" : appNoGestionado.getEjecucion().toString());
    }

    private void asignarEventosHolder(final ViewHolder holder, final int position) {
        TextWatcher cambioPdcVentas = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if ((charSequence.length() > 0 && !charSequence.toString().startsWith("-")) || (charSequence.toString().startsWith("-")) && charSequence.length() > 1) {
                    switch (tipo) {
                        case TIPO_APP_GESTIONADO:
                            dataSetGestionado.get(position).setEjecucion(Long.parseLong(charSequence.toString()));
                            break;
                        case TIPO_APP_NO_GESTIONADO:
                            dataSetNoGestionado.get(position).setEjecucion(Long.parseLong(charSequence.toString()));
                            break;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };

        View.OnClickListener clickExpandir = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onClickButton(holder);
            }
        };

        holder.txtPdcVentas.addTextChangedListener(cambioPdcVentas);
        holder.btnExpandir.setOnClickListener(clickExpandir);
    }

    public void modificarDatos(ETipoCalcularIp tipo, List lista) {
        switch (tipo) {
            case TIPO_APP_GESTIONADO:
                dataSetGestionado.clear();
                dataSetGestionado = new ArrayList<>();
                dataSetGestionado.addAll(lista);
                break;
            case TIPO_APP_NO_GESTIONADO:
                dataSetNoGestionado.clear();
                dataSetGestionado = new ArrayList<>();
                dataSetNoGestionado.addAll(lista);
                break;
        }
        for (int i = 0; i < lista.size(); i++) {
            expandState.append(i, false);
        }
        notifyDataSetChanged();
    }

    private int obtenerTamanoDataSetActivo() {
        int tamano = 0;
        switch (this.tipo) {
            case TIPO_APP_GESTIONADO:
                tamano = dataSetGestionado.size();
                break;
            case TIPO_APP_NO_GESTIONADO:
                tamano = dataSetNoGestionado.size();
                break;
        }
        return tamano;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtVariable;
        private TextView txtNvariable;
        private TextView txtSegmento;
        private TextView txtMetasMes;
        private TextView txtCumplimiento;
        private TextView txtPesoEsquema;
        private TextView txtIpVariable;
        private EditText txtPdcVentas;

        private RelativeLayout rlTituloExpandir;
        private RelativeLayout btnExpandir;

        public ViewHolder(View itemView) {
            super(itemView);
            txtVariable = (TextView) itemView.findViewById(R.id.txtVariable);
            txtNvariable = (TextView) itemView.findViewById(R.id.txtNvariable);
            txtSegmento = (TextView) itemView.findViewById(R.id.txtSegmento);
            txtMetasMes = (TextView) itemView.findViewById(R.id.txtMetasMes);
            txtCumplimiento = (TextView) itemView.findViewById(R.id.txtCumplimiento);
            txtPesoEsquema = (TextView) itemView.findViewById(R.id.txtPesoEsquema);
            txtIpVariable = (TextView) itemView.findViewById(R.id.txtIpVariable);
            txtPdcVentas = (EditText) itemView.findViewById(R.id.txtPdcVentas);
            rlTituloExpandir = (RelativeLayout) itemView.findViewById(R.id.rlTituloExpandir);
            btnExpandir = (RelativeLayout) itemView.findViewById(R.id.btnExpandir);
        }
    }
}
