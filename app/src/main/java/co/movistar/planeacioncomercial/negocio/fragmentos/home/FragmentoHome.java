package co.movistar.planeacioncomercial.negocio.fragmentos.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppNotificacion;
import co.movistar.planeacioncomercial.modelo.servicios.AppNotificacionService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 10/07/17.
 */

public class FragmentoHome extends Fragment {

    private android.widget.TextView txtMensajeNotificacion;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_home, container, false);
        this.txtMensajeNotificacion = (TextView) vista.findViewById(R.id.txtMensajeNotificacion);
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        consultarUltimaNotificacion();
        reDistribuirActivity();
    }

    private void consultarUltimaNotificacion() {
        Callback<AppNotificacion> callback = new Callback<AppNotificacion>() {
            @Override
            public void onResponse(Call<AppNotificacion> call, Response<AppNotificacion> response) {
                if (!response.isSuccessful()  || response.body() == null) {
                    return;
                }
                AppNotificacion ultimaNotificacion = response.body();
                txtMensajeNotificacion.setText(ultimaNotificacion.getNotificacion());
            }

            @Override
            public void onFailure(Call<AppNotificacion> call, Throwable t) {

            }
        };
        new AppNotificacionService(getActivity()).consultarUltimaNotificacion(callback);
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Planeación Comercial");
        home.getSupportActionBar().setSubtitle("");
    }
}
