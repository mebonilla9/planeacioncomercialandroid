package co.movistar.planeacioncomercial.negocio.fragmentos.contrasena;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.security.NoSuchAlgorithmException;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.AppUsuariosService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.CryptoUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 18/01/17.
 */

public class FragmentoCambiarContrasena extends Fragment {

    private EditText txtNuevaContrasena;
    private TextInputLayout tilNuevaContrasena;
    private EditText txtConfirmarContrasena;
    private TextInputLayout tilConfirmarContrasena;
    private FloatingActionButton fab;
    private ProgressDialog dialogo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_cambiar_contrasena, container, false);
        initComponents(vista);
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        asignarEventos();
        reDistribuirActivity();
    }

    private void initComponents(View vista) {
        this.tilConfirmarContrasena = (TextInputLayout) vista.findViewById(R.id.tilConfirmarContrasena);
        this.txtConfirmarContrasena = (EditText) vista.findViewById(R.id.txtConfirmarContrasena);
        this.tilNuevaContrasena = (TextInputLayout) vista.findViewById(R.id.tilNuevaContrasena);
        this.txtNuevaContrasena = (EditText) vista.findViewById(R.id.txtNuevaContrasena);
        HomeActivity home = (HomeActivity) getActivity();
        this.fab = home.getFabBotonFlotante();
        this.fab.show();
        home.getSupportActionBar().setTitle("Reestablecer contraseña");
    }

    private void asignarEventos() {
        View.OnClickListener clickCambiarContrasena = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!txtNuevaContrasena.getText().toString().equals(txtConfirmarContrasena.getText().toString())) {
                    AlertaUtil.mostrarAlerta(
                            "Error de contraseña",
                            "Los valores de los campos de contraseñas no coinciden",
                            null,
                            null,
                            getActivity()
                    );
                    return;
                }
                if(!FormularioUtil.validarIntegridadContrasena(txtNuevaContrasena.getText().toString())){
                    AlertaUtil.mostrarAlerta(
                            "Error de contraseña",
                            "La contraseña ingresada no coincide con los lineamientos de seguridad, ingresela nuevamente",
                            null,
                            null,
                            getActivity()
                    );
                    return;
                }
                dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
                enviarNuevaContrasena(txtNuevaContrasena.getText().toString());
            }
        };
        fab.setOnClickListener(clickCambiarContrasena);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals(txtNuevaContrasena.getText().toString())) {
                    tilConfirmarContrasena.setError("Las contraseñas no coinciden");
                } else{
                    tilConfirmarContrasena.setError("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        this.txtConfirmarContrasena.addTextChangedListener(watcher);
    }

    private void enviarNuevaContrasena(String contrasena) {
        try{
            Callback<ResponseBody> callback = new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (!response.isSuccessful()) {
                        ProgresoUtil.ocultarDialogoProgreso(dialogo);
                        return;
                    }
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    AlertaUtil.mostrarAlerta(
                            "Modificacion de contraseña",
                            "La contraseña ha sido modificada correctamente",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    regresarPrincipal();
                                }
                            },
                            null,
                            getActivity());
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            };
            new AppUsuariosService(getActivity()).reestablecerContrasena(CryptoUtil.cifrarSha384(contrasena), callback);
        } catch(NoSuchAlgorithmException e){
            Log.e("Cifrado Contraseña","Algoritmo de cifrado no encontrado");
        }
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Cambiar Contraseña");
        home.getSupportActionBar().setSubtitle("");
    }

    private void regresarPrincipal() {
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Planeacion Comercial");
        ((HomeActivity) getActivity()).getFabBotonFlotante().hide();
        FragmentManager transaccion = getActivity().getSupportFragmentManager();
        transaccion.beginTransaction().remove(this).commit();
    }
}
