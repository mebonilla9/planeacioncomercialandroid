package co.movistar.planeacioncomercial.negocio.fragmentos.consultanit;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppSegmentacion;
import co.movistar.planeacioncomercial.modelo.servicios.AppSegmentacionService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 8/08/17.
 */

public class FragmentoConsultaNit extends Fragment {

    private TextInputLayout txtNit;
    private TextInputLayout txtIdAgente;
    private CardView cvInfoConsultaNit;

    private ProgressDialog dialogo;

    private FloatingActionButton fabActivity;
    private TextView valorNit;
    private android.widget.LinearLayout liNit;
    private TextView valorSegmento;
    private android.widget.LinearLayout liSegmento;
    private TextView valorContrato;
    private android.widget.LinearLayout liContrato;

    private TextView txtNoInfo;

    private String nitActual;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_consulta_nit, container, false);
        initComponents(vista);
        return vista;
    }

    private void initComponents(View vista) {
        this.cvInfoConsultaNit = (CardView) vista.findViewById(R.id.cvInfoConsultaNit);
        this.liContrato = (LinearLayout) vista.findViewById(R.id.liContrato);
        this.valorContrato = (TextView) vista.findViewById(R.id.valorContrato);
        this.liSegmento = (LinearLayout) vista.findViewById(R.id.liSegmento);
        this.valorSegmento = (TextView) vista.findViewById(R.id.valorSegmento);
        this.liNit = (LinearLayout) vista.findViewById(R.id.liNit);
        this.valorNit = (TextView) vista.findViewById(R.id.valorNit);
        this.txtIdAgente = (TextInputLayout) vista.findViewById(R.id.txtIdAgente);
        this.txtNit = (TextInputLayout) vista.findViewById(R.id.txtNit);
        this.txtNoInfo = (TextView) vista.findViewById(R.id.txtNoInfo);

        this.liNit.setVisibility(View.GONE);
        this.liContrato.setVisibility(View.GONE);
        this.liSegmento.setVisibility(View.GONE);
        this.txtNoInfo.setVisibility(View.GONE);
    }

    private void asignarEventos() {
        View.OnClickListener clickFlotante = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNit.getEditText().getText().toString().equals("") || txtIdAgente.getEditText().getText().toString().equals("")) {
                    AlertaUtil.mostrarAlerta("Consulta Nit", "Debe diligenciar correctamente los datos del formulario", null, null, getActivity());
                    return;
                }
                dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
                consultarNit(txtNit.getEditText().getText().toString(), txtIdAgente.getEditText().getText().toString());
            }
        };
        ((HomeActivity) getActivity()).getFabBotonFlotante().setOnClickListener(clickFlotante);

    }

    private void consultarNit(String nit, String idAgente) {
        Callback<AppSegmentacion> callback = new Callback<AppSegmentacion>() {
            @Override
            public void onResponse(Call<AppSegmentacion> call, Response<AppSegmentacion> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    nitPorDefecto();
                    return;
                }
                AppSegmentacion resultadoNit = response.body();
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                mostrarResultadoConsultaNit(resultadoNit);
            }

            @Override
            public void onFailure(Call<AppSegmentacion> call, Throwable t) {

            }
        };
        new AppSegmentacionService(getActivity()).consultarInformacionDisponibilidad(nit, idAgente, callback);
    }

    @Override
    public void onResume() {
        super.onResume();
        reDistribuirActividad();
        asignarEventos();
    }

    private void reDistribuirActividad() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().show();
        home.getFabBotonFlotante().setImageDrawable(DrawableCompat.wrap(VectorDrawableCompat.create(getResources(), R.drawable.find_file, null)));
        home.getFabBotonFlotante().setEnabled(true);
        home.getSupportActionBar().setTitle("Consulta por Nit");
        home.getSupportActionBar().setSubtitle("");
    }

    private void mostrarResultadoConsultaNit(AppSegmentacion data) {
        this.valorNit.setText("");
        this.valorSegmento.setText("");
        this.valorContrato.setText("");
        this.liNit.setVisibility(View.GONE);
        this.liSegmento.setVisibility(View.GONE);
        this.liContrato.setVisibility(View.GONE);
        this.txtNoInfo.setVisibility(View.GONE);
        if (data == null) {
            nitPorDefecto();
            return;
        }
        switch (data.getIndicador().intValue()) {
            case 1:
                this.liNit.setVisibility(View.VISIBLE);
                this.liSegmento.setVisibility(View.VISIBLE);
                this.liContrato.setVisibility(View.VISIBLE);
                this.valorNit.setText(data.getNit());
                this.valorSegmento.setText(data.getSegmento());
                this.valorContrato.setText(data.getConMarco());
                break;
            case 2:
                this.liNit.setVisibility(View.VISIBLE);
                this.liSegmento.setVisibility(View.VISIBLE);
                this.valorNit.setText(data.getNit());
                this.valorSegmento.setText(data.getSegmento());
                break;
            case 3:
                this.liNit.setVisibility(View.VISIBLE);
                this.liContrato.setVisibility(View.VISIBLE);
                this.valorNit.setText(data.getNit());
                this.valorContrato.setText(data.getConMarco());
                break;
            case 4:
                this.liNit.setVisibility(View.VISIBLE);
                this.valorNit.setText(data.getNit());
                this.txtNoInfo.setVisibility(View.VISIBLE);
                break;
            default:
                this.liNit.setVisibility(View.GONE);
                this.liContrato.setVisibility(View.GONE);
                this.liSegmento.setVisibility(View.GONE);
                break;
        }
    }

    private void nitPorDefecto(){
        nitActual = txtNit.getEditText().getText().toString();
        txtNit.getEditText().setText("");
        txtIdAgente.getEditText().setText("");
        liNit.setVisibility(View.VISIBLE);
        valorNit.setText(nitActual);
        txtNoInfo.setVisibility(View.VISIBLE);
    }
}
