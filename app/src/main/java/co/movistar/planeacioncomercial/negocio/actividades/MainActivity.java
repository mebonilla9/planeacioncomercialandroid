package co.movistar.planeacioncomercial.negocio.actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.autoridad.AutenticacionService;
import co.movistar.planeacioncomercial.negocio.utilidades.MacAddressUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;

public class MainActivity extends AppCompatActivity {

    private static final long SPLASH_SCREEN_DELAY = 3000;
    private ProgressDialog dialogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toast.makeText(this, "MacAddress: "+ MacAddressUtil.getMacAddress(), Toast.LENGTH_LONG).show();
        this.dialogo = ProgresoUtil.getDialogo(MainActivity.this,"Iniciando Sesión","Validando la integridad del token, espere por favor");
        crearTemporizador();
    }

    private void crearTemporizador() {
        TimerTask tareaTemporal = new TimerTask() {
            @Override
            public void run() {
                try {
                    // validacion de el token si existe
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    String token = PreferenciasUtil.obtenerPreferencias("auth-token", MainActivity.this);
                    if (token.isEmpty()) {
                        eliminarProgreso();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    }
                    validarTokenExistente();
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }
            }
        };
        Timer timer = new Timer();
        timer.schedule(tareaTemporal, SPLASH_SCREEN_DELAY);
    }

    private void validarTokenExistente() {
        eliminarProgreso();
        new AutenticacionService(MainActivity.this).obtenerFechaToken();
    }

    private void eliminarProgreso(){
    }
}
