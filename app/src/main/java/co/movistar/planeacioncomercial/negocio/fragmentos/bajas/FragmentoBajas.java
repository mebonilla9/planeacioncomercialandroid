package co.movistar.planeacioncomercial.negocio.fragmentos.bajas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppBajas;
import co.movistar.planeacioncomercial.modelo.servicios.AppBajasService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.bajas.SubFragmentoBajas;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentoBajas extends Fragment {
    private ProgressDialog dialogo;
    private List<AppBajas> listaProductos;
    private List<AppBajas> listaRegionalCanal;
    private String regionalCanalSeleccionado;
    private Spinner spRegionalCanal;
    private FragmentTabHost tabHost;
    private FrameLayout tabcontent;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_bajas, container, false);
        this.tabHost = vista.findViewById(android.R.id.tabhost);
        this.tabcontent = vista.findViewById(android.R.id.tabcontent);
        this.spRegionalCanal = vista.findViewById(R.id.spRegionalCanal);
        this.dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        this.tabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        consultarRegionesCanales();
        return vista;
    }

    public void onResume() {
        super.onResume();
        asignarEventos();
        reDistribuirActivity();
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Calidad");
    }

    private void consultarProductos() {
        new AppBajasService(getActivity()).consultarProductos(new Callback<List<AppBajas>>() {
            public void onResponse(Call<List<AppBajas>> call, Response<List<AppBajas>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    ProgresoUtil.ocultarDialogoProgreso(FragmentoBajas.this.dialogo);
                    AlertaUtil.mostrarAlerta("Bajas", "El usuario no posee información que visualizar para este módulo", null, null, FragmentoBajas.this.getActivity());
                    return;
                }
                ProgresoUtil.ocultarDialogoProgreso(FragmentoBajas.this.dialogo);
                listaProductos = response.body();
                cargarProductos();
            }

            public void onFailure(Call<List<AppBajas>> call, Throwable t) {
            }
        });
    }

    private void consultarRegionesCanales() {
        new AppBajasService(getActivity()).consultarRegionalCanal(new Callback<List<AppBajas>>() {
            public void onResponse(Call<List<AppBajas>> call, Response<List<AppBajas>> response) {
                if (response.isSuccessful() && !response.body().isEmpty() && response.body().size() != 0) {
                    listaRegionalCanal = response.body();
                    PreferenciasUtil.eliminarPreferencias("regional_actual_bajas", FragmentoBajas.this.getActivity());
                    cargarRegionalCanal();
                }
            }

            public void onFailure(Call<List<AppBajas>> call, Throwable t) {
            }
        });
    }

    private void cargarProductos() {
        this.tabcontent.removeAllViews();
        this.tabHost.clearAllTabs();
        this.tabHost.getTabWidget().removeAllViews();
        for (int i = 0; i < this.listaProductos.size(); i++) {
            AppBajas producto = this.listaProductos.get(i);
            TabSpec tabSpec = this.tabHost.newTabSpec(producto.getProducto()).setIndicator(producto.getProducto());
            tabSpec.setContent(new Intent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            this.tabHost.addTab(tabSpec, SubFragmentoBajas.class, obtenerArgumentosTabs(producto.getProducto()));
        }
        asignarEstiloTabs();
        this.tabHost.setCurrentTab(1);
        this.tabHost.setCurrentTab(0);
    }

    private void cargarRegionalCanal() {
        List<String> regionales = new ArrayList();
        regionales.add("Seleccione...");
        for (AppBajas regional : this.listaRegionalCanal) {
            if (regional.getRegional() != null) {
                regionales.add(regional.getRegional());
            }
        }
        this.spRegionalCanal.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, regionales));
        consultarProductos();
    }

    private Bundle obtenerArgumentosTabs(String producto) {
        Bundle argumentos = new Bundle();
        argumentos.putString("producto", producto);
        return argumentos;
    }

    private void asignarEstiloTabs() {
        for (int i = 0; i < this.tabHost.getTabWidget().getChildCount(); i++) {
            ((TextView) this.tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title)).setTextColor(getResources().getColor(R.color.icons));
        }
    }

    private void asignarEventos() {
        this.spRegionalCanal.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position > 0) {
                    FragmentoBajas.this.regionalCanalSeleccionado = ((AppBajas) FragmentoBajas.this.listaRegionalCanal.get(position - 1)).getRegional();
                    PreferenciasUtil.guardarPreferencias("regional_actual_bajas", FragmentoBajas.this.regionalCanalSeleccionado, FragmentoBajas.this.getActivity());
                    FragmentoBajas.this.consultarProductos();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
}
