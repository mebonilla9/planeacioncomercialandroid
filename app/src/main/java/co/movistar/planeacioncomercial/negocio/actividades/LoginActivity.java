package co.movistar.planeacioncomercial.negocio.actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.autoridad.AutenticacionService;
import co.movistar.planeacioncomercial.negocio.fragmentos.olvidocontrasena.FragmentoOlvidoContrasena;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.CryptoUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText txtCorreo;
    private TextInputLayout tilCorreo;
    private EditText txtContrasena;
    private TextInputLayout tilContrasena;
    private Button btnIniciarSesion;
    private LinearLayout activitylogin;
    private ProgressDialog dialogo;
    private TextView lblContrasena;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        iniciarComponentes();
        asignarEventos();
    }

    private void iniciarComponentes() {
        this.activitylogin = findViewById(R.id.activity_login);
        this.btnIniciarSesion = findViewById(R.id.btnIniciarSesion);
        this.tilContrasena = findViewById(R.id.tilContrasena);
        this.txtContrasena = findViewById(R.id.txtContrasena);
        this.tilCorreo = findViewById(R.id.tilCorreo);
        this.txtCorreo = findViewById(R.id.txtCorreo);
        this.lblContrasena = findViewById(R.id.lblContrasena);
    }

    private void asignarEventos() {
        View.OnClickListener clickLogin = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilCorreo.setError(null);
                tilContrasena.setError(null);
                dialogo = ProgresoUtil.getDialogo(LoginActivity.this, "Iniciando Sesión", "Estamos validando sus credenciales, por favor espere.");
                String correo = txtCorreo.getText().toString();
                String contrasena = txtContrasena.getText().toString();
                enviarAutenticacionUsuario(correo, contrasena);
            }
        };
        btnIniciarSesion.setOnClickListener(clickLogin);

        this.lblContrasena.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new FragmentoOlvidoContrasena().show(LoginActivity.this.getSupportFragmentManager(), "olvido_contrasena");
            }
        });
    }

    private void enviarAutenticacionUsuario(String correo, String contrasena) {
        try{
            Callback<ResponseBody> callback = new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (!response.isSuccessful()) {
                            ProgresoUtil.ocultarDialogoProgreso(dialogo);
                            String mensaje = response.errorBody().string();
                            AlertaUtil.mostrarAlerta(
                                    "Autenticación de Usuario",
                                    mensaje.startsWith("<html>") ? "Usuario no encontrado, Verifique su usuario y contraseña" : mensaje,
                                    null,
                                    null,
                                    LoginActivity.this);
                            tilCorreo.setError("Verifique su correo");
                            tilContrasena.setError("Verifique su contraseña");
                            return;
                        }
                        ProgresoUtil.ocultarDialogoProgreso(dialogo);
                        PreferenciasUtil.guardarPreferencias("auth-token", response.body().string(), LoginActivity.this);
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }

            };
            new AutenticacionService(LoginActivity.this).autenticarUsuario(correo, CryptoUtil.cifrarSha384(contrasena), callback);
        }catch(NoSuchAlgorithmException e){
            Log.e("Inicio de sesión","Algoritmo de cifrado no encontrado");
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
