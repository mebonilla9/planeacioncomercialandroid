package co.movistar.planeacioncomercial.negocio.fragmentos.niveldeservicio;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppServicio;
import co.movistar.planeacioncomercial.modelo.servicios.AppServicioService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.niveldeservicio.SubfragmentoNivelesServicio;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 17/04/17.
 */

public class FragmentoNivelesServicio extends Fragment {

    private android.support.v4.app.FragmentTabHost tabhost;
    private ProgressDialog dialogo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_nivel_de_servicio, container, false);
        initComponents(vista);
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        consultarNivelesServicio();
        return vista;
    }

    private void consultarNivelesServicio() {
        Callback<List<AppServicio>> callback = new Callback<List<AppServicio>>() {
            @Override
            public void onResponse(Call<List<AppServicio>> call, Response<List<AppServicio>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    AlertaUtil.mostrarAlerta("Indicadores de Servicio", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppServicio> listaServicios = response.body();
                listarItemsNivelServicio(listaServicios);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }
            @Override
            public void onFailure(Call<List<AppServicio>> call, Throwable t) {
                t.printStackTrace();
            }
        };
        new AppServicioService(getActivity()).consultarServicios(callback);
    }

    private void initComponents(View vista) {
        this.tabhost = (FragmentTabHost) vista.findViewById(android.R.id.tabhost);
        this.tabhost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
    }

    private void listarItemsNivelServicio(List<AppServicio> listaServicios) {
        this.tabhost.addTab(this.tabhost.newTabSpec("Presencial").setIndicator("Presencial"), SubfragmentoNivelesServicio.class, obtenerArgumentosTabs("PRESENCIAL", listaServicios));
        this.tabhost.addTab(this.tabhost.newTabSpec("Telefonico").setIndicator("Telefonico"), SubfragmentoNivelesServicio.class, obtenerArgumentosTabs("TELEFONICO", listaServicios));
        asignarEstiloTabs();
    }

    private Bundle obtenerArgumentosTabs(String tipo, List<AppServicio> listaServicios) {
        Bundle argumentos = new Bundle();
        argumentos.putString("tipo", tipo);
        argumentos.putSerializable("servicio", obtenerServiciosPresencial(tipo,listaServicios));
        return argumentos;
    }

    private ArrayList obtenerServiciosPresencial(String tipo, List<AppServicio> listaServicios){
        ArrayList<AppServicio> servicios = new ArrayList<>();
        for (AppServicio servicio: listaServicios){
            if (servicio.getCanal().equals(tipo)){
                servicios.add(servicio);
            }
        }
        return servicios;
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Indicadores de Servicio");
        home.getSupportActionBar().setSubtitle("");
    }

    private void asignarEstiloTabs() {
        for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) tabhost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.icons));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        reDistribuirActivity();
    }
}
