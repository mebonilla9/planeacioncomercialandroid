package co.movistar.planeacioncomercial.negocio.constantes;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public enum EMensajes {

    ERROR_CAMPO_CORREO_VACIO(-1,"El campo del correo se encuentra vacio, debe diligenciarlo para iniciar sesión"),
    ERROR_CAMPO_CONTRASENA_VACIO(-1,"El campo de la contraseña se encuentra vacio, debe diligenciarlo para iniciar sesión"),
    ERROR_AUTENTICACION_TOKEN(-3,"El token utilizado no es válido"),
    ERROR_EXPIRACION_TOKEN(-4,"La fecha de validez del token ha caducado"),
    ERROR_CONEXION_INTERNET(-5,"El dispositivo no se encuentra conectado a una red deinternet"),
    ERROR_GENERACION_TOKEN(-1,"El token no ha podido ser generado");

    private int codigo;
    private String descripcion;


    private EMensajes(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
