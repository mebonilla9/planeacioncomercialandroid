package co.movistar.planeacioncomercial.negocio.fragmentos.olvidocontrasena;

import android.content.res.ColorStateList;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.AppUsuariosService;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentoOlvidoContrasena extends DialogFragment {
    private Button btnIniciarSesion;
    private Button btnReestablecer;
    private TextInputLayout txtCorreoReestablecer;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_olvido_contrasena, container, false);
        this.btnReestablecer = vista.findViewById(R.id.btnReestablecer);
        this.btnIniciarSesion = vista.findViewById(R.id.btnIniciarSesion);
        this.txtCorreoReestablecer = vista.findViewById(R.id.txtCorreoReestablecer);
        if (VERSION.SDK_INT >= 21) {
            this.btnReestablecer.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.primary_light)));
        } else {
            ViewCompat.setBackgroundTintList(this.btnReestablecer, ColorStateList.valueOf(getResources().getColor(R.color.primary_light)));
        }
        getDialog().setCanceledOnTouchOutside(false);
        asignarEventos();
        return vista;
    }

    private void asignarEventos() {
        this.btnIniciarSesion.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                FragmentoOlvidoContrasena.this.dismiss();
            }
        });
        this.btnReestablecer.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String correo = FragmentoOlvidoContrasena.this.txtCorreoReestablecer.getEditText().getText().toString();
                if (FormularioUtil.validarIntegridadCorreo(correo)) {
                    FragmentoOlvidoContrasena.this.btnIniciarSesion.setEnabled(false);
                    FragmentoOlvidoContrasena.this.btnReestablecer.setEnabled(false);
                    FragmentoOlvidoContrasena.this.txtCorreoReestablecer.setEnabled(false);
                    new AppUsuariosService(FragmentoOlvidoContrasena.this.getActivity()).reestablecerContrasenaCorreo(correo, new Callback<ResponseBody>() {
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {
                                if (!response.isSuccessful()) {
                                    FragmentoOlvidoContrasena.this.txtCorreoReestablecer.setError(response.errorBody().string());
                                }
                                String mensaje = response.body().string();
                                if (response.code() == ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION) {
                                    FragmentoOlvidoContrasena.this.txtCorreoReestablecer.setError("");
                                    FragmentoOlvidoContrasena.this.dismiss();
                                    AlertaUtil.mostrarAlerta("Restauración de contraseña", mensaje, null, null, FragmentoOlvidoContrasena.this.getActivity());
                                    return;
                                }
                                FragmentoOlvidoContrasena.this.txtCorreoReestablecer.setError(mensaje);
                            } catch (IOException e) {
                            }
                        }

                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                        }
                    });
                    return;
                }
                FragmentoOlvidoContrasena.this.txtCorreoReestablecer.setError("Formato de correo invalido, intente nuevamente");
            }
        });
    }
}
