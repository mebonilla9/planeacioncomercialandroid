package co.movistar.planeacioncomercial.negocio.constantes;

/**
 * Created by Lord_Nightmare on 8/03/17.
 */

public enum ETipoCalcularIp {

    TIPO_APP_GESTIONADO(1),
    TIPO_APP_NO_GESTIONADO(2);

    private ETipoCalcularIp(int tipo) {
        this.tipo = tipo;
    }

    private int tipo;

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
