package co.movistar.planeacioncomercial.negocio.utilidades;

import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppMenu;

/**
 * Created by lord_nightmare on 18/04/17.
 */

public final class MenuUtil {

    public static void activarOpcionesMenu(Menu menu, List<AppMenu> menuUsuario) {
        for (AppMenu appMenu : menuUsuario) {
            habilitarItemMenu(appMenu, menu);
            activarOpcionesSubMenu(appMenu, menu);
        }
    }

    private static void activarOpcionesSubMenu(AppMenu appMenu, Menu menu) {
        if (!appMenu.getSubMenu().isEmpty()) {
            for (AppMenu appSubMenu : appMenu.getSubMenu()) {
                habilitarItemMenu(appSubMenu, menu);
            }
        }
    }

    private static void habilitarItemMenu(AppMenu menuData, Menu menu) {
        MenuItem item = null;
        String nombreMenu = menuData.getNombreMenu();
        switch (nombreMenu) {
            case "Comercial":
                item = menu.findItem(R.id.miCaComercial);
                break;
            case "Red":
                item = menu.findItem(R.id.miCaRed);
                break;
            case "Ventas":
                item = menu.findItem(R.id.miVentas);
                break;
            case "Disponibilidad de red":
                item = menu.findItem(R.id.miDisponibilidad);
                break;
            case "Calculador IP":
                item = menu.findItem(R.id.miCalcularIp);
                break;
            case "Liquidación":
                if (menuData.getMenuPadre() != 0) {
                    item = menu.findItem(R.id.miLiquidacion);
                    break;
                } else {
                    item = menu.findItem(R.id.miCaLiquidacion);
                    break;
                }
            case "Metas":
                item = menu.findItem(R.id.miMetas);
                break;
            case "Incentivos":
                item = menu.findItem(R.id.miIncentivos);
                break;
            case "Inventarios":
                item = menu.findItem(R.id.miInventarios);
                break;
            case "Mapas":
                item = menu.findItem(R.id.miMapas);
                break;
            case "Proyección":
                item = menu.findItem(R.id.miProyeccion);
                break;
            case "Servicio":
                if (menuData.getMenuPadre() != 0) {
                    item = menu.findItem(R.id.miNivelesServicio);
                    break;
                } else {
                    item = menu.findItem(R.id.miCaServicio);
                    break;
                }
            case "Consulta Nit":
                item = menu.findItem(R.id.miConsultaNit);
                break;
            case "Oferta":
                item = menu.findItem(R.id.miOfertas);
                break;
            case "Comisiones":
                item = menu.findItem(R.id.miComisiones);
                break;
            case "Calidad":
                item = menu.findItem(R.id.miBajas);
                break;
            case "Fibra":
                item = menu.findItem(R.id.miFibra);
                break;
            case "Disponibilidad Fibra Optica":
                item = menu.findItem(R.id.miDisponibilidadFibra);
                break;
        }
        item.setVisible(true);
    }
}
