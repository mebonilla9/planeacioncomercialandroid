package co.movistar.planeacioncomercial.negocio.adapters.metas;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppPresupuestoComercial;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by Lord_Nightmare on 10/02/17.
 */

public class ContenedorInformacionMetasAdapter extends RecyclerView.Adapter<ContenedorInformacionMetasAdapter.ViewHolder>{

    private List<AppPresupuestoComercial> metas;

    public ContenedorInformacionMetasAdapter(List<AppPresupuestoComercial> metas) {
        this.metas = metas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_metas,parent,false);
        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppPresupuestoComercial meta = this.metas.get(position);
        holder.lblProductoMetas.setText(meta.getProducto());
        holder.lblValorPresupuesto.setText(FormularioUtil.convertirFormatoMiles(meta.getPresupuesto().toString()));
    }

    @Override
    public int getItemCount() {
        return this.metas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView lblProductoMetas;
        private TextView lblValorPresupuesto;

        public ViewHolder(View itemView) {
            super(itemView);
            lblProductoMetas = (TextView) itemView.findViewById(R.id.lblProductoMetas);
            lblValorPresupuesto = (TextView) itemView.findViewById(R.id.lblValorPresupuesto);
        }
    }
}
