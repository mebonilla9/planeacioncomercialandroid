package co.movistar.planeacioncomercial.negocio.adapters.comisiones;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.InformacionComisionesDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppCostoxAlta;

/**
 * Created by lord_nightmare on 21/10/17.
 */

public class ContenedorInformacionComisionesAdapter extends RecyclerView.Adapter<ContenedorInformacionComisionesAdapter.ViewHolder>{

    private Context context;
    private InformacionComisionesDTO informacionComisionesDTO;

    public ContenedorInformacionComisionesAdapter(Context context, InformacionComisionesDTO informacionComisionesDTO) {
        this.context = context;
        this.informacionComisionesDTO = informacionComisionesDTO;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_venta_contenedor,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        List<AppCostoxAlta> altasDto = this.informacionComisionesDTO.getListaComisiones().get(position);
        holder.lblTituloContenedor.setText(altasDto.get(0).getCanal());
        holder.rvItemsDetalle.setHasFixedSize(true);
        holder.rvItemsDetalle.setLayoutManager(new LinearLayoutManager(this.context));
        holder.rvItemsDetalle.setAdapter(new ContenedorDetalleComisionesAdapter(altasDto));
    }

    @Override
    public int getItemCount() {
        return this.informacionComisionesDTO.getListaComisiones().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView lblTituloContenedor;
        private RecyclerView rvItemsDetalle;

        public ViewHolder(View itemView) {
            super(itemView);
            this.lblTituloContenedor = itemView.findViewById(R.id.lblTituloContenedor);
            this.rvItemsDetalle = itemView.findViewById(R.id.rvItemsDetalle);
        }
    }

}
