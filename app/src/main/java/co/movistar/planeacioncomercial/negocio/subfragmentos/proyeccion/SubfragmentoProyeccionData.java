package co.movistar.planeacioncomercial.negocio.subfragmentos.proyeccion;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppAltas;
import co.movistar.planeacioncomercial.modelo.entidades.AppProyeccion;
import co.movistar.planeacioncomercial.modelo.servicios.AppAltasService;
import co.movistar.planeacioncomercial.modelo.servicios.AppProyeccionService;
import co.movistar.planeacioncomercial.negocio.adapters.proyeccion.ContenedorInformacionProyeccionAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 11/04/17.
 */

public class SubfragmentoProyeccionData extends Fragment {

    private ContenedorInformacionProyeccionAdapter proyeccionAdapter;
    private RecyclerView rvProteccionData;
    private ProgressDialog dialogo;
    private Bundle argumentos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_proyeccion_data, container, false);
        argumentos = getArguments();
        initComponents(vista);
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        consultarInformacionProyeccion();
        return vista;
    }

    private void initComponents(View vista) {
        this.rvProteccionData = (RecyclerView) vista.findViewById(R.id.rvProteccionData);
    }

    private void consultarInformacionProyeccion() {
        Callback<List<AppProyeccion>> callback = new Callback<List<AppProyeccion>>() {
            @Override
            public void onResponse(Call<List<AppProyeccion>> call, Response<List<AppProyeccion>> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppProyeccion> listaProyecciones = response.body();
                cargarInformacionLista(listaProyecciones);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }

            @Override
            public void onFailure(Call<List<AppProyeccion>> call, Throwable t) {

            }
        };
        new AppProyeccionService(getActivity()).listaProyeccionProductos(argumentos.getString("producto"),callback);
    }

    private void cargarInformacionLista(List<AppProyeccion> listaProyecciones) {
        this.proyeccionAdapter = new ContenedorInformacionProyeccionAdapter(listaProyecciones);
        rvProteccionData.setAdapter(proyeccionAdapter);
        rvProteccionData.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvProteccionData.setLayoutManager(layoutManager);
    }
}
