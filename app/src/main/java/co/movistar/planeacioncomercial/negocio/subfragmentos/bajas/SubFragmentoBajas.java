package co.movistar.planeacioncomercial.negocio.subfragmentos.bajas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.cast.TextTrackStyle;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppBajas;
import co.movistar.planeacioncomercial.modelo.servicios.AppBajasService;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubFragmentoBajas extends Fragment {

    private List<AppBajas> listaBajas;
    private LinearLayout lnCabecera;
    private LinearLayout lnCuerpo;
    private String producto;
    private String regionalCanal;
    private LinearLayout tblDatosBajas;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_bajas, container, false);
        this.tblDatosBajas = vista.findViewById(R.id.tblDatosBajas);
        this.lnCuerpo = vista.findViewById(R.id.lnCuerpo);
        this.lnCabecera = vista.findViewById(R.id.lnCabecera);
        this.lnCabecera.setVisibility(View.GONE);
        this.producto = getArguments().getString("producto");
        this.regionalCanal = PreferenciasUtil.obtenerPreferencias("regional_actual_bajas", getActivity());
        if (this.regionalCanal != null) {
            consultarBajasProductoRegional();
        }
        return vista;
    }

    private void consultarBajasProductoRegional() {
        new AppBajasService(getActivity()).consultarBajas(this.producto, this.regionalCanal, new Callback<List<AppBajas>>() {
            public void onResponse(Call<List<AppBajas>> call, Response<List<AppBajas>> response) {
                if (response.isSuccessful() && !response.body().isEmpty() && response.body().size() != 0) {
                    SubFragmentoBajas.this.listaBajas = response.body();
                    SubFragmentoBajas.this.cargarTablaBajasDinamica();
                }
            }

            public void onFailure(Call<List<AppBajas>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void cargarTablaBajasDinamica() {
        cargarCabeceraTabla();
        cargarCuerpoTabla();
        estilizarFilas();
    }

    private void cargarCabeceraTabla() {
        if (this.lnCabecera.getChildCount() > 0) {
            this.lnCabecera.removeAllViews();
        }
        TextView cabeceraFecha = new TextView(getActivity());
        cabeceraFecha.setLayoutParams(generarLayoutParams(0, -2, 2.0f));
        cabeceraFecha.setTextColor(ContextCompat.getColor(getActivity(), R.color.icons));
        cabeceraFecha.setText("FECHA");
        cabeceraFecha.setTextSize(14.0f);
        this.lnCabecera.addView(cabeceraFecha);
        for (String periodo : this.listaBajas.get(0).getPeriodos().keySet()) {
            TextView campo = new TextView(getActivity());
            campo.setLayoutParams(generarLayoutParams(0, -2, TextTrackStyle.DEFAULT_FONT_SCALE));
            campo.setTextColor(ContextCompat.getColor(getActivity(), R.color.icons));
            campo.setText(periodo);
            campo.setGravity(17);
            campo.setTextSize(14.0f);
            this.lnCabecera.addView(campo);
        }
        TextView cabeceraPromedio = new TextView(getActivity());
        cabeceraPromedio.setLayoutParams(generarLayoutParams(0, -2, TextTrackStyle.DEFAULT_FONT_SCALE));
        cabeceraPromedio.setTextColor(ContextCompat.getColor(getActivity(), R.color.icons));
        cabeceraPromedio.setGravity(17);
        cabeceraPromedio.setText("PAIS");
        cabeceraPromedio.setTextSize(14.0f);
        this.lnCabecera.addView(cabeceraPromedio);
        TextView cabeceraPais = new TextView(getActivity());
        cabeceraPais.setLayoutParams(generarLayoutParams(0, -2, TextTrackStyle.DEFAULT_FONT_SCALE));
        cabeceraPais.setTextColor(ContextCompat.getColor(getActivity(), R.color.icons));
        cabeceraPais.setGravity(17);
        cabeceraPais.setText("PROM");
        cabeceraPais.setTextSize(13.0f);
        this.lnCabecera.addView(cabeceraPais);
        this.lnCabecera.setVisibility(View.VISIBLE);
    }

    private void cargarCuerpoTabla() {
        for (AppBajas baja : this.listaBajas) {
            LinearLayout lnFila = new LinearLayout(getActivity());
            lnFila.setLayoutParams(generarLayoutParams(-1, -2, 0.0f));
            lnFila.setPadding(0, 5, 0, 5);
            lnFila.setOrientation(LinearLayout.HORIZONTAL);
            TextView celdaFecha = new TextView(getActivity());
            celdaFecha.setLayoutParams(generarLayoutParams(0, -2, 2.0f));
            celdaFecha.setText(baja.getFechaVenta().toString());
            celdaFecha.setTextSize(14.0f);
            lnFila.addView(celdaFecha);
            for (String periodo : baja.getPeriodos().values()) {
                TextView celdaCampo = new TextView(getActivity());
                celdaCampo.setLayoutParams(generarLayoutParams(0, -2, TextTrackStyle.DEFAULT_FONT_SCALE));
                if (periodo == null || periodo.equals("")) {
                    periodo = "N/A";
                }
                celdaCampo.setText(periodo);
                celdaCampo.setGravity(17);
                celdaCampo.setTextSize(14.0f);
                lnFila.addView(celdaCampo);
            }
            TextView celdaPromedio = new TextView(getActivity());
            celdaPromedio.setLayoutParams(generarLayoutParams(0, -2, TextTrackStyle.DEFAULT_FONT_SCALE));
            celdaPromedio.setGravity(17);
            CharSequence promedio = (baja.getPromedio() == null || baja.getPromedio().equals("")) ? "N/A" : baja.getPromedio();
            celdaPromedio.setText(promedio);
            celdaPromedio.setTextSize(14.0f);
            lnFila.addView(celdaPromedio);
            TextView celdaPais = new TextView(getActivity());
            celdaPais.setLayoutParams(generarLayoutParams(0, -2, TextTrackStyle.DEFAULT_FONT_SCALE));
            celdaPais.setGravity(17);
            promedio = (baja.getPaisProm() == null || baja.getPaisProm().equals("")) ? "N/A" : baja.getPaisProm();
            celdaPais.setText(promedio);
            celdaPais.setTextSize(14.0f);
            lnFila.addView(celdaPais);
            this.lnCuerpo.addView(lnFila);
        }
    }

    private LayoutParams generarLayoutParams(int width, int height, float weight) {
        return new LayoutParams(width, height, weight);
    }

    private void estilizarFilas() {
        for (int i = 0; i < this.lnCuerpo.getChildCount(); i++) {
            if (i % 2 == 0) {
                this.lnCuerpo.getChildAt(i).setBackground(getResources().getDrawable(R.drawable.border_set));
            } else {
                this.lnCuerpo.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.primary_light_alpha));
            }
        }
    }
}
