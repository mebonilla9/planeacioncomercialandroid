package co.movistar.planeacioncomercial.negocio.fragmentos.incentivos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TabWidget;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.AppArchivosService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.incentivos.SubfragmentoIncentivos;
import co.movistar.planeacioncomercial.negocio.subfragmentos.metas.SubfragmentoMetasData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 17/05/17.
 */

public class FragmentoIncentivos extends Fragment {

    private FragmentTabHost tabHost;
    private final String modulo = "Incentivos";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_incentivos, container, false);
        initComponents(vista);
        consultarCategoriaModulo();
        return vista;
    }

    private void initComponents(View vista) {
        this.tabHost = (FragmentTabHost) vista.findViewById(android.R.id.tabhost);
        tabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
    }

    private void consultarCategoriaModulo() {
        Callback<List<String>> callback = new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (!response.isSuccessful() || response.body().isEmpty()) {
                    return;
                }
                List<String> categorias = response.body();
                cargarCategorias(categorias);
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

            }
        };
        new AppArchivosService(getActivity()).consultarCategoriasModulo(modulo, callback);
    }

    private void cargarCategorias(List<String> categorias) {
        for (int i = 0; i < categorias.size(); i++) {
            String categoria = categorias.get(i);
            tabHost.addTab(tabHost.newTabSpec(categoria).setIndicator(categoria), SubfragmentoIncentivos.class, obtenerArgumentosTabs(categoria));
        }
        asignarEstiloTabs();
        reDistribuirActivity();
    }

    private Bundle obtenerArgumentosTabs(String categoria) {
        Bundle argumentos = new Bundle();
        argumentos.putString("categoria", categoria);
        return argumentos;
    }

    private void asignarEstiloTabs() {
        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.icons));
        }
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Incentivos");
        home.getSupportActionBar().setSubtitle("");
        home.getFabBotonFlotante().hide();
    }

    @Override
    public void onResume() {
        super.onResume();
        reDistribuirActivity();
    }

}
