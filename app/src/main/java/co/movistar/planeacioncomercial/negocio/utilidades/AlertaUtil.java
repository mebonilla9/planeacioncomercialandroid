package co.movistar.planeacioncomercial.negocio.utilidades;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.actividades.MainActivity;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public final class AlertaUtil {

    /**
     * @param titulo
     * @param mensaje
     * @param aceptar
     * @param cancelar
     * @param contexto
     */
    public static void mostrarAlerta(String titulo, String mensaje,
                                     DialogInterface.OnClickListener aceptar,
                                     DialogInterface.OnClickListener cancelar,
                                     Context contexto) {
        generarAlerta(titulo, mensaje, aceptar, cancelar, contexto);
    }

    public static void mostrarAlertaAsync(final String titulo, final String mensaje,
                                          final DialogInterface.OnClickListener aceptar,
                                          final DialogInterface.OnClickListener cancelar,
                                          final Context contexto) {
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                generarAlerta(titulo, mensaje, aceptar, cancelar, contexto);
            }
        }).start();*/
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                generarAlerta(titulo, mensaje, aceptar, cancelar, contexto);
            }
        });

    }

    private static void generarAlerta(String titulo, String mensaje,
                                      DialogInterface.OnClickListener aceptar,
                                      DialogInterface.OnClickListener cancelar,
                                      Context contexto) {
        AlertDialog.Builder alerta = new AlertDialog.Builder(contexto);
        alerta.setTitle(titulo);
        alerta.setMessage(mensaje);
        alerta.setPositiveButton("Aceptar", aceptar);
        alerta.setNegativeButton("Cancelar", cancelar);
        alerta.show();
    }

    public static void mostrarNotificacion(Context contexto, String titulo, String mensaje) {
        Intent intent = new Intent(contexto, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(contexto, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(contexto);
        builder.setContentTitle(titulo);
        builder.setContentText(mensaje);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) contexto.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

}
