package co.movistar.planeacioncomercial.negocio.excepciones;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;

/**
 * Created by Lord_Nightmare on 25/10/16.
 */

public class PlaneacionComercialException extends Exception {

    private int codigo;
    private String mensaje;
    private Object datos;

    public PlaneacionComercialException(EMensajes mensaje) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
    }

    public PlaneacionComercialException(EMensajes mensaje, Object datos) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
        this.datos = datos;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getDatos() {
        return datos;
    }

    public void setDatos(Object datos) {
        this.datos = datos;
    }
}
