package co.movistar.planeacioncomercial.negocio.fragmentos.ventas;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppUsuarios;
import co.movistar.planeacioncomercial.modelo.servicios.AppAltasService;
import co.movistar.planeacioncomercial.modelo.servicios.AppPuntosService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.ventas.SubFragmentoVentasData;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gloria.garzon on 28/10/2016.
 */

public class FragmentoVenta extends Fragment {

    private FragmentTabHost tabHost;
    private FrameLayout tabcontent;
    private ProgressDialog dialogo;
    private LinearLayout lnSubordinados;
    private ImageButton btnLimpiarVentas;
    private Spinner spSubordinadosVentas;
    private Long idUsuario;

    private List<AppUsuarios> listaUsuarios;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_venta, container, false);
        this.spSubordinadosVentas = vista.findViewById(R.id.spSubordinadosVentas);
        this.btnLimpiarVentas = vista.findViewById(R.id.btnLimpiarVentas);
        this.lnSubordinados = vista.findViewById(R.id.lnSubordinados);
        this.tabcontent = vista.findViewById(android.R.id.tabcontent);
        initComponents(vista);
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        consultarSubordinados();
        return vista;
    }

    private void initComponents(View vista) {
        // obtener el control de pestañas desde el layout
        tabHost = (FragmentTabHost) vista.findViewById(android.R.id.tabhost);
        // configurar el control de pestañas utilizando la actividad que lo contiene, un gestor
        // de fragmentos y un contenedor definido en el layout.
        tabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        this.idUsuario = Long.parseLong(PreferenciasUtil.obtenerPreferencias("id_usuario", getActivity()));
    }

    private void consultarSubordinados() {
        Callback<List<AppUsuarios>> callback = new Callback<List<AppUsuarios>>() {
            @Override
            public void onResponse(Call<List<AppUsuarios>> call, Response<List<AppUsuarios>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    lnSubordinados.setVisibility(View.GONE);
                    btnLimpiarVentas.setVisibility(View.GONE);
                    cargarProductosUsuario();
                    return;
                }
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                listaUsuarios = response.body();
                cargarInfoSubordinados();
            }

            @Override
            public void onFailure(Call<List<AppUsuarios>> call, Throwable t) {

            }
        };
        new AppPuntosService(getActivity()).consultarSubordinados(callback);
    }

    private void cargarProductosUsuario() {
        Callback<List<String>> callback = new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<String> productos = response.body();
                listarProductosUsuario(productos);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                AlertaUtil.mostrarAlerta("Error Consulta Ventas", t.getMessage(), null, null, getActivity());
            }
        };
        new AppAltasService(getActivity()).listaProductosDisponibles(idUsuario.toString(), callback);
    }

    private void listarProductosUsuario(List<String> productos) {
        this.tabcontent.removeAllViews();
        this.tabHost.clearAllTabs();
        this.tabHost.setCurrentTab(1);
        // Adicion de las pestañas, segun los subfragmentos creados para el modulo
        for (int i = 0; i < productos.size(); i++) {
            String producto = productos.get(i);
            tabHost.addTab(tabHost.newTabSpec(producto).setIndicator(producto), SubFragmentoVentasData.class, obtenerArgumentosTabs(producto));
        }
        asignarEstiloTabs();
        this.tabHost.setCurrentTab(0);
    }

    private void cargarInfoSubordinados() {
        AppUsuarios usuarioIndice = new AppUsuarios(0L);
        usuarioIndice.setNombre("Elija a un usuario");
        List<AppUsuarios> listaSpinner = new ArrayList<>();
        listaSpinner.add(usuarioIndice);
        listaSpinner.addAll(listaUsuarios);
        spSubordinadosVentas.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                listaSpinner
        ));
        spSubordinadosVentas.setSelected(false);
    }

    private Bundle obtenerArgumentosTabs(String producto) {
        Bundle argumentos = new Bundle();
        argumentos.putString("producto", producto);
        argumentos.putLong("id_usuario", idUsuario);
        return argumentos;
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Ventas");
    }

    private void asignarEstiloTabs() {
        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.icons));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        reDistribuirActivity();
        asignarEventos();
    }

    private void asignarEventos() {
        spSubordinadosVentas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idUsuario = position == 0 ? Long.parseLong(PreferenciasUtil.obtenerPreferencias("id_usuario", getActivity())) : listaUsuarios.get(position - 1).getIdUsuario();
                cargarProductosUsuario();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnLimpiarVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spSubordinadosVentas.setSelection(0);
                idUsuario = Long.parseLong(PreferenciasUtil.obtenerPreferencias("id_usuario", getActivity()));
                cargarProductosUsuario();
            }
        });
    }
}
