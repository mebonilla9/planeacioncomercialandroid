package co.movistar.planeacioncomercial.negocio.utilidades;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Created by Lord_Nightmare on 10/03/17.
 */

public final class NullableUtil {

    public static boolean validarObjetoNulo(Object objeto) {
        Boolean estado = true;
        try {
            for (Field atributo : objeto.getClass().getDeclaredFields()) {
                atributo.setAccessible(true);
                if (atributo.get(objeto) != null) {
                    estado = false;
                }
                //atributo.setAccessible(false);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return estado;
    }

    public static boolean validarListaVacia(Collection coleccion){
        return coleccion.isEmpty();
    }
}
