package co.movistar.planeacioncomercial.negocio.fragmentos.calcularip;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.InformacionCalcularIpDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppGestionado;
import co.movistar.planeacioncomercial.modelo.entidades.AppNoGestionado;
import co.movistar.planeacioncomercial.modelo.servicios.AppCalcularIpService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.calcularip.SubfragmentoCalcularIpDataGestionado;
import co.movistar.planeacioncomercial.negocio.subfragmentos.calcularip.SubfragmentoCalcularIpDataNoGestionado;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 3/02/17.
 */

public class FragmentoCalcularIp extends Fragment {

    private final String tagGest = "Gestionado";
    private final String tagNoGest = "No Gestionado";

    private ProgressDialog dialogo;
    private FragmentTabHost tabhost;
    private FloatingActionButton fabActivity;
    private List<AppGestionado> listaGestionado;
    private List<AppNoGestionado> listaNoGestionado;
    private InformacionCalcularIpDTO infoCalcular;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_calcular_ip, container, false);
        initComponents(vista);
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        obtenerInformacionCalculador();
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        asignarEventos();
    }

    private void initComponents(View vista) {
        this.tabhost = (FragmentTabHost) vista.findViewById(android.R.id.tabhost);
        this.tabhost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        this.fabActivity = ((HomeActivity) getActivity()).getFabBotonFlotante();
        this.fabActivity.setImageDrawable(DrawableCompat.wrap(VectorDrawableCompat.create(getResources(), R.drawable.calculator, null)));
    }

    private void asignarEventos() {
        View.OnClickListener clickEnviarInformacion = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getChildFragmentManager();

                tabhost.setCurrentTabByTag(tagGest);
                SubfragmentoCalcularIpDataGestionado fragmentoGestionado = (SubfragmentoCalcularIpDataGestionado) manager.findFragmentByTag(tagGest);
                listaGestionado = fragmentoGestionado.getListaGestionados();

                tabhost.setCurrentTabByTag(tagNoGest);

                SubfragmentoCalcularIpDataNoGestionado fragmentoNoGestionado = (SubfragmentoCalcularIpDataNoGestionado) manager.findFragmentByTag(tagNoGest);
                listaNoGestionado = fragmentoNoGestionado.getListaNoGestionados();

                System.out.println(listaGestionado.size());
                System.out.println(listaNoGestionado.size());

                infoCalcular.setListaAppGestionado(listaGestionado);
                infoCalcular.setListaAppNoGestionado(listaNoGestionado);

                tabhost.setCurrentTabByTag(tagGest);

                dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
                ejecutarActualizacionCalcularIp(infoCalcular);
            }
        };

        this.fabActivity.setOnClickListener(clickEnviarInformacion);
    }

    private void ejecutarActualizacionCalcularIp(InformacionCalcularIpDTO infoCalcular) {
        Callback<InformacionCalcularIpDTO> callback = new Callback<InformacionCalcularIpDTO>() {
            @Override
            public void onResponse(Call<InformacionCalcularIpDTO> call, Response<InformacionCalcularIpDTO> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    try {
                        System.out.println(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }

                InformacionCalcularIpDTO infoActualizadoCalcular = response.body();
                actualizarTabsInformacionCalculador(infoActualizadoCalcular);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }

            @Override
            public void onFailure(Call<InformacionCalcularIpDTO> call, Throwable t) {

            }
        };
        new AppCalcularIpService(getActivity()).actualizarInformacionCalculador(infoCalcular, callback);
    }

    private void actualizarTabsInformacionCalculador(InformacionCalcularIpDTO infoActualizadoCalcular) {
        FragmentManager manager = getChildFragmentManager();
        SubfragmentoCalcularIpDataGestionado fragmentoGestionado = (SubfragmentoCalcularIpDataGestionado) manager.findFragmentByTag(tagGest);
        SubfragmentoCalcularIpDataNoGestionado fragmentoNoGestionado = (SubfragmentoCalcularIpDataNoGestionado) manager.findFragmentByTag(tagNoGest);

        fragmentoGestionado.getListaGestionados().clear();
        fragmentoGestionado.getListaGestionados().addAll(infoActualizadoCalcular.getListaAppGestionado());
        fragmentoGestionado.actualizarDataSet();

        fragmentoNoGestionado.getListaNoGestionados().clear();
        fragmentoNoGestionado.getListaNoGestionados().addAll(infoActualizadoCalcular.getListaAppNoGestionado());
        fragmentoGestionado.actualizarDataSet();

        reDistribuirActivity(obtenerPorcentajeTotal(infoActualizadoCalcular));

        AlertaUtil.mostrarAlerta("Calcular Ip", "Se han actualizado los valores por ejecución", null, null, getActivity());
    }

    private void obtenerInformacionCalculador() {
        Callback<InformacionCalcularIpDTO> callback = new Callback<InformacionCalcularIpDTO>() {
            @Override
            public void onResponse(Call<InformacionCalcularIpDTO> call, Response<InformacionCalcularIpDTO> response) {
                if (!response.isSuccessful()  || response.body() == null) {
                    AlertaUtil.mostrarAlerta("Calcular Ip", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                infoCalcular = response.body();
                generarTabsCalculador(infoCalcular);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }

            @Override
            public void onFailure(Call<InformacionCalcularIpDTO> call, Throwable t) {

            }
        };
        new AppCalcularIpService(getActivity()).consultarInformacionCalculador(callback);
    }

    private void generarTabsCalculador(InformacionCalcularIpDTO infoCalcular) {
        try {
            this.tabhost.addTab(this.tabhost.newTabSpec(tagGest).setIndicator(tagGest), SubfragmentoCalcularIpDataGestionado.class, exportarArgumentosTabs(tagGest, infoCalcular));
            this.tabhost.addTab(this.tabhost.newTabSpec(tagNoGest).setIndicator(tagNoGest), SubfragmentoCalcularIpDataNoGestionado.class, exportarArgumentosTabs(tagNoGest, infoCalcular));
            this.tabhost.setCurrentTabByTag(tagNoGest);
            reDistribuirActivity(obtenerPorcentajeTotal(infoCalcular));
            asignarEstiloTabs();
            Thread.sleep(2000);
            this.tabhost.setCurrentTabByTag(tagGest);
            this.fabActivity.show();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String obtenerPorcentajeTotal(InformacionCalcularIpDTO infoCalcular) {
        Double porcentaje = 0.0;
        for (AppGestionado gestionado : infoCalcular.getListaAppGestionado()){
            if (gestionado.getIpVariable()!= null){
                porcentaje += gestionado.getIpVariable();
            }
        }
        for (AppNoGestionado noGestionado : infoCalcular.getListaAppNoGestionado()){
            if (noGestionado.getIpVariable()!= null){
                porcentaje += noGestionado.getIpVariable();
            }
        }
        return convertirCumplimientoPorcentaje(porcentaje);
    }

    private Bundle exportarArgumentosTabs(String tab, InformacionCalcularIpDTO infoCalcular) {
        Bundle argumentos = new Bundle();
        switch (tab) {
            case tagGest:
                argumentos.putSerializable("data", (ArrayList<AppGestionado>) infoCalcular.getListaAppGestionado());
                break;
            case tagNoGest:
                argumentos.putSerializable("data", (ArrayList<AppNoGestionado>) infoCalcular.getListaAppNoGestionado());
                break;
        }
        return argumentos;
    }

    private void reDistribuirActivity(String porcentaje) {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Calcular Ip");
        home.getSupportActionBar().setSubtitle("Ip Total: " + porcentaje);
    }

    private void asignarEstiloTabs() {
        for (int i = 0; i < this.tabhost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) this.tabhost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.icons));
        }
    }

    private String convertirCumplimientoPorcentaje(Double valor) {
        valor = valor * 100;
        String[] seccionesCumplimiento = valor.toString().split(Pattern.quote("."));
        return seccionesCumplimiento[0] + "." + seccionesCumplimiento[1].substring(0, 1) + "%";
    }

}