package co.movistar.planeacioncomercial.negocio.subfragmentos.liquidacion;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppLiquidacionHist;
import co.movistar.planeacioncomercial.modelo.servicios.AppLiquidacionHistService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.adapters.liquidacion.ContenedorInformacionLiquidacionAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gloria.garzon on 15/02/2017.
 */

public class SubfragmentoLiquidacionDetalle extends Fragment {

    private TextView txtTituloProducto;
    private RecyclerView rvItemsProductoLiquidacion;
    private ProgressDialog dialogo;
    private Bundle argumentos;

    private String periodo;
    private String categoria;

    private ContenedorInformacionLiquidacionAdapter liquidacionAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_liquidacion_detalle, container, false);
        argumentos = getArguments();
        initComponents(vista);
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        habilitarCaracteristicas();
        consultarInformacionDetalleLiquidacion();
        return vista;
    }

    private void initComponents(View vista) {
        this.rvItemsProductoLiquidacion = (RecyclerView) vista.findViewById(R.id.rvItemsProductoLiquidacion);
        this.txtTituloProducto = (TextView) vista.findViewById(R.id.txtTituloProducto);
        this.periodo = argumentos.getString("periodo");
        this.categoria = argumentos.getString("categoria");
    }

    private void habilitarCaracteristicas() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Liquidación");
        home.getSupportActionBar().setSubtitle("Detalle de categoría");
    }

    private void consultarInformacionDetalleLiquidacion() {
        Callback<List<AppLiquidacionHist>> callback = new Callback<List<AppLiquidacionHist>>() {
            @Override
            public void onResponse(Call<List<AppLiquidacionHist>> call, Response<List<AppLiquidacionHist>> response) {
                if (!response.isSuccessful()){
                    AlertaUtil.mostrarAlerta("Liquidación", "El usuario no posee información que visualizar para esta sección", null, null, getActivity());
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppLiquidacionHist> listaDetalle = response.body();
                cargarListaProductosDetalle(listaDetalle);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }

            @Override
            public void onFailure(Call<List<AppLiquidacionHist>> call, Throwable t) {

            }
        };
        new AppLiquidacionHistService(getActivity()).consultarDetalleLiquidacion(categoria, periodo, callback);
    }

    private void cargarListaProductosDetalle(List<AppLiquidacionHist> listaDetalle) {
        this.txtTituloProducto.setText(listaDetalle.get(0).getCategoria());
        this.liquidacionAdapter = new ContenedorInformacionLiquidacionAdapter(getActivity(), listaDetalle);
        this.rvItemsProductoLiquidacion.setAdapter(liquidacionAdapter);
        this.rvItemsProductoLiquidacion.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        this.rvItemsProductoLiquidacion.setLayoutManager(layoutManager);
    }
}
