package co.movistar.planeacioncomercial.negocio.excepciones;

import java.io.IOException;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;

/**
 * Created by lord_nightmare on 17/10/17.
 */

public class SinConexionException extends IOException {

    private int codigo;
    private String mensaje;
    private Object datos;

    public SinConexionException(EMensajes mensaje) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
    }

    public SinConexionException(EMensajes mensaje, Object datos) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
        this.datos = datos;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getDatos() {
        return datos;
    }

    public void setDatos(Object datos) {
        this.datos = datos;
    }
}
