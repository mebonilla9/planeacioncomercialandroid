package co.movistar.planeacioncomercial.negocio.adapters.proyeccion;

import android.support.annotation.ColorInt;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.eralp.circleprogressview.CircleProgressView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppAltas;
import co.movistar.planeacioncomercial.modelo.entidades.AppProyeccion;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by lord_nightmare on 11/04/17.
 */

public class ContenedorInformacionProyeccionAdapter extends RecyclerView.Adapter<ContenedorInformacionProyeccionAdapter.ViewHolder> {

    private List<AppProyeccion> listaProyecciones;
    private static final int ANGULO_INICIO = 1;

    @ColorInt
    public static final int RED_DARK = 0xFFCC0000;
    @ColorInt
    public static final int GREEN_DARK = 0xFF669900;

    public ContenedorInformacionProyeccionAdapter(List<AppProyeccion> listaProyecciones) {
        this.listaProyecciones = listaProyecciones;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_proyeccion, parent, false);
        return new ContenedorInformacionProyeccionAdapter.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppProyeccion proyeccion = this.listaProyecciones.get(position);
        holder.txtSegmento.setText(proyeccion.getProducto() + " - " + proyeccion.getSegmento());
        holder.txtValorMetas.setText(FormularioUtil.convertirFormatoMiles(proyeccion.getPresupuesto().toString()));
        holder.txtValorAltas.setText(FormularioUtil.convertirFormatoMiles(proyeccion.getAltas().toString()));
        Double progreso = (proyeccion.getAltas() / proyeccion.getPresupuesto().doubleValue()) * 100;
        holder.cpvProgresoProyeccion.setTextEnabled(false);
        holder.cpvProgresoProyeccion.setProgressWithAnimation(progreso.intValue(), 2000);
        holder.txtValorPorcentaje.setText(progreso.intValue()+"%");
        holder.txtValorAlcorte.setText(FormularioUtil.convertirFormatoMiles(proyeccion.getAlcorte().toString()));
        if (progreso.intValue() > 99) {
            holder.cpvProgresoProyeccion.setTextColor(GREEN_DARK);
            holder.cpvProgresoProyeccion.setCircleColor(GREEN_DARK);
            holder.txtValorPorcentaje.setTextColor(GREEN_DARK);
        } else {
            holder.cpvProgresoProyeccion.setTextColor(RED_DARK);
            holder.cpvProgresoProyeccion.setCircleColor(RED_DARK);
            holder.txtValorPorcentaje.setTextColor(RED_DARK);
        }
    }

    @Override
    public int getItemCount() {
        return this.listaProyecciones.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtSegmento;
        private CircleProgressView cpvProgresoProyeccion;
        private TextView txtValorMetas;
        private TextView txtValorAltas;
        private TextView txtValorPorcentaje;
        private TextView txtValorAlcorte;

        public ViewHolder(View itemView) {
            super(itemView);

            txtSegmento = (TextView) itemView.findViewById(R.id.txtSegmento);
            cpvProgresoProyeccion = (CircleProgressView) itemView.findViewById(R.id.cpvProgresoProyeccion);
            cpvProgresoProyeccion.setTextEnabled(true);
            cpvProgresoProyeccion.setInterpolator(new AccelerateDecelerateInterpolator());
            cpvProgresoProyeccion.setStartAngle(ANGULO_INICIO);

            txtValorMetas = (TextView) itemView.findViewById(R.id.txtValorMetas);
            txtValorAltas = (TextView) itemView.findViewById(R.id.txtValorAltas);
            txtValorPorcentaje = (TextView) itemView.findViewById(R.id.txtValorPorcentaje);
            txtValorAlcorte = (TextView) itemView.findViewById(R.id.txtValorAlcorte);
        }
    }
}
