package co.movistar.planeacioncomercial.negocio.fragmentos.fibra;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.IOException;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.AppDireccionesFoService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.constantes.ETipoInmueble;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentoFibra extends Fragment {

    private String complemento;
    private String conjunto;
    private String direccion;
    private TextView lblComplemento;
    private TextView lblConjunto;
    private String municipio;
    private MaterialBetterSpinner spComplemento;
    private MaterialBetterSpinner spConjunto;
    private MaterialBetterSpinner spDireccion;
    private MaterialBetterSpinner spMunicipio;
    private Switch swTipo;
    private ETipoInmueble tipo;
    private TextView txtDireccionFinal;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_fibra, container, false);
        this.spComplemento = vista.findViewById(R.id.spComplemento);
        this.spDireccion = vista.findViewById(R.id.spDireccion);
        this.spConjunto = vista.findViewById(R.id.spConjunto);
        this.spMunicipio = vista.findViewById(R.id.spMunicipio);
        this.lblConjunto = vista.findViewById(R.id.lblConjunto);
        this.lblComplemento = vista.findViewById(R.id.lblComplemento);
        this.txtDireccionFinal = vista.findViewById(R.id.txtDireccionFinal);
        this.swTipo = vista.findViewById(R.id.swTipo);
        this.spComplemento.setVisibility(View.GONE);
        this.spDireccion.setVisibility(View.GONE);
        this.spConjunto.setVisibility(View.GONE);
        this.spMunicipio.setVisibility(View.GONE);
        this.tipo = ETipoInmueble.APARTAMENTO;
        this.lblConjunto.setText("Conjunto");
        this.lblComplemento.setText("Complemento");
        this.spConjunto.setHint("Seleccione un Conjunto");
        consultarInfoMunicipios();
        asignarEventos();
        return vista;
    }

    public void onResume() {
        super.onResume();
        reDistribuirActivity();
    }

    private void asignarEventos() {
        this.swTipo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FragmentoFibra.this.reestablecerSpinners();
                if (isChecked) {
                    tipo = ETipoInmueble.CASA;
                    lblConjunto.setText("Vía Principal");
                    lblComplemento.setText("Vía Generadora");
                    spConjunto.setHint("Seleccione un vía");
                    consultarInfoMunicipios();
                    return;
                }
                tipo = ETipoInmueble.APARTAMENTO;
                lblConjunto.setText("Conjunto");
                lblComplemento.setText("Complemento");
                spConjunto.setHint("Seleccione un Conjunto");
                consultarInfoMunicipios();
            }
        });
        TextWatcher verificador = new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null) {
                    if (spMunicipio.getText().hashCode() == charSequence.hashCode()) {
                        municipio = charSequence.toString();
                        consultarListaConjuntos();
                    } else if (spConjunto.getText().hashCode() == charSequence.hashCode()) {
                        conjunto = charSequence.toString();
                        consultarInfoComplementos();
                        if (tipo == ETipoInmueble.APARTAMENTO) {
                            consultarInfoDireccion();
                        }
                    } else if (spComplemento.getText().hashCode() == charSequence.hashCode()) {
                        complemento = charSequence.toString();
                        mostrarDireccionFinal();
                    } else if (spDireccion.getText().hashCode() == charSequence.hashCode()) {
                        direccion = charSequence.toString();
                        mostrarDireccionFinal();
                    }
                }
            }

            public void afterTextChanged(Editable editable) {
            }
        };
        this.spComplemento.addTextChangedListener(verificador);
        this.spDireccion.addTextChangedListener(verificador);
        this.spConjunto.addTextChangedListener(verificador);
        this.spMunicipio.addTextChangedListener(verificador);
    }

    private void mostrarDireccionFinal() {
        switch (this.tipo) {
            case CASA:
                consultarInfoDireccionFinal();
                return;
            case APARTAMENTO:
                String str;
                StringBuilder append = new StringBuilder();
                append.append(this.direccion != null ? this.direccion : "");
                append.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                if (this.complemento != null) {
                    str = this.complemento;
                } else {
                    str = "";
                }
                this.txtDireccionFinal.setText(append.append(str).toString());
                return;
            default:
                return;
        }
    }

    private void consultarInfoMunicipios() {
        Callback<List<String>> callback = new Callback<List<String>>() {
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    cargarListaMunicipios(response.body());
                }
            }

            public void onFailure(Call<List<String>> call, Throwable t) {
            }
        };
        new AppDireccionesFoService(getActivity()).consultarMunicipios(callback, this.tipo.toString());
    }

    private void consultarListaConjuntos() {
        new AppDireccionesFoService(getActivity()).consultarConjuntos(new Callback<List<String>>() {
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    FragmentoFibra.this.cargarListaConjuntos((List) response.body());
                }
            }

            public void onFailure(Call<List<String>> call, Throwable t) {
            }
        }, this.municipio, this.tipo.toString());
    }

    private void consultarInfoComplementos() {
        new AppDireccionesFoService(getActivity()).consultarComplementos(new Callback<List<String>>() {
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    FragmentoFibra.this.cargarListaComplementos((List) response.body());
                }
            }

            public void onFailure(Call<List<String>> call, Throwable t) {
            }
        }, this.conjunto, this.tipo.toString());
    }

    private void consultarInfoDireccion() {
        new AppDireccionesFoService(getActivity()).consultarDireccionConjunto(new Callback<List<String>>() {
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    FragmentoFibra.this.cargarListaDirecciones((List) response.body());
                }
            }
            public void onFailure(Call<List<String>> call, Throwable t) {
            }
        }, this.conjunto, this.tipo.toString());
    }

    private void consultarInfoDireccionFinal() {
        new AppDireccionesFoService(getActivity()).consultarDireccionComplemento(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        txtDireccionFinal.setText(response.body().string() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + complemento);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        }, this.complemento, this.tipo.toString());
    }

    private void cargarListaMunicipios(List<String> municipios) {
        this.spMunicipio.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, municipios));
        this.spMunicipio.setVisibility(View.VISIBLE);
    }

    private void cargarListaConjuntos(List<String> conjuntos) {
        this.spConjunto.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, conjuntos));
        this.spConjunto.setVisibility(View.VISIBLE);
    }

    private void cargarListaComplementos(List<String> complementos) {
        this.spComplemento.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, complementos));
        this.spComplemento.setVisibility(View.VISIBLE);
    }

    private void cargarListaDirecciones(List<String> direcciones) {
        this.spDireccion.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, direcciones));
        this.spDireccion.setVisibility(View.VISIBLE);
    }

    private void reestablecerSpinners() {
        this.spConjunto.setVisibility(View.GONE);
        this.spComplemento.setVisibility(View.GONE);
        this.spDireccion.setVisibility(View.GONE);
        this.spMunicipio.setAdapter(null);
        this.spConjunto.setAdapter(null);
        this.spComplemento.setAdapter(null);
        this.spDireccion.setAdapter(null);
        this.spMunicipio.setText("");
        this.spConjunto.setText("");
        this.spComplemento.setText("");
        this.spDireccion.setText("");
        this.municipio = null;
        this.conjunto = null;
        this.complemento = null;
        this.direccion = null;
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Fibra");
        home.getSupportActionBar().setSubtitle("");
    }
}
