package co.movistar.planeacioncomercial.negocio.fragmentos.comisiones;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.AppFinancieroService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.comisiones.SubFragmentoComisionesData;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 24/10/17.
 */

public class FragmentoComisiones extends Fragment {


    private Spinner spPeriodosFinanciero;
    private ProgressDialog dialogo;
    private FragmentTabHost tabhost;
    private List<String> listaPeriodos;
    private FrameLayout tabcontent;
    private String periodoSeleccionado;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_comisiones, container, false);
        this.tabcontent = vista.findViewById(android.R.id.tabcontent);
        this.tabhost = vista.findViewById(android.R.id.tabhost);
        this.tabhost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        this.spPeriodosFinanciero = vista.findViewById(R.id.spPeriodosFinanciero);
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        consultarPeriodos();
        cargarProductosPeriodo();
        return vista;
    }

    private void consultarPeriodos() {
        Callback<List<String>> callback = new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    AlertaUtil.mostrarAlerta("Comisiones", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;

                }
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                listaPeriodos = response.body();
                periodoSeleccionado = listaPeriodos.get(0);

                cargarInfoPeriodos();
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

            }
        };
        new AppFinancieroService(getActivity()).consultarPeriodos(callback);
    }

    private void cargarProductosPeriodo() {
        Callback<List<String>> callback = new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<String> productos = response.body();
                listarProductosPeriodo(productos);
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

            }
        };
        new AppFinancieroService(getActivity()).listaProductosDisponibles(periodoSeleccionado, callback);
    }

    private void listarProductosPeriodo(List<String> productos) {
        this.tabcontent.removeAllViews();
        this.tabhost.clearAllTabs();
        this.tabhost.setCurrentTab(1);
        for (int i = 0; i < productos.size(); i++) {
            String producto = productos.get(i);
            TabHost.TabSpec tabSpec = this.tabhost.newTabSpec(producto).setIndicator(producto);
            tabSpec.setContent(new Intent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            tabhost.addTab(tabSpec, SubFragmentoComisionesData.class, obtenerArgumentosTabs(producto));
        }
        asignarEstiloTabs();
    }


    private void asignarEstiloTabs() {
        for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
            TextView tv = tabhost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.icons));
        }
    }

    private Bundle obtenerArgumentosTabs(String producto) {
        Bundle argumentos = new Bundle();
        argumentos.putString("producto", producto);
        argumentos.putString("periodo", periodoSeleccionado);
        return argumentos;
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Comisiones");
        home.getSupportActionBar().setSubtitle("");
    }

    private void cargarInfoPeriodos() {
        spPeriodosFinanciero.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                listaPeriodos)
        );
        //cargarProductosPeriodo();
    }

    @Override
    public void onResume() {
        super.onResume();
        reDistribuirActivity();
        asignarEventos();
    }

    private void asignarEventos() {
        spPeriodosFinanciero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                periodoSeleccionado = (String) spPeriodosFinanciero.getSelectedItem();
                cargarProductosPeriodo();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
