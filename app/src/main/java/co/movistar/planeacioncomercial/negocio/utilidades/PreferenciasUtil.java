package co.movistar.planeacioncomercial.negocio.utilidades;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public final class PreferenciasUtil {

    public static void guardarPreferencias(String llave, String valor, Context contexto){
        SharedPreferences preferencias  = PreferenceManager.getDefaultSharedPreferences(contexto);
        preferencias.edit().putString(llave,valor).apply();

    }

    public static String obtenerPreferencias(String llave, Context contexto){
        SharedPreferences preferencias  = PreferenceManager.getDefaultSharedPreferences(contexto);
        return preferencias.getString(llave,"");
    }

    public static void eliminarPreferencias(String llave, Context contexto){
        SharedPreferences preferencias  = PreferenceManager.getDefaultSharedPreferences(contexto);
        preferencias.edit().remove(llave).apply();
    }

}
