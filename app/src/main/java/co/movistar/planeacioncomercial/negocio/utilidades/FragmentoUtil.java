package co.movistar.planeacioncomercial.negocio.utilidades;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.fragmentos.bajas.FragmentoBajas;
import co.movistar.planeacioncomercial.negocio.fragmentos.calcularip.FragmentoCalcularIp;
import co.movistar.planeacioncomercial.negocio.fragmentos.comisiones.FragmentoComisiones;
import co.movistar.planeacioncomercial.negocio.fragmentos.consultanit.FragmentoConsultaNit;
import co.movistar.planeacioncomercial.negocio.fragmentos.contrasena.FragmentoCambiarContrasena;
import co.movistar.planeacioncomercial.negocio.fragmentos.disponibilidad.FragmentoDisponibilidad;
import co.movistar.planeacioncomercial.negocio.fragmentos.disponibilidadfibra.FragmentoDisponibilidadFibra;
import co.movistar.planeacioncomercial.negocio.fragmentos.fibra.FragmentoFibra;
import co.movistar.planeacioncomercial.negocio.fragmentos.home.FragmentoHome;
import co.movistar.planeacioncomercial.negocio.fragmentos.incentivos.FragmentoIncentivos;
import co.movistar.planeacioncomercial.negocio.fragmentos.inventarios.FragmentoInventario;
import co.movistar.planeacioncomercial.negocio.fragmentos.mapas.FragmentoMapas;
import co.movistar.planeacioncomercial.negocio.fragmentos.ofertas.FragmentoOfertas;
import co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad.SubfragmentoDisponibilidadDireccion;
import co.movistar.planeacioncomercial.negocio.fragmentos.liquidacion.FragmentoLiquidacion;
import co.movistar.planeacioncomercial.negocio.fragmentos.metas.FragmentoMetas;
import co.movistar.planeacioncomercial.negocio.fragmentos.niveldeservicio.FragmentoNivelesServicio;
import co.movistar.planeacioncomercial.negocio.fragmentos.proyeccion.FragmentoProyeccion;
import co.movistar.planeacioncomercial.negocio.fragmentos.ventas.FragmentoVenta;
import co.movistar.planeacioncomercial.negocio.subfragmentos.archivos.SubfragmentoVisorArchivos;
import co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad.SubfragmentoDisponibiidadCaja;
import co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad.SubfragmentoDisponibilidadDepartamento;
import co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad.SubfragmentoDisponibilidadRegional;
import co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad.SubfragmentoDisponibilidadRespuesta;
import co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad.SubfragmentoDisponibilidadResumen;
import co.movistar.planeacioncomercial.negocio.subfragmentos.liquidacion.SubfragmentoLiquidacionDetalle;
import co.movistar.planeacioncomercial.negocio.subfragmentos.liquidacion.SubfragmentoLiquidacionMenu;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public final class FragmentoUtil {

    public static void obtenerFragmentoSeleccionado(int idMenu, Bundle argumentos, FragmentTransaction transaccion, int contenedor) {
        Fragment fragmento = null;
        String tag = null;
        switch (idMenu) {
            // intento de manejo de ids de menu para carga de fragmentos!
            case R.id.btnConsultaDepartamentoLocalidad:
            case R.id.btnConsultarCaja:
            case R.id.btnRegionalBogota:
            case R.id.btnRegionalCaribe:
            case R.id.btnRegionalNoroccidente:
            case R.id.btnRegionalNororiente:
            case R.id.btnRegionalSuroccidente:
            case R.id.btnRegionalSuroriente:
                fragmento = new SubfragmentoDisponibilidadRespuesta();
                tag = "Respuesta";
                break;
            case R.id.btnConsultaDetalleCaja:
                fragmento = new SubfragmentoDisponibiidadCaja();
                tag = "Caja";
                break;
            case R.id.btnConsultaDireccionCaja:
                fragmento = new SubfragmentoDisponibilidadDireccion();
                tag = "Dirección";
                break;
            case R.id.btnConsultaResumen:
                fragmento = new SubfragmentoDisponibilidadResumen();
                tag = "Resumen";
                break;
            case R.id.btnDepartamentosLocalidades:
                fragmento = new SubfragmentoDisponibilidadDepartamento();
                tag = "Departamento";
                break;
            case R.id.btnRegionales:
                fragmento = new SubfragmentoDisponibilidadRegional();
                tag = "Regional";
                break;
            case R.id.contenedorFragmentos:
                fragmento = new FragmentoHome();
                tag = "Home";
                break;
            case R.id.cvItemArchivo:
                fragmento = new SubfragmentoVisorArchivos();
                tag = "Departamento";
                break;
            case R.id.fabBotonFlotante:
                fragmento = obtenerFragmentoArgumentoBotonFlotante(argumentos.getString("subfragmento"));
                tag = argumentos.getString("subfragmento");
                break;
            case R.id.miAcerca:
                fragmento = new Fragment();
                tag = "Acerca";
                break;
            case R.id.miBajas:
                fragmento = new FragmentoBajas();
                tag = "Calidad";
                break;
            case R.id.miCalcularIp:
                fragmento = new FragmentoCalcularIp();
                tag = "Calcular Ip";
                break;
            case R.id.miCambiarContraseña:
                fragmento = new FragmentoCambiarContrasena();
                tag = "Contraseña";
                break;
            case R.id.miComisiones:
                fragmento = new FragmentoComisiones();
                tag = "Comisiones";
                break;
            case R.id.miConsultaNit:
                fragmento = new FragmentoConsultaNit();
                tag = "ConsultaNit";
                break;
            case R.id.miDisponibilidad:
                fragmento = new FragmentoDisponibilidad();
                tag = "Disponibilidad";
                break;
            case R.id.miFibra:
                fragmento = new FragmentoFibra();
                tag = "Fibra";
                break;
            case R.id.miDisponibilidadFibra:
                fragmento = new FragmentoDisponibilidadFibra();
                tag = "Fibra";
                break;
            case R.id.miIncentivos:
                fragmento = new FragmentoIncentivos();
                tag = "Incentivos";
                break;
            case R.id.miInventarios:
                fragmento = new FragmentoInventario();
                tag = "Incentivos";
                break;
            case R.id.miLiquidacion:
                fragmento = new FragmentoLiquidacion();
                tag = "Liquidacion";
                break;
            case R.id.miMapas:
                fragmento = new FragmentoMapas();
                tag = "Mapas";
                break;
            case R.id.miMetas:
                fragmento = new FragmentoMetas();
                tag = "Metas";
                break;
            case R.id.miNivelesServicio:
                fragmento = new FragmentoNivelesServicio();
                tag = "Servicio";
                break;
            case R.id.miOfertas:
                fragmento = new FragmentoOfertas();
                tag = "Ofertas";
                break;
            case R.id.miProyeccion:
                fragmento = new FragmentoProyeccion();
                tag = "Proyección";
                break;
            case R.id.miVentas:
                fragmento = new FragmentoVenta();
                tag = "Ventas";
                break;
        }
        if (argumentos != null) {
            fragmento.setArguments(argumentos);
        }
        lanzarNuevoFragmento(fragmento, transaccion, contenedor, tag);
    }

    private static void lanzarNuevoFragmento(Fragment fragmento, FragmentTransaction transaccion, int contenedor, String tag) {
        transaccion.replace(contenedor, fragmento, tag).addToBackStack(tag);
        transaccion.commit();
    }

    private static Fragment obtenerFragmentoArgumentoBotonFlotante(String subfragmento) {
        Fragment fragmento = new Fragment();
        switch (subfragmento) {
            case "liquidacionMenu":
                fragmento = new SubfragmentoLiquidacionMenu();
                break;
            case "liquidacionDetalle":
                fragmento = new SubfragmentoLiquidacionDetalle();
                break;
        }
        return fragmento;
    }


}
