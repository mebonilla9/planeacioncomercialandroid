package co.movistar.planeacioncomercial.negocio.constantes;

public enum ETipoInmueble {
    CASA("Casa"),
    APARTAMENTO("Apartamento");
    
    private String tipo;

    private ETipoInmueble(String tipo) {
        this.tipo = tipo;
    }

    public String toString() {
        return this.tipo;
    }
}
