package co.movistar.planeacioncomercial.negocio.subfragmentos.comisiones;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.InformacionComisionesDTO;
import co.movistar.planeacioncomercial.modelo.servicios.AppFinancieroService;
import co.movistar.planeacioncomercial.negocio.adapters.comisiones.ContenedorInformacionComisionesAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 24/10/17.
 */

public class SubFragmentoComisionesData extends Fragment {

    private String periodoActual;
    private String producto;

    private RecyclerView rvItemsFinancieroData;
    private ContenedorInformacionComisionesAdapter adapter;


    private InformacionComisionesDTO listaComisiones;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_comisiones_data, container, false);
        this.rvItemsFinancieroData = vista.findViewById(R.id.rvItemsFinancieroData);
        this.periodoActual = getArguments().getString("periodo");
        this.producto = getArguments().getString("producto");
        consultarInformacionComisiones();
        return vista;
    }

    private void consultarInformacionComisiones() {
        Callback<InformacionComisionesDTO> callback = new Callback<InformacionComisionesDTO>() {
            @Override
            public void onResponse(Call<InformacionComisionesDTO> call, Response<InformacionComisionesDTO> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                listaComisiones = response.body();
                cargarInformacionComisiones(listaComisiones);
            }

            @Override
            public void onFailure(Call<InformacionComisionesDTO> call, Throwable t) {

            }
        };
        new AppFinancieroService(getActivity()).listaCostoxAltaProductos(periodoActual, producto, callback);
    }

    private void cargarInformacionComisiones(InformacionComisionesDTO listaComisiones) {
        this.adapter = new ContenedorInformacionComisionesAdapter(getActivity(),listaComisiones);
        this.rvItemsFinancieroData.setLayoutManager(new LinearLayoutManager(getContext()));
        this.rvItemsFinancieroData.setHasFixedSize(true);
        this.rvItemsFinancieroData.setAdapter(adapter);
    }
}
