package co.movistar.planeacioncomercial.negocio.fragmentos.disponibilidadfibra;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidadFo;
import co.movistar.planeacioncomercial.modelo.servicios.AppDisponibilidadFoService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 5/03/18.
 */

public class FragmentoDisponibilidadFibra extends Fragment {

    private TextView txtDireccionFoFinal;
    private TextView txtXlibreCto;
    private TextView txtCto;
    private TextView txtPares;
    private MaterialBetterSpinner spRegionalFibra;
    private MaterialBetterSpinner spDepartamentoFibra;
    private MaterialBetterSpinner spLocalidadFibra;
    private MaterialBetterSpinner spBarrioFibra;
    private MaterialBetterSpinner spTipoPrincipalFibra;
    private MaterialBetterSpinner spReferenciaPrincipalFibra;
    private MaterialBetterSpinner spTipoCruceFibra;
    private MaterialBetterSpinner spReferenciaCruceFibra;
    private MaterialBetterSpinner spPlacaFibra;
    private MaterialBetterSpinner spComplementoFibra;
    private AppDisponibilidadFo disFibra;
    private FloatingActionButton fab;
    private android.widget.ImageView verificado;

    private ProgressDialog dialogo;
    private android.widget.Switch swDireccionFibra;
    private android.widget.LinearLayout lyPrincipalFirba;
    private android.widget.LinearLayout lyCruceFibra;
    private android.widget.LinearLayout lyPlacaFibra;
    private MaterialBetterSpinner spDireccionNoParamFibra;
    private android.widget.LinearLayout lyDirNoParamFibra;

    private Boolean parametrizado;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_disponibilidad_fibra, container, false);
        this.swDireccionFibra = vista.findViewById(R.id.swDireccionFibra);
        this.verificado = vista.findViewById(R.id.verificado);
        this.lyDirNoParamFibra = vista.findViewById(R.id.lyDirNoParamFibra);
        this.lyPlacaFibra = vista.findViewById(R.id.lyPlacaFibra);
        this.lyCruceFibra = vista.findViewById(R.id.lyCruceFibra);
        this.lyPrincipalFirba = vista.findViewById(R.id.lyPrincipalFirba);
        this.spDireccionNoParamFibra = vista.findViewById(R.id.spDireccionNoParamFibra);
        this.spComplementoFibra = vista.findViewById(R.id.spComplementoFibra);
        this.spPlacaFibra = vista.findViewById(R.id.spPlacaFibra);
        this.spReferenciaCruceFibra = vista.findViewById(R.id.spReferenciaCruceFibra);
        this.spTipoCruceFibra = vista.findViewById(R.id.spTipoCruceFibra);
        this.spReferenciaPrincipalFibra = vista.findViewById(R.id.spReferenciaPrincipalFibra);
        this.spTipoPrincipalFibra = vista.findViewById(R.id.spTipoPrincipalFibra);
        this.spBarrioFibra = vista.findViewById(R.id.spBarrioFibra);
        this.spLocalidadFibra = vista.findViewById(R.id.spLocalidadFibra);
        this.spDepartamentoFibra = vista.findViewById(R.id.spDepartamentoFibra);
        this.spRegionalFibra = vista.findViewById(R.id.spRegionalFibra);
        this.txtPares = vista.findViewById(R.id.txtPares);
        this.txtCto = vista.findViewById(R.id.txtCto);
        this.txtXlibreCto = vista.findViewById(R.id.txtXlibreCto);
        this.txtDireccionFoFinal = vista.findViewById(R.id.txtDireccionFoFinal);
        lyDirNoParamFibra.setVisibility(View.GONE);
        disFibra = new AppDisponibilidadFo();
        fab = ((HomeActivity) getActivity()).getFabBotonFlotante();
        fab.setVisibility(View.VISIBLE);
        FormularioUtil.aplicarColorBoton(fab, getActivity(), R.color.secondary_text);
        verificado.setVisibility(View.GONE);
        fab.setEnabled(false);
        parametrizado = true;
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        habilitarCaracteristicas();
        asignarEventos();
        reDistribuirActividad();
        cargarListasIniciales();
        fab = ((HomeActivity) getActivity()).getFabBotonFlotante();
        fab.setVisibility(View.VISIBLE);
        fab.setEnabled(false);
    }

    private void habilitarCaracteristicas() {
        spRegionalFibra.setVisibility(View.INVISIBLE);
        spDepartamentoFibra.setVisibility(View.INVISIBLE);
        spLocalidadFibra.setVisibility(View.INVISIBLE);
        spBarrioFibra.setVisibility(View.INVISIBLE);
        spTipoPrincipalFibra.setVisibility(View.INVISIBLE);
        spReferenciaPrincipalFibra.setVisibility(View.INVISIBLE);
        spTipoCruceFibra.setVisibility(View.INVISIBLE);
        spReferenciaCruceFibra.setVisibility(View.INVISIBLE);
        spPlacaFibra.setVisibility(View.INVISIBLE);
        spComplementoFibra.setVisibility(View.INVISIBLE);
        spDireccionNoParamFibra.setVisibility(View.INVISIBLE);
    }

    private void asignarEventos() {

        View.OnClickListener clickFlotante = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consultarDisponibilidadFibra();
            }
        };
        fab.setOnClickListener(clickFlotante);

        CompoundButton.OnCheckedChangeListener selector = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    parametrizado = false;
                    lyPrincipalFirba.setVisibility(View.GONE);
                    lyCruceFibra.setVisibility(View.GONE);
                    lyPlacaFibra.setVisibility(View.GONE);
                    lyDirNoParamFibra.setVisibility(View.VISIBLE);
                } else {
                    parametrizado = true;
                    lyPrincipalFirba.setVisibility(View.VISIBLE);
                    lyCruceFibra.setVisibility(View.VISIBLE);
                    lyPlacaFibra.setVisibility(View.VISIBLE);
                    lyDirNoParamFibra.setVisibility(View.GONE);
                }

                if (isChecked && spBarrioFibra.getText().toString().isEmpty()) {
                    FormularioUtil.aplicarColorBoton(fab, getActivity(), R.color.secondary_text);
                    fab.setEnabled(false);
                }

                if (isChecked && !spBarrioFibra.getText().toString().isEmpty()) {
                    FormularioUtil.aplicarColorBoton(fab, getActivity(), R.color.accent);
                    fab.setEnabled(true);
                }
                cargarListasIniciales();
            }
        };
        swDireccionFibra.setOnCheckedChangeListener(selector);

        TextWatcher eventoCambio = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().equals("")) {
                    return;
                }
                if (spRegionalFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setRegional(charSequence.toString());
                    restaurarSeleccion(1);
                    consultarDepartamentos(disFibra.getRegional());
                }
                if (spDepartamentoFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setDepartamento(charSequence.toString());
                    restaurarSeleccion(2);
                    consultarLocalidades(disFibra.getDepartamento());
                }
                if (spLocalidadFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setLocalidad(charSequence.toString());
                    restaurarSeleccion(3);
                    consultarBarrios(disFibra.getLocalidad());
                }
                if (spBarrioFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setBarrio(charSequence.toString());
                    if (swDireccionFibra.isChecked()) {
                        consultarDireccionesNoParametrizada();
                        return;
                    }
                    restaurarSeleccion(4);
                    consultarPrincipal(disFibra.getLocalidad(), disFibra.getBarrio());
                }
                //----
                // colocar spDireccion no param
                //----
                if (spDireccionNoParamFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setDireccion(charSequence.toString());
                    FormularioUtil.aplicarColorBoton(fab, getActivity(), R.color.accent);
                    fab.setEnabled(true);
                }
                if (spTipoPrincipalFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setPrincipal(charSequence.toString());
                    restaurarSeleccion(5);
                    consultarReferenciaPrincipal(disFibra.getLocalidad(), disFibra.getBarrio(), disFibra.getPrincipal());
                }
                if (spReferenciaPrincipalFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setReferenciaPrincipal(charSequence.toString());
                    restaurarSeleccion(6);
                    consultarCruce(disFibra.getLocalidad(), disFibra.getBarrio(), disFibra.getPrincipal(), disFibra.getReferenciaPrincipal());
                }
                if (spTipoCruceFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setCruce(charSequence.toString());
                    restaurarSeleccion(7);
                    consultarReferenciaCruce(disFibra.getLocalidad(), disFibra.getBarrio(), disFibra.getPrincipal(), disFibra.getReferenciaPrincipal(), disFibra.getCruce());
                }
                if (spReferenciaCruceFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setReferenciaCruce(charSequence.toString());
                    restaurarSeleccion(8);
                    consultarPlaca(disFibra.getLocalidad(), disFibra.getBarrio(), disFibra.getPrincipal(), disFibra.getReferenciaPrincipal(), disFibra.getCruce(), disFibra.getReferenciaCruce());
                }
                if (spPlacaFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setPlaca(charSequence.toString());
                    disFibra.setComplemento("");
                    restaurarSeleccion(9);
                    consultarComplemento(disFibra.getLocalidad(), disFibra.getBarrio(), disFibra.getPrincipal(), disFibra.getReferenciaPrincipal(), disFibra.getCruce(), disFibra.getReferenciaCruce(), disFibra.getPlaca());
                    FormularioUtil.aplicarColorBoton(fab, getActivity(), R.color.accent);
                    fab.setEnabled(true);
                }
                if (spComplementoFibra.getText().hashCode() == charSequence.hashCode()) {
                    disFibra.setComplemento(charSequence.toString());
                    FormularioUtil.aplicarColorBoton(fab, getActivity(), R.color.accent);
                    fab.setEnabled(true);
                }

                if (charSequence.hashCode() != spDireccionNoParamFibra.getText().hashCode() && charSequence.hashCode() != spComplementoFibra.getText().hashCode() && charSequence.hashCode() != spPlacaFibra.getText().hashCode()) {
                    FormularioUtil.aplicarColorBoton(fab, getActivity(), R.color.secondary_text);
                    fab.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        spRegionalFibra.addTextChangedListener(eventoCambio);
        spDepartamentoFibra.addTextChangedListener(eventoCambio);
        spLocalidadFibra.addTextChangedListener(eventoCambio);
        spBarrioFibra.addTextChangedListener(eventoCambio);
        spTipoPrincipalFibra.addTextChangedListener(eventoCambio);
        spReferenciaPrincipalFibra.addTextChangedListener(eventoCambio);
        spTipoCruceFibra.addTextChangedListener(eventoCambio);
        spReferenciaCruceFibra.addTextChangedListener(eventoCambio);
        spPlacaFibra.addTextChangedListener(eventoCambio);
        spComplementoFibra.addTextChangedListener(eventoCambio);
        spDireccionNoParamFibra.addTextChangedListener(eventoCambio);
    }

    private void consultarDisponibilidadFibra() {
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        Callback<AppDisponibilidadFo> callback = new Callback<AppDisponibilidadFo>() {
            @Override
            public void onResponse(Call<AppDisponibilidadFo> call, Response<AppDisponibilidadFo> response) {
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                if (!response.isSuccessful()) {
                    AlertaUtil.mostrarAlerta("Disponibilidad de Fibra optica", "No se ha encontrado disponibilidad", null, null, getActivity());
                    return;
                }
                AppDisponibilidadFo disponible = response.body();
                if (disponible != null) {
                    txtDireccionFoFinal.setText(disponible.getDireccion());
                    txtXlibreCto.setText(FormularioUtil.convertirCumplimientoPorcentaje(Double.parseDouble(disponible.getxLibreCto())));
                    txtPares.setText(disponible.getParesLibres() + "");
                    txtCto.setText(disponible.getCto());
                    verificado.setVisibility(View.VISIBLE);
                } else {
                    AlertaUtil.mostrarAlerta("Disponibilidad de Fibra optica", "No se ha encontrado disponibilidad", null, null, getActivity());
                    txtDireccionFoFinal.setText("Dirección");
                    txtXlibreCto.setText("");
                    txtCto.setText("");
                    verificado.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AppDisponibilidadFo> call, Throwable t) {

            }
        };

        if (swDireccionFibra.isChecked()) {
            new AppDisponibilidadFoService(getActivity()).consultarNoParametrizada(disFibra, callback);
        } else {

            new AppDisponibilidadFoService(getActivity()).consultarPorDireccion(disFibra, callback);
        }
    }

    private void cargarListasIniciales() {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaRegionales = response.body();
                indexarListaRegionalesSpinner(listaRegionales);
                spRegionalFibra.setVisibility(View.VISIBLE);
                spRegionalFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarRegionales(parametrizado, callback);
    }

    private void consultarDepartamentos(String regional) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaDepartamentos = response.body();
                indexarListaDepartamentosSpinner(listaDepartamentos);
                spDepartamentoFibra.setVisibility(View.VISIBLE);
                spDepartamentoFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarDepartamentos(regional, parametrizado, callback);
    }

    private void consultarLocalidades(String departamento) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaLocalidades = response.body();
                indexarListaLocalidadesSpinner(listaLocalidades);
                spLocalidadFibra.setVisibility(View.VISIBLE);
                spLocalidadFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarLocalidades(departamento, parametrizado, callback);
    }

    private void consultarBarrios(String localidad) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaBarrios = response.body();
                indexarListaBarriosSpinner(listaBarrios);
                spBarrioFibra.setVisibility(View.VISIBLE);
                spBarrioFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarBarrios(localidad, parametrizado, callback);
    }

    private void consultarPrincipal(String localidad, String barrio) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaTipoPrincipal = response.body();
                indexarListaTipoPrincipalSpinner(listaTipoPrincipal);
                spTipoPrincipalFibra.setVisibility(View.VISIBLE);
                spTipoPrincipalFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarPrincipal(localidad, barrio, callback);
    }

    private void consultarReferenciaPrincipal(String localidad, String barrio, String principal) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaRefPrincipal = response.body();
                indexarListaRefPrincipalSpinner(listaRefPrincipal);
                spReferenciaPrincipalFibra.setVisibility(View.VISIBLE);
                spReferenciaPrincipalFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarReferenciaPrincipal(localidad, barrio, principal, callback);
    }

    private void consultarCruce(String localidad, String barrio, String principal, String referenciaPrincipal) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaCruce = response.body();
                indexarListaCruceSpinner(listaCruce);
                spTipoCruceFibra.setVisibility(View.VISIBLE);
                spTipoCruceFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarCruce(localidad, barrio, principal, referenciaPrincipal, callback);
    }

    private void consultarReferenciaCruce(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaRefCruces = response.body();
                indexarListaRefCrucesSpinner(listaRefCruces);
                spReferenciaCruceFibra.setVisibility(View.VISIBLE);
                spReferenciaCruceFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarReferenciaCruce(localidad, barrio, principal, referenciaPrincipal, cruce, callback);
    }

    private void consultarPlaca(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaPlacas = response.body();
                indexarListaPlacasSpinner(listaPlacas);
                spPlacaFibra.setVisibility(View.VISIBLE);
                spPlacaFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarPlaca(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce, callback);
    }

    private void consultarComplemento(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce, String placa) {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaComplementos = response.body();
                indexarListaComplementosSpinner(listaComplementos);
                spComplementoFibra.setVisibility(View.VISIBLE);
                spComplementoFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarComplemento(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce, placa, callback);
    }

    private void consultarDireccionesNoParametrizada() {
        Callback<List<AppDisponibilidadFo>> callback = new Callback<List<AppDisponibilidadFo>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidadFo>> call, Response<List<AppDisponibilidadFo>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidadFo> listaDirecciones = response.body();
                indexarDireccionesNoParamSpinner(listaDirecciones);
                spDireccionNoParamFibra.setVisibility(View.VISIBLE);
                spDireccionNoParamFibra.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidadFo>> call, Throwable t) {

            }
        };
        new AppDisponibilidadFoService(getActivity()).consultarDireccionesNoParametrizada(disFibra, callback);
    }

    private void indexarDireccionesNoParamSpinner(List<AppDisponibilidadFo> listaDirecciones) {
        List<String> regionales = new ArrayList<>();
        for (AppDisponibilidadFo direccion : listaDirecciones) {
            if (direccion.getDireccion() != null && !direccion.getDireccion().isEmpty()) {
                regionales.add(direccion.getDireccion());
            }
        }
        spDireccionNoParamFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                regionales
        ));
    }

    private void indexarListaRegionalesSpinner(List<AppDisponibilidadFo> listaRegionales) {
        List<String> regionales = new ArrayList<>();
        for (AppDisponibilidadFo regional : listaRegionales) {
            if (regional.getRegional() != null && !regional.getRegional().isEmpty()) {
                regionales.add(regional.getRegional());
            }
        }
        spRegionalFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                regionales
        ));
    }

    private void indexarListaDepartamentosSpinner(List<AppDisponibilidadFo> listaDepartamentos) {
        List<String> departamentos = new ArrayList<>();
        for (AppDisponibilidadFo departamento : listaDepartamentos) {
            if (departamento.getDepartamento() != null && !departamento.getDepartamento().isEmpty()) {
                departamentos.add(departamento.getDepartamento());
            }
        }
        spDepartamentoFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                departamentos
        ));
    }

    private void indexarListaLocalidadesSpinner(List<AppDisponibilidadFo> listaLocalidades) {
        List<String> localidades = new ArrayList<>();
        for (AppDisponibilidadFo localidad : listaLocalidades) {
            if (localidad.getLocalidad() != null && !localidad.getLocalidad().equals("")) {
                localidades.add(localidad.getLocalidad());
            }
        }
        spLocalidadFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                localidades
        ));
    }


    private void indexarListaBarriosSpinner(List<AppDisponibilidadFo> listaBarrios) {
        List<String> barrios = new ArrayList<>();
        for (AppDisponibilidadFo barrio : listaBarrios) {
            if (barrio.getBarrio() != null && !barrio.getBarrio().isEmpty()) {
                barrios.add(barrio.getBarrio());
            }
        }
        spBarrioFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                barrios
        ));
    }

    private void indexarListaTipoPrincipalSpinner(List<AppDisponibilidadFo> listaTipoPrincipal) {
        List<String> principales = new ArrayList<>();
        for (AppDisponibilidadFo principal : listaTipoPrincipal) {
            if (principal.getPrincipal() != null && !principal.getPrincipal().isEmpty()) {
                principales.add(principal.getPrincipal());
            }
        }
        spTipoPrincipalFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                principales
        ));
    }

    private void indexarListaRefPrincipalSpinner(List<AppDisponibilidadFo> listaRefPrincipal) {
        List<String> referenciasPrincipal = new ArrayList<>();
        for (AppDisponibilidadFo refPrincipal : listaRefPrincipal) {
            if (refPrincipal.getReferenciaPrincipal() != null && !refPrincipal.getReferenciaPrincipal().isEmpty()) {
                referenciasPrincipal.add(refPrincipal.getReferenciaPrincipal());
            }
        }
        spReferenciaPrincipalFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                referenciasPrincipal
        ));
    }

    private void indexarListaCruceSpinner(List<AppDisponibilidadFo> listaCruce) {
        List<String> cruces = new ArrayList<>();
        for (AppDisponibilidadFo cruce : listaCruce) {
            if (cruce.getCruce() != null && !cruce.getCruce().isEmpty()) {
                cruces.add(cruce.getCruce());
            }
        }
        spTipoCruceFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                cruces
        ));
    }

    private void indexarListaRefCrucesSpinner(List<AppDisponibilidadFo> listaRefCruces) {
        List<String> referenciasCruces = new ArrayList<>();
        for (AppDisponibilidadFo refCruce : listaRefCruces) {
            if (refCruce.getReferenciaCruce() != null && !refCruce.getReferenciaCruce().isEmpty()) {
                referenciasCruces.add(refCruce.getReferenciaCruce());
            }
        }
        spReferenciaCruceFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                referenciasCruces
        ));
    }

    private void indexarListaPlacasSpinner(List<AppDisponibilidadFo> listaPlacas) {
        List<String> placas = new ArrayList<>();
        for (AppDisponibilidadFo placa : listaPlacas) {
            if (placa.getPlaca() != null && !placa.getPlaca().isEmpty()) {
                placas.add(placa.getPlaca());
            }
        }
        spPlacaFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                placas
        ));
    }

    private void indexarListaComplementosSpinner(List<AppDisponibilidadFo> listaComplementos) {
        List<String> complementos = new ArrayList<>();
        for (AppDisponibilidadFo complemento : listaComplementos) {
            if (complemento.getComplemento() != null && !complemento.getComplemento().isEmpty()) {
                complementos.add(complemento.getComplemento());
            }
        }
        spComplementoFibra.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                complementos
        ));
    }

    private void reDistribuirActividad() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Disponibilidad Fibra");
        home.getSupportActionBar().setSubtitle("");
        fab.setVisibility(View.VISIBLE);
    }

    private void restaurarSeleccion(int grado) {
        MaterialBetterSpinner[] sps = null;
        switch (grado) {
            case 1:
                sps = new MaterialBetterSpinner[]{
                        spDepartamentoFibra,
                        spLocalidadFibra,
                        spBarrioFibra,
                        spTipoPrincipalFibra,
                        spReferenciaPrincipalFibra,
                        spTipoCruceFibra,
                        spReferenciaCruceFibra,
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setDepartamento(null);
                disFibra.setLocalidad(null);
                disFibra.setBarrio(null);
                disFibra.setPrincipal(null);
                disFibra.setReferenciaPrincipal(null);
                disFibra.setCruce(null);
                disFibra.setReferenciaCruce(null);
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 2:
                sps = new MaterialBetterSpinner[]{
                        spLocalidadFibra,
                        spBarrioFibra,
                        spTipoPrincipalFibra,
                        spReferenciaPrincipalFibra,
                        spTipoCruceFibra,
                        spReferenciaCruceFibra,
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setLocalidad(null);
                disFibra.setBarrio(null);
                disFibra.setPrincipal(null);
                disFibra.setReferenciaPrincipal(null);
                disFibra.setCruce(null);
                disFibra.setReferenciaCruce(null);
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 3:
                sps = new MaterialBetterSpinner[]{
                        spBarrioFibra,
                        spTipoPrincipalFibra,
                        spReferenciaPrincipalFibra,
                        spTipoCruceFibra,
                        spReferenciaCruceFibra,
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setBarrio(null);
                disFibra.setPrincipal(null);
                disFibra.setReferenciaPrincipal(null);
                disFibra.setCruce(null);
                disFibra.setReferenciaCruce(null);
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 4:
                if (swDireccionFibra.isSelected()) {
                    sps = new MaterialBetterSpinner[]{
                            spDireccionNoParamFibra
                    };
                    disFibra.setDireccion(null);
                    break;
                }
                sps = new MaterialBetterSpinner[]{
                        spTipoPrincipalFibra,
                        spReferenciaPrincipalFibra,
                        spTipoCruceFibra,
                        spReferenciaCruceFibra,
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setPrincipal(null);
                disFibra.setReferenciaPrincipal(null);
                disFibra.setCruce(null);
                disFibra.setReferenciaCruce(null);
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 5:
                sps = new MaterialBetterSpinner[]{
                        spReferenciaPrincipalFibra,
                        spTipoCruceFibra,
                        spReferenciaCruceFibra,
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setReferenciaPrincipal(null);
                disFibra.setCruce(null);
                disFibra.setReferenciaCruce(null);
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 6:
                sps = new MaterialBetterSpinner[]{
                        spTipoCruceFibra,
                        spReferenciaCruceFibra,
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setCruce(null);
                disFibra.setReferenciaCruce(null);
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 7:
                sps = new MaterialBetterSpinner[]{
                        spReferenciaCruceFibra,
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setReferenciaCruce(null);
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 8:
                sps = new MaterialBetterSpinner[]{
                        spPlacaFibra,
                        spComplementoFibra
                };
                disFibra.setPlaca(null);
                disFibra.setComplemento(null);
                break;
            case 9:
                sps = new MaterialBetterSpinner[]{
                        spComplementoFibra
                };
                disFibra.setComplemento(null);
                break;
        }
        FormularioUtil.limpiarMaterialSpinner(sps, getActivity());
    }
}
