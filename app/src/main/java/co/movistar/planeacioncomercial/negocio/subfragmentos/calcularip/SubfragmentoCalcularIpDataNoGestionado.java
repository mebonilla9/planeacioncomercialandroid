package co.movistar.planeacioncomercial.negocio.subfragmentos.calcularip;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppNoGestionado;
import co.movistar.planeacioncomercial.negocio.constantes.ETipoCalcularIp;

/**
 * Created by Lord_Nightmare on 13/03/17.
 */

public class SubfragmentoCalcularIpDataNoGestionado extends SubfragmentoCalcularIpDataFragment {

    private final ETipoCalcularIp tipo = ETipoCalcularIp.TIPO_APP_NO_GESTIONADO;
    private List<AppNoGestionado> listaNoGestionados;

    @Override
    public void cargarData() {
        this.setListaNoGestionados((List<AppNoGestionado>) getArguments().getSerializable("data"));
        generarVista(this.getListaNoGestionados(),this.tipo);
    }

    @Override
    public void actualizarDataSet() {
        this.cargarDatosView(this.listaNoGestionados, this.tipo);
    }

    public List<AppNoGestionado> getListaNoGestionados() {
        return listaNoGestionados;
    }

    public void setListaNoGestionados(List<AppNoGestionado> listaNoGestionados) {
        this.listaNoGestionados = listaNoGestionados;
    }
}
