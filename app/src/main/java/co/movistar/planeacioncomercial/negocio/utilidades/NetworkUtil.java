package co.movistar.planeacioncomercial.negocio.utilidades;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by lord_nightmare on 17/10/17.
 */

public final class NetworkUtil {

    public static boolean isOnline(Context contexto){
        ConnectivityManager manager = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        return (info != null && info.isConnected());
    }

}
