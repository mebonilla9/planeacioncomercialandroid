package co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import co.movistar.planeacioncomercial.modelo.servicios.AppDisponibilidadService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 5/12/16.
 */

public class SubfragmentoDisponibiidadCaja extends Fragment {

    private MaterialBetterSpinner spDistribuidor;
    private MaterialBetterSpinner spArmario;
    private MaterialBetterSpinner spCaja;
    private Button btnConsultarCaja;
    private MaterialBetterSpinner spDepartamentosCaja;
    private MaterialBetterSpinner spLocalidadesCaja;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_disponibilidad_caja, container, false);
        initComponents(vista);
        asignarListeners();
        cargarListaInicial();
        return vista;
    }

    private void initComponents(View vista) {
        this.btnConsultarCaja = (Button) vista.findViewById(R.id.btnConsultarCaja);
        this.spCaja = (MaterialBetterSpinner) vista.findViewById(R.id.spCaja);
        this.spArmario = (MaterialBetterSpinner) vista.findViewById(R.id.spArmario);
        this.spDistribuidor = (MaterialBetterSpinner) vista.findViewById(R.id.spDistribuidor);
        this.spLocalidadesCaja = (MaterialBetterSpinner) vista.findViewById(R.id.spLocalidadesCaja);
        this.spDepartamentosCaja = (MaterialBetterSpinner) vista.findViewById(R.id.spDepartamentosCaja);
        this.btnConsultarCaja.setEnabled(false);
        spDepartamentosCaja.setVisibility(View.GONE);
        spArmario.setVisibility(View.GONE);
        spDistribuidor.setVisibility(View.GONE);
        spLocalidadesCaja.setVisibility(View.GONE);
        spCaja.setVisibility(View.GONE);
        //
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Consulta por caja");
        home.getSupportActionBar().setSubtitle("Disponibilidad de Red");
    }

    private void asignarListeners() {
        TextWatcher eventoCambio = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (spDepartamentosCaja.getText().hashCode() == charSequence.hashCode()){
                    cargarListaLocalidades(charSequence.toString());
                    habilitarBoton();
                    return;
                }
                if (spLocalidadesCaja.getText().hashCode() == charSequence.hashCode()){
                    cargarDistribuidores(spDepartamentosCaja.getText().toString(),charSequence.toString());
                    habilitarBoton();
                    return;
                }
                if (spDistribuidor.getText().hashCode() == charSequence.hashCode()) {
                    cargarArmarioPorDistribuidor(charSequence.toString());
                    habilitarBoton();
                    return;
                }
                if (spArmario.getText().hashCode() == charSequence.hashCode()) {
                    cargarCajaPorArmario(spDistribuidor.getText().toString(), charSequence.toString());
                    habilitarBoton();
                    return;
                }
                if (spCaja.getText().hashCode() == charSequence.hashCode()) {
                    habilitarBoton();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        View.OnClickListener eventoClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder builder = new StringBuilder();
                builder.append("[]?[]?[]?[");
                builder.append(spDistribuidor.getText().toString());
                builder.append("]?[");
                builder.append(spArmario.getText().toString());
                builder.append("]?[");
                builder.append(spCaja.getText().toString());
                builder.append("]");
                Bundle bundle = new Bundle();
                bundle.putString("data-request", builder.toString());
                bundle.putString("tipoCaja","caja");
                FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(), bundle, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
            }
        };

        spDepartamentosCaja.addTextChangedListener(eventoCambio);
        spLocalidadesCaja.addTextChangedListener(eventoCambio);
        spDistribuidor.addTextChangedListener(eventoCambio);
        spArmario.addTextChangedListener(eventoCambio);
        spCaja.addTextChangedListener(eventoCambio);

        btnConsultarCaja.setOnClickListener(eventoClick);
    }

    private void cargarListaInicial() {
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaDepartamentos = response.body();
                indexarListaDepartamentosSpinner(listaDepartamentos);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {

            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionDepartamentos(callback);
    }

    private void cargarListaLocalidades(String departamento){
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaLocalidades = response.body();
                indexarListaLocalidadesSpinner(listaLocalidades);
                spLocalidadesCaja.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {
                t.printStackTrace();
            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionLocalidades(departamento,callback);
    }

    private void cargarDistribuidores(String departamento, String localidad) {
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaDistribuidores = response.body();
                indexarListaDistribuidoresSpinner(listaDistribuidores);
                spDistribuidor.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {
                t.printStackTrace();
            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionDistritos(departamento, localidad, callback);
    }

    private void cargarArmarioPorDistribuidor(String distrito) {
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaArmarios = response.body();
                indexarListaArmariosSpinner(listaArmarios);
                spArmario.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {
                t.printStackTrace();
            }
        };

        new AppDisponibilidadService(getActivity()).consultarInformacionArmarios(distrito, callback);
    }

    private void cargarCajaPorArmario(String distrito, String armario) {
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaCajas = response.body();
                indexarListaCajasSpinner(listaCajas);
                spCaja.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {
                t.printStackTrace();
            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionCajas(distrito, armario, callback);
    }

    private void indexarListaDepartamentosSpinner(List<AppDisponibilidad> listaDepartamentos) {
        List<String> departamentos = new ArrayList<>();
        for (AppDisponibilidad disponible : listaDepartamentos) {
            if (disponible.getRegional() != null) {
                departamentos.add(disponible.getRegional());
            }
        }
        try {
            spDepartamentosCaja.setAdapter(new ArrayAdapter<>(
                    getActivity(),
                    android.R.layout.simple_dropdown_item_1line,
                    departamentos
            ));
        } catch(Exception e){
            System.out.println("Objeto nulo de spinner");
        }
        spDepartamentosCaja.setVisibility(View.VISIBLE);
    }

    private void indexarListaLocalidadesSpinner(List<AppDisponibilidad> listaLocalidades) {
        List<String> localidades = new ArrayList<>();
        for (AppDisponibilidad disponible : listaLocalidades) {
            if (disponible.getLocalidad() != null) {
                localidades.add(disponible.getLocalidad());
            }
        }
        spLocalidadesCaja.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                localidades
        ));
    }

    private void indexarListaDistribuidoresSpinner(List<AppDisponibilidad> listaDistribuidores) {
        List<String> distribuidores = new ArrayList<>();
        for (AppDisponibilidad disponible : listaDistribuidores) {
            if (disponible.getMDF() != null) {
                distribuidores.add(disponible.getMDF());
            }
        }
        spDistribuidor.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                distribuidores
        ));
    }

    private void indexarListaArmariosSpinner(List<AppDisponibilidad> listaArmarios) {
        List<String> armarios = new ArrayList<>();
        for (AppDisponibilidad disponible : listaArmarios) {
            if (disponible.getArmario() != null) {
                armarios.add(disponible.getArmario());
            }
        }
        spArmario.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                armarios
        ));
    }

    private void indexarListaCajasSpinner(List<AppDisponibilidad> listaCajas) {
        List<String> cajas = new ArrayList<>();
        for (AppDisponibilidad disponible : listaCajas) {
            if (disponible.getCaja() != null) {
                cajas.add(disponible.getCaja());
            }
        }
        spCaja.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                cajas
        ));
    }

    private void habilitarBoton() {
        if (spDistribuidor.getText().toString().equals("") || spArmario.getText().toString().equals("") || spCaja.getText().toString().equals("")) {
            btnConsultarCaja.setEnabled(false);
            return;
        }
        btnConsultarCaja.setEnabled(true);
    }
}
