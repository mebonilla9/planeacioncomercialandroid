package co.movistar.planeacioncomercial.negocio.subfragmentos.calcularip;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.adapters.calcularip.ContenedorInformacionCalcularIpAdapter;
import co.movistar.planeacioncomercial.negocio.constantes.ETipoCalcularIp;

/**
 * Created by Lord_Nightmare on 13/02/17.
 */

public abstract class SubfragmentoCalcularIpDataFragment extends Fragment {

    protected RecyclerView rvItemsCalculador;
    protected ContenedorInformacionCalcularIpAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_calcular_ip_data, container, false);
        this.rvItemsCalculador = (RecyclerView) vista.findViewById(R.id.rvItemsCalculador);
        cargarData();
        return vista;
    }

    protected void generarVista(List lista, ETipoCalcularIp tipo) {
        adapter = new ContenedorInformacionCalcularIpAdapter(getContext(), tipo);
        this.rvItemsCalculador.setAdapter(adapter);
        this.rvItemsCalculador.setLayoutManager(new LinearLayoutManager(getContext()));
        this.rvItemsCalculador.setHasFixedSize(true);
        cargarDatosView(lista, tipo);
    }

    protected abstract void cargarData();
    protected abstract void actualizarDataSet();

    protected void cargarDatosView(List lista, ETipoCalcularIp tipo) {
        adapter.modificarDatos(tipo, lista);
    }
}
