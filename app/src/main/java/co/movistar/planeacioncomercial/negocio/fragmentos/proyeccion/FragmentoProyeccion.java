package co.movistar.planeacioncomercial.negocio.fragmentos.proyeccion;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.servicios.AppProyeccionService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.proyeccion.SubfragmentoProyeccionData;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 11/04/17.
 */

public class FragmentoProyeccion extends Fragment {

    private android.support.v4.app.FragmentTabHost tabhost;
    private ProgressDialog dialogo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_proyeccion, container, false);
        initComponents(vista);
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        cargarProductosUsuario();
        return vista;
    }

    private void initComponents(View vista) {
        this.tabhost = (FragmentTabHost) vista.findViewById(android.R.id.tabhost);
        this.tabhost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
    }

    private void cargarProductosUsuario() {
        Callback<List<String>> callback = new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (!response.isSuccessful()  || response.body().isEmpty() || response.body().size() == 0) {
                    AlertaUtil.mostrarAlerta("Proyección", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<String> productos = response.body();
                listarProductosUsuario(productos);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                AlertaUtil.mostrarAlerta("Error Consulta Ventas", t.getMessage(), null, null, getActivity());
            }
        };
        new AppProyeccionService(getActivity()).listaProductosDisponibles(callback);
    }

    private void listarProductosUsuario(List<String> productos) {
        // Adicion de las pestañas, segun los subfragmentos creados para el módulo
        for (int i = 0; i < productos.size(); i++) {
            String producto = productos.get(i);
            tabhost.addTab(tabhost.newTabSpec(producto).setIndicator(producto), SubfragmentoProyeccionData.class, obtenerArgumentosTabs(producto));
        }
        asignarEstiloTabs();
    }

    private Bundle obtenerArgumentosTabs(String producto) {
        Bundle argumentos = new Bundle();
        argumentos.putString("producto", producto);
        return argumentos;
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Proyección");
        home.getSupportActionBar().setSubtitle("");
    }

    private void asignarEstiloTabs() {
        for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) tabhost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.icons));
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        reDistribuirActivity();
    }
}
