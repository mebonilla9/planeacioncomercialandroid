package co.movistar.planeacioncomercial.negocio.fragmentos.metas;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppPresupuestoComercial;
import co.movistar.planeacioncomercial.modelo.servicios.AppMetasService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.subfragmentos.metas.SubfragmentoMetasData;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 15/03/17.
 */

public class FragmentoMetas extends Fragment {

    private FragmentTabHost tabHost;
    private com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner spPeriodo;
    private int tabActual;
    private String periodoActual;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_metas, container, false);
        initComponents(vista);
        consultarPeriodosMetas();
        return vista;
    }

    private void initComponents(View vista) {
        tabHost = (FragmentTabHost) vista.findViewById(android.R.id.tabhost);
        tabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        this.spPeriodo = (MaterialBetterSpinner) vista.findViewById(R.id.spPeriodo);
        this.spPeriodo.setVisibility(View.GONE);
    }

    private void consultarPeriodosMetas() {
        Callback<List<AppPresupuestoComercial>> callback = new Callback<List<AppPresupuestoComercial>>() {
            @Override
            public void onResponse(Call<List<AppPresupuestoComercial>> call, Response<List<AppPresupuestoComercial>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    //ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    AlertaUtil.mostrarAlerta("Metas", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    //ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppPresupuestoComercial> listaPeriodos = response.body();
                cargarPeriodoSpinner(listaPeriodos);
                spPeriodo.setVisibility(View.VISIBLE);
                //consultarCategoriasMetas();
            }

            @Override
            public void onFailure(Call<List<AppPresupuestoComercial>> call, Throwable t) {

            }
        };
        new AppMetasService(getActivity()).consultarPeriodosUsuario(callback);
    }

    private void cargarPeriodoSpinner(List<AppPresupuestoComercial> listaPeriodos) {
        List<String> periodos = new ArrayList<>();
        for (AppPresupuestoComercial appPresupuestoComercial : listaPeriodos) {
            if (appPresupuestoComercial.getPeriodo() != null) {
                periodos.add(appPresupuestoComercial.getPeriodo());
            }
        }
        this.spPeriodo.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                periodos
        ));
        this.spPeriodo.setText(periodos.get(0));
        periodoActual = periodos.get(0);
    }

    private void consultarCategoriasMetas() {
        Callback<List<AppPresupuestoComercial>> callback = new Callback<List<AppPresupuestoComercial>>() {
            @Override
            public void onResponse(Call<List<AppPresupuestoComercial>> call, Response<List<AppPresupuestoComercial>> response) {
                if (!response.isSuccessful()) {
                    AlertaUtil.mostrarAlerta("Liquidación", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    return;
                }
                List<AppPresupuestoComercial> listaCategorias = response.body();
                cargarCategoriasTabs(listaCategorias);
            }

            @Override
            public void onFailure(Call<List<AppPresupuestoComercial>> call, Throwable t) {

            }
        };
        new AppMetasService(getActivity()).consultarCategoriasUsuario(callback);
    }

    private void cargarCategoriasTabs(List<AppPresupuestoComercial> listaCategorias) {
        for (int i = 0; i < listaCategorias.size(); i++) {
            AppPresupuestoComercial categoria = listaCategorias.get(i);
            String nombreCategoria = categoria.getCategoria();
            tabHost.addTab(tabHost.newTabSpec(nombreCategoria).setIndicator(nombreCategoria), SubfragmentoMetasData.class, obtenerArgumentosTabs(nombreCategoria));
        }
        asignarEstiloTabs();
        reDistribuirActivity();
    }

    private Bundle obtenerArgumentosTabs(String categoria) {
        Bundle argumentos = new Bundle();
        argumentos.putString("categoria", categoria);
        return argumentos;
    }

    private void asignarEstiloTabs() {
        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.icons));
        }
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().hide();
        home.getSupportActionBar().setTitle("Metas");
        home.getSupportActionBar().setSubtitle("");
    }

    @Override
    public void onResume() {
        super.onResume();
        asignarEventos();
        reDistribuirActivity();
    }

    private void asignarEventos() {
        TextWatcher cambioSpinner = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tabActual = tabHost.getCurrentTab() >= 0 ? tabHost.getCurrentTab() : 0;
                tabHost.clearAllTabs();
                //dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
                periodoActual = s.toString();
                consultarCategoriasMetas();
                tabHost.setCurrentTab(tabActual);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        this.spPeriodo.addTextChangedListener(cambioSpinner);
    }

    public String getPeriodoActual() {
        return this.periodoActual;
    }

}
