package co.movistar.planeacioncomercial.negocio.subfragmentos.niveldeservicio;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppServicio;
import co.movistar.planeacioncomercial.negocio.adapters.niveldeservicio.ContenedorInformacionNivelesServicioAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;

/**
 * Created by lord_nightmare on 17/04/17.
 */

public class SubfragmentoNivelesServicio extends Fragment {


    private ContenedorInformacionNivelesServicioAdapter nivelesServicioAdapter;
    private RecyclerView rvNivelesServicioData;
    private String tipo;
    private List<AppServicio> listaServicio;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_nivel_de_servicio, container, false);
        this.tipo = getArguments().getString("tipo");
        this.listaServicio = (ArrayList<AppServicio>) getArguments().getSerializable("servicio");
        initComponents(vista);
        return vista;
    }

    private void initComponents(View vista) {
        this.rvNivelesServicioData = (RecyclerView) vista.findViewById(R.id.rvNivelesServicioData);
        cargarInformacionLista();
    }

    private void cargarInformacionLista() {
        this.nivelesServicioAdapter = new ContenedorInformacionNivelesServicioAdapter(listaServicio, tipo);
        rvNivelesServicioData.setAdapter(nivelesServicioAdapter);
        rvNivelesServicioData.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvNivelesServicioData.setLayoutManager(layoutManager);
    }
}
