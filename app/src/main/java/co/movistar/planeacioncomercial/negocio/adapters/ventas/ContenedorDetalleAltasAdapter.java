package co.movistar.planeacioncomercial.negocio.adapters.ventas;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.GeneracionAltasDTO;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by Lord_Nightmare on 18/11/16.
 */

public class ContenedorDetalleAltasAdapter extends RecyclerView.Adapter<ContenedorDetalleAltasAdapter.ViewHolder> {

    private Context context;
    private List<GeneracionAltasDTO> altas;

    public ContenedorDetalleAltasAdapter(Context context, List<GeneracionAltasDTO> altas) {
        this.context = context;
        this.altas = altas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_venta_detalle,parent,false);
        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GeneracionAltasDTO altasDto = this.altas.get(position);
        this.asignarValorCampoPrincipal(holder.lblCampoPrincipal,altasDto);
        holder.lblAltas.setText(FormularioUtil.convertirFormatoMiles(altasDto.getAppAltas().getAltas().toString()));
        holder.lblMeta.setText(FormularioUtil.convertirFormatoMiles(altasDto.getMeta().toString()));
        holder.lblCumplimiento.setTextColor(Color.GREEN);
        if (altasDto.getAppAltas().getAltas() < altasDto.getMeta()) {
            holder.lblCumplimiento.setTextColor(Color.RED);
        }
        holder.lblCumplimiento.setText(modificarValorPorcentualCumplimiento(altasDto.getCumplimiento()));
    }

    private String modificarValorPorcentualCumplimiento(Double cumplimientoProyectado) {
        Long numeroPorcentual = Math.round(Math.floor(cumplimientoProyectado*100));
        return numeroPorcentual.toString()+"%";

    }

    private void asignarValorCampoPrincipal(TextView texto, GeneracionAltasDTO altasDto){
        String valor = "";
        if (altasDto.getAppAltas().getCanal()!= null){
            valor = altasDto.getAppAltas().getCanal();
        }
        if (altasDto.getAppAltas().getRegional()!= null){
            valor = altasDto.getAppAltas().getRegional();
        }
        if (altasDto.getAppAltas().getSegmento()!= null){
            valor = altasDto.getAppAltas().getSegmento();
        }
        texto.setText(valor);
    }



    @Override
    public int getItemCount() {
        return this.altas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView lblCampoPrincipal;
        private TextView lblAltas;
        private TextView lblMeta;
        private TextView lblCumplimiento;

        public ViewHolder(View itemView) {
            super(itemView);
            this.lblCampoPrincipal = (TextView) itemView.findViewById(R.id.lblCampoPrincipal);
            this.lblAltas = (TextView) itemView.findViewById(R.id.lblAltas);
            this.lblMeta = (TextView) itemView.findViewById(R.id.lblMeta);
            this.lblCumplimiento = (TextView) itemView.findViewById(R.id.lblCumplimiento);
        }
    }
}
