package co.movistar.planeacioncomercial.negocio.utilidades;

import android.content.Context;
import android.content.Intent;

import java.util.Date;

import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.actividades.LoginActivity;

/**
 * Created by Lord_Nightmare on 24/10/16.
 */

public final class TokenUtil {

    private static final String TAG = "utilidades.TokenUtil";

    public static String obtenerTokenLocal(Context contexto) {
        return PreferenciasUtil.obtenerPreferencias("auth-token", contexto);
    }

    public static void verificarFechaToken(Context contexto, Date fechaExpiracion) {
        if (fechaExpiracion.before(new Date())) {
            contexto.startActivity(new Intent(contexto, LoginActivity.class));
        }
        contexto.startActivity(new Intent(contexto, HomeActivity.class));
    }
}
