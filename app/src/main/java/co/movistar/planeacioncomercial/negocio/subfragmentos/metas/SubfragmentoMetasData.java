package co.movistar.planeacioncomercial.negocio.subfragmentos.metas;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppPresupuestoComercial;
import co.movistar.planeacioncomercial.modelo.servicios.AppMetasService;
import co.movistar.planeacioncomercial.negocio.adapters.metas.ContenedorInformacionMetasAdapter;
import co.movistar.planeacioncomercial.negocio.fragmentos.metas.FragmentoMetas;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 15/03/17.
 */

public class SubfragmentoMetasData extends Fragment {

    private RecyclerView rvItemsMetasData;
    private ContenedorInformacionMetasAdapter adapter;

    private Bundle argumentos;
    private ProgressDialog dialogo;
    private String periodo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_metas_data, container, false);
        initComponents(vista);
        this.argumentos = getArguments();
        FragmentoMetas fragmentoMetas = (FragmentoMetas) getParentFragment();
        periodo = fragmentoMetas.getPeriodoActual();

        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        consultarInformacionMetas();
    }

    private void consultarInformacionMetas() {
        Callback<List<AppPresupuestoComercial>> callback = new Callback<List<AppPresupuestoComercial>>() {
            @Override
            public void onResponse(Call<List<AppPresupuestoComercial>> call, Response<List<AppPresupuestoComercial>> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppPresupuestoComercial> listaMetas = response.body();
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                cargarInformacionMetas(listaMetas);
            }

            @Override
            public void onFailure(Call<List<AppPresupuestoComercial>> call, Throwable t) {

            }
        };
        new AppMetasService(getActivity()).consultarMetaUsuario(argumentos.getString("categoria"), periodo, callback);
    }

    private void initComponents(View vista) {
        this.rvItemsMetasData = (RecyclerView) vista.findViewById(R.id.rvItemsMetasData);
        this.rvItemsMetasData.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        this.rvItemsMetasData.setHasFixedSize(true);
    }

    private void cargarInformacionMetas(List<AppPresupuestoComercial> listaMetas) {
        adapter = new ContenedorInformacionMetasAdapter(listaMetas);
        this.rvItemsMetasData.setAdapter(adapter);
    }
}
