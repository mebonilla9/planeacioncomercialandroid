package co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;

/**
 * Created by alejandro.jimenez on 28/11/2016.
 */

public class SubfragmentoDisponibilidadResumen extends Fragment {

    private CardView btnRegionales;
    private CardView btnDepartamentosLocalidades;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_disponibilidad_resumen, container, false);
        initComponents(vista);
        asignarEventos();
        return vista;
    }

    private void initComponents(View vista) {
        this.btnDepartamentosLocalidades = (CardView) vista.findViewById(R.id.btnDepartamentosLocalidades);
        this.btnRegionales = (CardView) vista.findViewById(R.id.btnRegionales);
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Seleccion de opción de resumen");
        home.getSupportActionBar().setSubtitle("Disponibilidad de Red");
    }

    private void asignarEventos() {
        View.OnClickListener eventoClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(), null, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
            }
        };
        btnRegionales.setOnClickListener(eventoClick);
        btnDepartamentosLocalidades.setOnClickListener(eventoClick);
    }
}
