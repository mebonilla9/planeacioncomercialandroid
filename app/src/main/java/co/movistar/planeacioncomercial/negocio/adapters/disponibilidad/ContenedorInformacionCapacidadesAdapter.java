package co.movistar.planeacioncomercial.negocio.adapters.disponibilidad;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by Lord_Nightmare on 20/02/17.
 */

public class ContenedorInformacionCapacidadesAdapter extends RecyclerView.Adapter<ContenedorInformacionCapacidadesAdapter.ViewHolder> {

    private Context context;
    private List<AppDisponibilidad> capacidades;

    public ContenedorInformacionCapacidadesAdapter(Context context, List<AppDisponibilidad> capacidades) {
        this.context = context;
        this.capacidades = capacidades;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_disponibilidad, parent, false);
        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppDisponibilidad disponibilidad = this.capacidades.get(position);
        holder.lblPuertos.setText(disponibilidad.getCategoria());
        if (disponibilidad.getqOferta() != null) {
            if (disponibilidad.getqOferta().contains("Mb")) {
                holder.lblDataPuertos.setText(disponibilidad.getqOferta() == null ? "0" : disponibilidad.getqOferta().toString());
                return;
            }
        }
        holder.lblDataPuertos.setText(disponibilidad.getqOferta() == null ? "0" : FormularioUtil.convertirFormatoMiles(disponibilidad.getqOferta().toString()));
    }

    @Override
    public int getItemCount() {
        return this.capacidades.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView lblPuertos;
        private TextView lblDataPuertos;

        public ViewHolder(View itemView) {
            super(itemView);
            this.lblPuertos = (TextView) itemView.findViewById(R.id.lblPuertos);
            this.lblDataPuertos = (TextView) itemView.findViewById(R.id.lblDataPuertos);
        }
    }
}
