package co.movistar.planeacioncomercial.negocio.adapters.ventas;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.GeneracionAltasDTO;
import co.movistar.planeacioncomercial.modelo.dto.InformacionAltasGeneralDTO;

/**
 * Created by Lord_Nightmare on 18/11/16.
 */

public class ContenedorInformacionAltasAdapter extends RecyclerView.Adapter<ContenedorInformacionAltasAdapter.ViewHolder>  {

    private Context context;
    private InformacionAltasGeneralDTO informacionAltasGeneralDTO;

    public ContenedorInformacionAltasAdapter(Context context, InformacionAltasGeneralDTO informacionAltasGeneralDTO) {
        this.context = context;
        this.informacionAltasGeneralDTO = informacionAltasGeneralDTO;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_venta_contenedor,parent,false);
        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0){
            holder.lblTituloContenedor.setText("Canal");
        }
        if (position == 1){
            holder.lblTituloContenedor.setText("Regional");
        }
        if (position == 2){
            holder.lblTituloContenedor.setText("Segmento");
        }
        //holder.lblPorcentajeContenedor.setText(this.informacionAltasGeneralDTO.getPorcentajeCorte().getDeberiamosIrAl());
        List<GeneracionAltasDTO> altasDto = this.informacionAltasGeneralDTO.getListaAltas().get(position);

        ContenedorDetalleAltasAdapter detalleAltasAdapter = new ContenedorDetalleAltasAdapter(context,altasDto);
        holder.rvItemsDetalle.setAdapter(detalleAltasAdapter);
        holder.rvItemsDetalle.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(context,2);
        holder.rvItemsDetalle.setLayoutManager(layoutManager);
    }

    @Override
    public int getItemCount() {
        return informacionAltasGeneralDTO.getListaAltas().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView lblTituloContenedor;

        private RecyclerView rvItemsDetalle;

        public ViewHolder(View itemView) {
            super(itemView);

            this.lblTituloContenedor = (TextView) itemView.findViewById(R.id.lblTituloContenedor);
            this.rvItemsDetalle = (RecyclerView) itemView.findViewById(R.id.rvItemsDetalle);
        }
    }

}
