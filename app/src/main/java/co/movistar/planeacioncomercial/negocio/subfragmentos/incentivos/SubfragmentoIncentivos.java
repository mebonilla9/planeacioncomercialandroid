package co.movistar.planeacioncomercial.negocio.subfragmentos.incentivos;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppArchivos;
import co.movistar.planeacioncomercial.modelo.servicios.AppArchivosService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.adapters.archivos.ArchivosAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 31/03/17.
 */

public class SubfragmentoIncentivos extends Fragment {

    private RecyclerView rvArchivosIncentivos;
    private ArchivosAdapter archivosAdapter;
    private final String MODULO = "Incentivos";
    private Bundle argumentos;
    private ProgressDialog dialogo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_incentivos, container, false);
        initComponents(vista);
        this.argumentos = getArguments();
        dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
        consultarInformacionArchivos();
        return vista;
    }

    private void consultarInformacionArchivos() {
        Callback<List<AppArchivos>> callback = new Callback<List<AppArchivos>>() {
            @Override
            public void onResponse(Call<List<AppArchivos>> call, Response<List<AppArchivos>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    AlertaUtil.mostrarAlerta("Incentivos", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppArchivos> listaArchivos = response.body();
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                cargarInformacionArchivos(listaArchivos);
            }

            @Override
            public void onFailure(Call<List<AppArchivos>> call, Throwable t) {

            }
        };
        new AppArchivosService(getActivity()).consultarArchivosModuloCategoria(MODULO, argumentos.getString("categoria"),callback);
    }

    private void cargarInformacionArchivos(List<AppArchivos> listaArchivos) {
        archivosAdapter = new ArchivosAdapter(listaArchivos, getActivity());
        this.rvArchivosIncentivos.setAdapter(archivosAdapter);
    }

    private void initComponents(View vista) {
        this.rvArchivosIncentivos = (RecyclerView) vista.findViewById(R.id.rvArchivosIncentivos);
        this.rvArchivosIncentivos.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.rvArchivosIncentivos.setHasFixedSize(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ProgresoUtil.ocultarDialogoProgreso(dialogo);
    }
}
