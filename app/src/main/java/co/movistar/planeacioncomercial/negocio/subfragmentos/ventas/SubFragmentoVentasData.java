package co.movistar.planeacioncomercial.negocio.subfragmentos.ventas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.InformacionAltasGeneralDTO;
import co.movistar.planeacioncomercial.modelo.servicios.AppAltasService;
import co.movistar.planeacioncomercial.negocio.adapters.ventas.ContenedorInformacionAltasAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 6/02/17.
 */

public class SubFragmentoVentasData extends Fragment {

    private ContenedorInformacionAltasAdapter altasAdapter;
    private RecyclerView rvItemsData;
    private Bundle argumentos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_venta_data, container, false);
        argumentos = getArguments();
        initComponents(vista);
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        consultarInformacionAltasContrato();
    }

    private void initComponents(View vista) {
        this.rvItemsData = (RecyclerView) vista.findViewById(R.id.rvItemsData);
    }

    private void consultarInformacionAltasContrato() {

        Callback<InformacionAltasGeneralDTO> callback = new Callback<InformacionAltasGeneralDTO>() {
            @Override
            public void onResponse(Call<InformacionAltasGeneralDTO> call, Response<InformacionAltasGeneralDTO> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                InformacionAltasGeneralDTO infoAltas = response.body();
                reAsignarValorSubTitulo(infoAltas);
                cargarInformacionLista(infoAltas);
            }

            @Override
            public void onFailure(Call<InformacionAltasGeneralDTO> call, Throwable t) {
                AlertaUtil.mostrarAlerta("Error Consulta Ventas", t.getMessage(), null, null, getActivity());

            }
        };
        new AppAltasService(getActivity()).consultarInformacionAltas(argumentos.getString("producto"), argumentos.getLong("id_usuario")+"", callback);
    }

    private void reAsignarValorSubTitulo(InformacionAltasGeneralDTO infoAltas) {
        StringBuilder builder = new StringBuilder("Porcentaje / ");
        builder.append(infoAltas.getPorcentajeCorte().getDeberiamosIrAl());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(builder.toString());
    }

    private void cargarInformacionLista(InformacionAltasGeneralDTO informacionAltasGeneralDTO) {
        this.altasAdapter = new ContenedorInformacionAltasAdapter(getActivity(), informacionAltasGeneralDTO);
        rvItemsData.setAdapter(altasAdapter);
        rvItemsData.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        rvItemsData.setLayoutManager(layoutManager);
    }
}
