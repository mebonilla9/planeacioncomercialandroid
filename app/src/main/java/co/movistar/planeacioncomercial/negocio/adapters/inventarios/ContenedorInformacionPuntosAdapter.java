package co.movistar.planeacioncomercial.negocio.adapters.inventarios;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppPuntos;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public class ContenedorInformacionPuntosAdapter extends RecyclerView.Adapter<ContenedorInformacionPuntosAdapter.ViewHolder> {

    private List<AppPuntos> listaPuntos;
    private Context contexto;

    public ContenedorInformacionPuntosAdapter(List<AppPuntos> listaPuntos, Context contexto) {
        this.listaPuntos = listaPuntos;
        this.contexto = contexto;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContenedorInformacionPuntosAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inventarios_puntos, parent, false));
    }

    @Override
    public void onBindViewHolder(ContenedorInformacionPuntosAdapter.ViewHolder holder, int position) {
        AppPuntos punto = this.listaPuntos.get(position);
        holder.txtNombrePunto.setText(punto.getNombrePunto());
        holder.txtDireccionPunto.setText(punto.getDireccion());
        holder.txtDepartamentoPunto.setText(punto.getDepartamento());
        holder.txtCiudadPunto.setText(punto.getCiudad());
        //asignarEvento(punto, holder.cvPunto);
    }

    /*private void asignarEvento(final AppPuntos punto, CardView cvPunto) {
        cvPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle argumentos = new Bundle();
                argumentos.putString("id_punto",punto.getIdPunto());
                FragmentoUtil.obtenerFragmentoSeleccionado(R.id.rvPuntos,argumentos, ((AppCompatActivity)contexto).getSupportFragmentManager().beginTransaction(),R.id.contenedorFragmentos);
            }
        });
    }*/

    @Override
    public int getItemCount() {
        return this.listaPuntos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txtNombrePunto;
        private TextView txtDireccionPunto;
        private TextView txtDepartamentoPunto;
        private TextView txtCiudadPunto;
        private CardView cvPunto;

        public ViewHolder(View itemView) {
            super(itemView);
            txtNombrePunto = (TextView) itemView.findViewById(R.id.txtNombrePunto);
            txtDireccionPunto = (TextView) itemView.findViewById(R.id.txtDireccionPunto);
            txtDepartamentoPunto = (TextView) itemView.findViewById(R.id.txtDepartamentoPunto);
            txtCiudadPunto = (TextView) itemView.findViewById(R.id.txtCiudadPunto);
            cvPunto = (CardView) itemView.findViewById(R.id.cvPunto);
        }
    }
}
