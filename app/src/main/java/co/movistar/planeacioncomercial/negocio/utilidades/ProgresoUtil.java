package co.movistar.planeacioncomercial.negocio.utilidades;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Lord_Nightmare on 23/01/17.
 */

public class ProgresoUtil {

    public static ProgressDialog getDialogo(Context contexto, String titulo, String mensaje){
        return ProgressDialog.show(contexto,titulo,mensaje,true);
    }

    public static void ocultarDialogoProgreso(final ProgressDialog dialogo){
        Thread hiloDialogo = new Thread(new Runnable() {
            @Override
            public void run() {
                dialogo.dismiss();
            }
        });
        hiloDialogo.start();
    }

}
