package co.movistar.planeacioncomercial.negocio.utilidades;

//import com.example.UserValidator;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.View;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lord_Nightmare on 25/10/16.
 */

public final class FormularioUtil {

    private static final String PATRON_CONTRASENA = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{10,20})";
    private static final String REGEX_CORREO = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static String convertirFormatoMiles(String valor) {
        NumberFormat money = NumberFormat.getCurrencyInstance();
        money.setMinimumFractionDigits(0);
        money.setMaximumFractionDigits(0);
        String moneda = "";
        if (valor.contains(".")) {
            Double valorDoble = Double.parseDouble(valor);
            moneda = money.format(valorDoble.longValue());
        } else {
            moneda = money.format(Long.parseLong(valor));
        }
        String convertido = moneda.replace(Pattern.quote(" "), "").replace("$", "").replace("€", "").replace(",", ".").trim().replaceAll("\\s+$", "");
        return rearmarNumero(convertido.split(Pattern.quote(".")));
    }

    public static String convertirCumplimientoPorcentaje(Double cumplimiento) {
        cumplimiento = cumplimiento * 100;
        String[] seccionesCumplimiento = cumplimiento.toString().split(Pattern.quote("."));
        return seccionesCumplimiento[0] + "." + seccionesCumplimiento[1].substring(0, 1) + "%";
    }

    private static String rearmarNumero(String[] miles) {
        if (miles.length < 2) {
            return miles[0];
        }
        StringBuilder dato = new StringBuilder();

        for (int i = 0; i < miles.length; i++) {
            dato.append(miles[i]);
            if (i < miles.length - 1) {
                dato.append(".");
            }
        }
        return dato.toString();
    }

    public static boolean validarIntegridadContrasena(String contrasena) {
        Pattern patron = Pattern.compile(PATRON_CONTRASENA);
        Matcher coincidencia = patron.matcher(contrasena);
        return coincidencia.matches();
    }

    public static boolean validarIntegridadCorreo(String correo) {
        return Pattern.compile(REGEX_CORREO).matcher(correo).matches();
    }

    public static void aplicarColorBoton(View boton, Context contexto, int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            boton.setBackgroundTintList(ColorStateList.valueOf(contexto.getResources().getColor(color)));
        } else {
            ViewCompat.setBackgroundTintList(boton, ColorStateList.valueOf(contexto.getResources().getColor(color)));
        }
    }
    public static void limpiarMaterialSpinner(MaterialBetterSpinner[] sps, Context contexto) {
        if (sps == null){
            return;
        }
        for (int i = 0; i < sps.length; i++) {
            sps[i].setText("");
            sps[i].setVisibility(View.INVISIBLE);
        }
    }
}
