package co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;


/**
 * Created by alejandro.jimenez on 29/11/2016.
 */

public class SubfragmentoDisponibilidadRegional extends Fragment {

    private CardView btnRegionalBogota;
    private CardView btnRegionalCaribe;
    private CardView btnRegionalNoroccidente;
    private CardView btnRegionalNororiente;
    private CardView btnRegionalSuroccidente;
    private CardView btnRegionalSuroriente;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_disponibilidad_regional, container, false);
        initComponents(vista);
        asignarEventos();
        return vista;
    }

    private void initComponents(View vista) {
        this.btnRegionalSuroriente = (CardView) vista.findViewById(R.id.btnRegionalSuroriente);
        this.btnRegionalSuroccidente = (CardView) vista.findViewById(R.id.btnRegionalSuroccidente);
        this.btnRegionalNororiente = (CardView) vista.findViewById(R.id.btnRegionalNororiente);
        this.btnRegionalNoroccidente = (CardView) vista.findViewById(R.id.btnRegionalNoroccidente);
        this.btnRegionalCaribe = (CardView) vista.findViewById(R.id.btnRegionalCaribe);
        this.btnRegionalBogota = (CardView) vista.findViewById(R.id.btnRegionalBogota);
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Consulta por regional");
        home.getSupportActionBar().setSubtitle("Disponibilidad de Red");
    }

    private void asignarEventos() {
        View.OnClickListener eventoClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String regional = "";
                switch (view.getId()) {
                    case R.id.btnRegionalBogota:
                        regional = "BOGOTA";
                        break;
                    case R.id.btnRegionalCaribe:
                        regional = "CARIBE";
                        break;
                    case R.id.btnRegionalNoroccidente:
                        regional = "NOROCCIDENTE";
                        break;
                    case R.id.btnRegionalNororiente:
                        regional = "NORORIENTE";
                        break;
                    case R.id.btnRegionalSuroccidente:
                        regional = "SUROCCIDENTE";
                        break;
                    case R.id.btnRegionalSuroriente:
                        regional = "SURORIENTE";
                        break;
                }
                StringBuilder builder = new StringBuilder();
                builder.append("[");
                builder.append(regional);
                builder.append("]?[]?[]?[]?[]?[]");
                Bundle bundle = new Bundle();
                bundle.putString("data-request",builder.toString());
                bundle.putString("tipoCaja","grupoCajas");
                FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(), bundle, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
            }
        };

        btnRegionalBogota.setOnClickListener(eventoClick);
        btnRegionalCaribe.setOnClickListener(eventoClick);
        btnRegionalNoroccidente.setOnClickListener(eventoClick);
        btnRegionalNororiente.setOnClickListener(eventoClick);
        btnRegionalSuroccidente.setOnClickListener(eventoClick);
        btnRegionalSuroriente.setOnClickListener(eventoClick);
    }
}
