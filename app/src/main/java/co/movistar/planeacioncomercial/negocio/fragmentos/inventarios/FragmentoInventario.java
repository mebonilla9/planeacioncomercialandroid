package co.movistar.planeacioncomercial.negocio.fragmentos.inventarios;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppArticulos;
import co.movistar.planeacioncomercial.modelo.entidades.AppPuntos;
import co.movistar.planeacioncomercial.modelo.entidades.AppUsuarios;
import co.movistar.planeacioncomercial.modelo.servicios.AppArticulosService;
import co.movistar.planeacioncomercial.modelo.servicios.AppPuntosService;
import co.movistar.planeacioncomercial.modelo.servicios.AppUsuariosService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.adapters.inventarios.ContenedorInformacionArticulosAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public class FragmentoInventario extends Fragment {

    private ProgressDialog dialogo;

    private RecyclerView rvArticulos;
    private ContenedorInformacionArticulosAdapter adapter;
    private MaterialBetterSpinner spSubordinados;
    private Spinner spPuntos;
    private LinearLayout lnInvSubordinados;
    private Long idUsuario;
    private Long idPunto;

    private List<AppUsuarios> listaUsuarios;
    private List<AppPuntos> listaPuntos;
    private ImageButton btnLimpiar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_inventarios, container, false);
        this.btnLimpiar = (ImageButton) vista.findViewById(R.id.btnLimpiar);
        this.rvArticulos = (RecyclerView) vista.findViewById(R.id.rvArticulos);
        this.rvArticulos.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.rvArticulos.setHasFixedSize(true);
        this.spSubordinados = (MaterialBetterSpinner) vista.findViewById(R.id.spSubordinados);
        this.lnInvSubordinados = (LinearLayout) vista.findViewById(R.id.lnInvSubordinados);
        this.spSubordinados.setVisibility(View.GONE);
        this.spPuntos = (Spinner) vista.findViewById(R.id.spPuntos);
        this.spPuntos.setVisibility(View.GONE);
        this.dialogo = ProgresoUtil.getDialogo(getActivity(), "Obteniendo Información", "Estamos procesando su información, espere...");
        this.idUsuario = Long.parseLong(PreferenciasUtil.obtenerPreferencias("id_usuario", getActivity()));
        consultarSubordinados();
        consultarPuntos();
        return vista;
    }

    private void consultarSubordinados() {
        Callback<List<AppUsuarios>> callback = new Callback<List<AppUsuarios>>() {
            @Override
            public void onResponse(Call<List<AppUsuarios>> call, Response<List<AppUsuarios>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    lnInvSubordinados.setVisibility(View.GONE);
                    return;
                }
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                listaUsuarios = response.body();
                cargarInfoSubordinados();
                spSubordinados.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppUsuarios>> call, Throwable t) {

            }
        };
        new AppPuntosService(getActivity()).consultarSubordinados(callback);
    }

    private void cargarInfoSubordinados() {
        List<String> misSubordinados = new ArrayList<>();
        for (AppUsuarios usuario : listaUsuarios) {
            if (usuario.getNombre() != null) {
                misSubordinados.add(usuario.getNombre());
            }
        }
        spSubordinados.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                misSubordinados
        ));

    }

    private void consultarPuntos() {
        Callback<List<AppPuntos>> callback = new Callback<List<AppPuntos>>() {
            @Override
            public void onResponse(Call<List<AppPuntos>> call, Response<List<AppPuntos>> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                listaPuntos = response.body();
                cargarInformacionPuntos(listaPuntos);
                spPuntos.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppPuntos>> call, Throwable t) {

            }
        };
        new AppPuntosService(getActivity()).obtenerPuntosPorUsuario(this.idUsuario.toString(), callback);
    }

    private void cargarInformacionPuntos(List<AppPuntos> listaPuntos) {
        List<String> misPuntos = new ArrayList<>();
        for (AppPuntos punto : listaPuntos) {
            if (punto.getNombrePunto() != null) {
                misPuntos.add(punto.getNombrePunto() + "-" + punto.getIdPunto());
            }
        }
        spPuntos.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                listaPuntos
        ));
    }

    private void consultarArticulos() {
        Callback<List<AppArticulos>> callback = new Callback<List<AppArticulos>>() {
            @Override
            public void onResponse(Call<List<AppArticulos>> call, Response<List<AppArticulos>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                List<AppArticulos> listaArticulos = response.body();
                cargarInformacionArticulos(listaArticulos);
            }

            @Override
            public void onFailure(Call<List<AppArticulos>> call, Throwable t) {

            }
        };
        new AppArticulosService(getActivity()).obtenerArticulosPunto(idPunto.toString(), callback);
    }

    private void cargarInformacionArticulos(List<AppArticulos> listaArticulos) {
        this.adapter = new ContenedorInformacionArticulosAdapter(listaArticulos);
        this.rvArticulos.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        asignarEventos();
        reDistribuirActivity();
    }

    private void asignarEventos() {
        TextWatcher eventoCambio = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (spSubordinados.getText().hashCode() == charSequence.hashCode()) {
                    idUsuario = buscarIdSubordinado(charSequence.toString());
                    consultarPuntos();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //
            }
        };

        View.OnClickListener clickLimpiar = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spSubordinados.setText("");
                spPuntos.setSelection(0);
                idUsuario = Long.parseLong(PreferenciasUtil.obtenerPreferencias("id_usuario", getActivity()));
                consultarPuntos();
            }
        };

        spPuntos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idPunto = Long.parseLong(listaPuntos.get(position).getIdPunto());
                consultarArticulos();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSubordinados.addTextChangedListener(eventoCambio);
        btnLimpiar.setOnClickListener(clickLimpiar);
    }

    private Long buscarIdSubordinado(String nombre) {
        Long idUsuario = 0L;
        for (AppUsuarios usuario : this.listaUsuarios) {
            if (usuario.getNombre().equals(nombre))
                idUsuario = usuario.getIdUsuario();
        }
        return idUsuario;
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Inventarios");
        home.getSupportActionBar().setSubtitle("Selección de Personal a cargo");
    }
}
