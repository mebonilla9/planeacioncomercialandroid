package co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by Lord_Nightmare on 1/03/17.
 */

public class SubfragmentoDisponibilidadRespuestaGrupoCajas extends Fragment {

    private android.widget.TextView lblTotalCajas;
    private android.widget.TextView lblCajaA;
    private android.widget.TextView lblDataCajaA;
    private android.widget.TextView lblCajaB;
    private android.widget.TextView lblDataCajaB;
    private android.widget.TextView lblCajaC;
    private android.widget.TextView lblDataCajaC;

    private List<AppDisponibilidad> cajas;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_disponibilidad_respuesta_grupo_cajas, container, false);
        initComponents(vista);
        this.cajas = (ArrayList<AppDisponibilidad>)getArguments().getSerializable("cajas");
        asignarInformacionRespuesta();
        return vista;
    }

    private void initComponents(View vista) {
        this.lblDataCajaC = (TextView) vista.findViewById(R.id.lblDataCajaC);
        this.lblCajaC = (TextView) vista.findViewById(R.id.lblCajaC);
        this.lblDataCajaB = (TextView) vista.findViewById(R.id.lblDataCajaB);
        this.lblCajaB = (TextView) vista.findViewById(R.id.lblCajaB);
        this.lblDataCajaA = (TextView) vista.findViewById(R.id.lblDataCajaA);
        this.lblCajaA = (TextView) vista.findViewById(R.id.lblCajaA);
        this.lblTotalCajas = (TextView) vista.findViewById(R.id.lblTotalCajas);
    }

    private void asignarInformacionRespuesta(){
        this.lblTotalCajas.setText(FormularioUtil.convertirFormatoMiles(this.sumatoriaCajas().toString()));
        //sumatoriaCajas();
        asignarValoresCajas();
    }

    @NonNull
    private Long sumatoriaCajas() {
        long sumatoria = 0;
        for (int i = 0; i < cajas.size(); i++) {
            sumatoria += Long.parseLong(cajas.get(i).getqOferta());
        }
        return sumatoria;
    }

    private void asignarValoresCajas() {
        for (int i = 0; i < cajas.size(); i++) {
            switch (cajas.get(i).getCategoria()) {
                case "A":
                    this.lblDataCajaA.setText(FormularioUtil.convertirFormatoMiles(cajas.get(i).getqOferta().toString()));
                    break;
                case "B":
                    this.lblDataCajaB.setText(FormularioUtil.convertirFormatoMiles(cajas.get(i).getqOferta().toString()));
                    break;
                case "C":
                    this.lblDataCajaC.setText(FormularioUtil.convertirFormatoMiles(cajas.get(i).getqOferta().toString()));
                    break;
            }
        }
    }
}
