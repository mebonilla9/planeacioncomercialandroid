package co.movistar.planeacioncomercial.negocio.interceptor;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import java.io.IOException;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.NetworkUtil;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lord_nightmare on 17/10/17.
 */

public class ConnectivityInterceptor implements Interceptor {

    private Context contexto;
    private DialogInterface.OnClickListener clickSalir;

    public ConnectivityInterceptor(final Context contexto){
        this.contexto = contexto;
        clickSalir = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((Activity) contexto).moveTaskToBack(true);
                ((Activity) contexto).finishAffinity();
            }
        };
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!NetworkUtil.isOnline(contexto)) {
            AlertaUtil.mostrarAlertaAsync("Error Conexión", EMensajes.ERROR_CONEXION_INTERNET.getDescripcion(),clickSalir, null,contexto);
        }
        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }
}
