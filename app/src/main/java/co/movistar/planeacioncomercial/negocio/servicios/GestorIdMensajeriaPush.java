package co.movistar.planeacioncomercial.negocio.servicios;

import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppLlaves;
import co.movistar.planeacioncomercial.modelo.servicios.AppLlavesService;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 23/12/16.
 */

public class GestorIdMensajeriaPush extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String tokenReciente = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token servicio: "+ tokenReciente);
        PreferenciasUtil.guardarPreferencias(getString(R.string.fcm_token),tokenReciente,getApplicationContext());
        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.tema_notificaciones));

        registrarLlaveServidor(tokenReciente);
    }

    private void registrarLlaveServidor(String token){
        AppLlaves llave = new AppLlaves();
        llave.setLlave(token);
        Callback<ResponseBody> callback = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                String respuesta = response.body().toString();
                Toast.makeText(GestorIdMensajeriaPush.this, respuesta, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AlertaUtil.mostrarAlerta("Registro de Mensajeria Push","Error registrando el token de mensajeria",null,null,getApplicationContext());
            }
        };
        new AppLlavesService(getApplicationContext()).insertarLlave(llave,callback);
    }
}
