package co.movistar.planeacioncomercial.negocio.utilidades;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by lord_nightmare on 8/09/17.
 */

public final class ViewUtil {

    public static void disableAllViews(View v){
        if (v == null){
            return;
        }
        v.setEnabled(false);
        if(v instanceof ViewGroup){

            for (int i = 0; i < ((ViewGroup)v).getChildCount();  i++) {
                View view = ((ViewGroup)v).getChildAt(i);
                disableAllViews(view);
            }
        }
    }
    public static void enableAllViews(View v){
        if (v == null){
            return;
        }
        v.setEnabled(true);
        if(v instanceof ViewGroup){

            for (int i = 0; i < ((ViewGroup)v).getChildCount();  i++) {
                View view = ((ViewGroup)v).getChildAt(i);
                enableAllViews(view);
            }
        }
    }

}
