package co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.DireccionDTO;
import co.movistar.planeacioncomercial.modelo.dto.InfoDisponibilidadDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import co.movistar.planeacioncomercial.modelo.servicios.AppDisponibilidadService;
import co.movistar.planeacioncomercial.negocio.adapters.disponibilidad.ContenedorInformacionCapacidadesAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 2/12/16.
 */

public class SubfragmentoDisponibilidadRespuesta extends Fragment {

    private TextView lblRegional;
    private TextView lblLocalidad;
    private TextView lblDepartamento;
    private TextView lblDistrito;
    private TextView lblArmario;
    private TextView lblCaja;
    private RecyclerView rvItemsCapacidades;

    private ContenedorInformacionCapacidadesAdapter capacidadesAdapter;

    private String dataRequest;
    private DireccionDTO dataRequestDire;
    private String tipoCaja;

    private ProgressDialog dialogo;
    private android.widget.FrameLayout contenedorCajas;

    private DialogInterface.OnClickListener clickRetornarError = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            FragmentoUtil.obtenerFragmentoSeleccionado(R.id.contenedorFragmentos, null, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_disponibilidad_respuesta, container, false);
        initComponents(vista);
        this.dataRequest = getArguments().getString("data-request");
        this.dataRequestDire = (DireccionDTO) getArguments().getSerializable("data-request-dire");
        this.tipoCaja = getArguments().getString("tipoCaja");
        dialogo = ProgresoUtil.getDialogo(getActivity(), "Obteniendo Información", "Estamos procesando su información, espere...");
        consultarInformacionRespuestaDisponibilidad();
        return vista;
    }

    private void initComponents(View vista) {
        this.rvItemsCapacidades = (RecyclerView) vista.findViewById(R.id.rvItemsCapacidades);
        this.contenedorCajas = (FrameLayout) vista.findViewById(R.id.contenedorCajas);
        this.lblCaja = (TextView) vista.findViewById(R.id.lblCaja);
        this.lblArmario = (TextView) vista.findViewById(R.id.lblArmario);
        this.lblDistrito = (TextView) vista.findViewById(R.id.lblDistrito);
        this.lblLocalidad = (TextView) vista.findViewById(R.id.lblLocalidad);
        this.lblDepartamento = (TextView) vista.findViewById(R.id.lblDepartamento);
        this.lblRegional = (TextView) vista.findViewById(R.id.lblRegional);
    }

    private void consultarInformacionRespuestaDisponibilidad() {
        Callback<InfoDisponibilidadDTO> callback = new Callback<InfoDisponibilidadDTO>() {
            @Override
            public void onResponse(Call<InfoDisponibilidadDTO> call, Response<InfoDisponibilidadDTO> response) {
                try{
                    if (!response.isSuccessful()) {
                        AlertaUtil.mostrarAlerta("Disponibilidad de red", response.errorBody().string(), clickRetornarError, null, getActivity());
                        ProgresoUtil.ocultarDialogoProgreso(dialogo);
                        return;
                    }
                    InfoDisponibilidadDTO listaDisponibilidad = response.body();
                    asignarInformacionRespuesta(listaDisponibilidad);
                    reAsignarValorSubTitulo();
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                } catch (IOException e){

                }
            }

            @Override
            public void onFailure(Call<InfoDisponibilidadDTO> call, Throwable t) {
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                AlertaUtil.mostrarAlerta("Error Consulta Ventas!", t.getMessage(), null, null, getActivity());
            }
        };
        if (this.dataRequestDire != null) {
            new AppDisponibilidadService(getActivity()).consultarInformacionDirecciones(this.dataRequestDire, callback);
        } else {
            new AppDisponibilidadService(getActivity()).consultarInformacionDisponibilidad(this.dataRequest, callback);
        }
    }

    private void asignarInformacionRespuesta(InfoDisponibilidadDTO lista) {
        asignarValoresTitulos(lista.getCabecera());
//        this.lblTotalCajas.setText(this.sumatoriaCajas(lista.getCajas()).toString());
//        asignarValoresCajas(lista.getCajas());
        generarFragmentoCaja(lista.getCajas());
        cargarInformacionCapacidades(lista.getCapacidades());
    }

    private void generarFragmentoCaja(List<AppDisponibilidad> cajas) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("cajas", (ArrayList<AppDisponibilidad>) cajas);
        Fragment fragmentoCaja = null;
        switch (this.tipoCaja) {
            case "grupoCajas":
                fragmentoCaja = new SubfragmentoDisponibilidadRespuestaGrupoCajas();
                break;
            case "caja":
                fragmentoCaja = new SubfragmentoDisponibilidadRespuestaCaja();
                break;
        }
        fragmentoCaja.setArguments(arguments);
        getChildFragmentManager().beginTransaction().replace(R.id.contenedorCajas, fragmentoCaja).commit();
    }

    private void asignarValoresTitulos(AppDisponibilidad cabecera) {
        this.lblRegional.setText(cabecera.getRegional());
        this.lblDepartamento.setText(cabecera.getDepartamento());
        this.lblLocalidad.setText(cabecera.getLocalidad());
        this.lblDistrito.setText(cabecera.getDistrito());
        this.lblArmario.setText(cabecera.getArmario());
        this.lblCaja.setText(cabecera.getCaja());
    }


    private void reAsignarValorSubTitulo() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("Resumen de consulta");
    }

    private void cargarInformacionCapacidades(List<AppDisponibilidad> capacidades) {
        if (!capacidades.isEmpty()) {
            this.capacidadesAdapter = new ContenedorInformacionCapacidadesAdapter(getActivity(), capacidades);
            this.rvItemsCapacidades.setAdapter(capacidadesAdapter);
            this.rvItemsCapacidades.setHasFixedSize(true);
            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
            rvItemsCapacidades.setLayoutManager(layoutManager);
        }
    }

}
