package co.movistar.planeacioncomercial.negocio.adapters.archivos;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppArchivos;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;

/**
 * Created by Lord_Nightmare on 31/03/17.
 */

public class ArchivosAdapter extends RecyclerView.Adapter<ArchivosAdapter.ViewHolder> {

    private List<AppArchivos> listaArchivos;
    private Context contexto;

    public ArchivosAdapter(List<AppArchivos> listaArchivos, Context contexto) {
        this.listaArchivos = listaArchivos;
        this.contexto = contexto;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_archivos, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppArchivos archivo = this.listaArchivos.get(position);
        holder.txtNombreArchivo.setText(archivo.getNombreArchivo());
        Glide.with(this.contexto)
                .load(archivo.getRuta())
                .into(holder.imgArchivo);

        asignarEvento(holder.cvItemArchivo, archivo.getRuta(), archivo.getNombreArchivo());
    }

    private void asignarEvento(CardView cvItemArchivo, final String ruta, final String nombre) {
        cvItemArchivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle argumentos = new Bundle();
                argumentos.putString("ruta", ruta);
                argumentos.putString("nombre", nombre);
                FragmentoUtil.obtenerFragmentoSeleccionado(v.getId(), argumentos, ((FragmentActivity) contexto).getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.listaArchivos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgArchivo;
        private TextView txtNombreArchivo;
        private CardView cvItemArchivo;

        public ViewHolder(View itemView) {
            super(itemView);
            imgArchivo = (ImageView) itemView.findViewById(R.id.imgArchivo);
            txtNombreArchivo = (TextView) itemView.findViewById(R.id.txtNombreArchivo);
            cvItemArchivo = (CardView) itemView.findViewById(R.id.cvItemArchivo);
        }
    }
}
