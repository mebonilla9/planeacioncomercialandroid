package co.movistar.planeacioncomercial.negocio.subfragmentos.calcularip;

import java.util.List;

import co.movistar.planeacioncomercial.modelo.entidades.AppGestionado;
import co.movistar.planeacioncomercial.negocio.constantes.ETipoCalcularIp;

/**
 * Created by Lord_Nightmare on 12/03/17.
 */

public class SubfragmentoCalcularIpDataGestionado extends SubfragmentoCalcularIpDataFragment {

    private final ETipoCalcularIp tipo = ETipoCalcularIp.TIPO_APP_GESTIONADO;
    private List<AppGestionado> listaGestionados;

    @Override
    public void cargarData() {
        this.setListaGestionados((List<AppGestionado>) getArguments().getSerializable("data"));
        generarVista(this.getListaGestionados(),this.tipo);
    }

    @Override
    public void actualizarDataSet() {
        this.cargarDatosView(this.listaGestionados, this.tipo);
    }
    public List<AppGestionado> getListaGestionados() {
        return listaGestionados;
    }

    public void setListaGestionados(List<AppGestionado> listaGestionados) {
        this.listaGestionados = listaGestionados;
    }

}
