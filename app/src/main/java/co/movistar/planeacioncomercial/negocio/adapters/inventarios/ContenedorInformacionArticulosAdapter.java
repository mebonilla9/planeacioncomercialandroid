package co.movistar.planeacioncomercial.negocio.adapters.inventarios;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppArticulos;

/**
 * Created by lord_nightmare on 13/09/17.
 */

public class ContenedorInformacionArticulosAdapter extends RecyclerView.Adapter<ContenedorInformacionArticulosAdapter.ViewHolder> {

    private List<AppArticulos> listaArticulos;

    public ContenedorInformacionArticulosAdapter(List<AppArticulos> listaArticulos) {
        this.listaArticulos = listaArticulos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContenedorInformacionArticulosAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inventarios_articulos, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppArticulos articulo = this.listaArticulos.get(position);
        holder.txtNombreArticulo.setText(articulo.getNombreArticulo());
        holder.txtCantidadArticulo.setText(articulo.getCantidad().toString());
    }

    @Override
    public int getItemCount() {
        return this.listaArticulos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txtNombreArticulo;
        private TextView txtCantidadArticulo;

        public ViewHolder(View itemView) {
            super(itemView);
            txtNombreArticulo = (TextView) itemView.findViewById(R.id.txtNombreArticulo);
            txtCantidadArticulo = (TextView) itemView.findViewById(R.id.txtCantidadArticulo);
        }
    }
}
