package co.movistar.planeacioncomercial.negocio.utilidades;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created by lord_nightmare on 21/04/17.
 */

public class DotNetDateConverter implements JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String s = json.getAsJsonPrimitive().getAsString();
        long longValue = Long.parseLong(s);
        return new Date(longValue);
    }
}
