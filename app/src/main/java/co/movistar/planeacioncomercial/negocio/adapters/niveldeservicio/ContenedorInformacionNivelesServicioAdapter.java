package co.movistar.planeacioncomercial.negocio.adapters.niveldeservicio;

import android.support.annotation.ColorInt;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.eralp.circleprogressview.CircleProgressView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppServicio;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by lord_nightmare on 17/04/17.
 */

public class ContenedorInformacionNivelesServicioAdapter extends RecyclerView.Adapter<ContenedorInformacionNivelesServicioAdapter.ViewHolder> {

    private List<AppServicio> lista;
    private String tipoVisita;
    private static final int ANGULO_INICIO = 1;

    @ColorInt
    public static final int RED_DARK = 0xFFCC0000;
    @ColorInt
    public static final int GREEN_DARK = 0xFF669900;

    public ContenedorInformacionNivelesServicioAdapter(List<AppServicio> lista, String tipoVisita) {
        this.lista = lista;
        this.tipoVisita = tipoVisita;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nivel_de_servicio, parent, false);
        return new ContenedorInformacionNivelesServicioAdapter.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppServicio servicio = this.lista.get(position);
        if (servicio.getCanal().equals(this.tipoVisita)) {
            if (this.tipoVisita.equals("Presencial")) {
                holder.txtVisita.setText("Visitas");
            } else {
                holder.txtVisita.setText("Llamadas");
            }
            holder.txtValorVisita.setText(FormularioUtil.convertirFormatoMiles(servicio.getVisitas().toString()));
            holder.txtRegional.setText(/*servicio.getRegional().equals("PAIS") || servicio.getRegional().equals("Pais") ? "" : */servicio.getRegional());
            holder.txtValorTma.setText(servicio.getTma());
            holder.cpvProgresoNivelServicio.setTextEnabled(false);
            Double progreso = Math.floor(servicio.getNivelServicio() * 100);
            holder.cpvProgresoNivelServicio.setProgressWithAnimation(progreso.intValue(), 2000);
            holder.txtServicioValorPorcentaje.setText(progreso.intValue() + "%");
            if (progreso.intValue() > 79) {
                holder.cpvProgresoNivelServicio.setTextColor(GREEN_DARK);
                holder.cpvProgresoNivelServicio.setCircleColor(GREEN_DARK);
                holder.txtServicioValorPorcentaje.setTextColor(GREEN_DARK);
            } else {
                holder.cpvProgresoNivelServicio.setTextColor(RED_DARK);
                holder.cpvProgresoNivelServicio.setCircleColor(RED_DARK);
                holder.txtServicioValorPorcentaje.setTextColor(RED_DARK);
            }
        }

    }

    @Override
    public int getItemCount() {
        return this.lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleProgressView cpvProgresoNivelServicio;
        private TextView txtRegional;
        private TextView txtVisita;
        private TextView txtValorVisita;
        private TextView txtValorTma;
        private TextView txtServicioValorPorcentaje;

        public ViewHolder(View itemView) {
            super(itemView);

            cpvProgresoNivelServicio = (CircleProgressView) itemView.findViewById(R.id.cpvProgresoNivelServicio);
            cpvProgresoNivelServicio.setTextEnabled(true);
            cpvProgresoNivelServicio.setInterpolator(new AccelerateDecelerateInterpolator());
            cpvProgresoNivelServicio.setStartAngle(ANGULO_INICIO);

            txtRegional = (TextView) itemView.findViewById(R.id.txtRegional);
            txtVisita = (TextView) itemView.findViewById(R.id.txtVisita);
            txtValorVisita = (TextView) itemView.findViewById(R.id.txtValorVisita);
            txtValorTma = (TextView) itemView.findViewById(R.id.txtValorTma);
            txtServicioValorPorcentaje = (TextView) itemView.findViewById(R.id.txtServicioValorPorcentaje);
        }
    }
}
