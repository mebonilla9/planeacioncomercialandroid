package co.movistar.planeacioncomercial.negocio.subfragmentos.liquidacion;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppLiquidacionHist;
import co.movistar.planeacioncomercial.modelo.servicios.AppLiquidacionHistService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gloria.garzon on 15/02/2017.
 */

public class SubfragmentoLiquidacionMenu extends Fragment {

    private MaterialBetterSpinner spCategoria;
    private ProgressDialog dialogo;
    private FloatingActionButton fabActivity;
    private String periodo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_liquidacion_menu, container, false);
        initComponents(vista);
        asignarEventos();
        dialogo = ProgresoUtil.getDialogo(getActivity(), "Obteniendo Información", "Estamos procesando su información, espere...");
        cargarListas();
        this.periodo = getArguments().getString("periodo");
        return vista;
    }

    private void initComponents(View vista) {
        this.spCategoria = (MaterialBetterSpinner) vista.findViewById(R.id.spCategoria);
        this.spCategoria.setVisibility(View.GONE);
        this.fabActivity = ((HomeActivity) getActivity()).getFabBotonFlotante();
        this.fabActivity.setEnabled(false);
        this.fabActivity.setImageDrawable(DrawableCompat.wrap(VectorDrawableCompat.create(getResources(), R.drawable.arrow_right_bold, null)));
    }

    private void asignarEventos() {
        TextWatcher cambioSpinner = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                habilitarBoton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        View.OnClickListener clickDetalle = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle argumentos = new Bundle();
                argumentos.putString("categoria",spCategoria.getText().toString());
                argumentos.putString("periodo",periodo);
                argumentos.putString("subfragmento","liquidacionDetalle");
                FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(),argumentos, getActivity().getSupportFragmentManager().beginTransaction(),R.id.contenedorFragmentos);
            }
        };

        this.fabActivity.setOnClickListener(clickDetalle);
        this.spCategoria.addTextChangedListener(cambioSpinner);
    }

    private void cargarListas() {
        Callback<List<AppLiquidacionHist>> callbackCategoria = new Callback<List<AppLiquidacionHist>>() {
            @Override
            public void onResponse(Call<List<AppLiquidacionHist>> call, Response<List<AppLiquidacionHist>> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppLiquidacionHist> listaCategorias = response.body();
                indexarListaCategorias(listaCategorias);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
                spCategoria.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppLiquidacionHist>> call, Throwable t) {

            }
        };
        new AppLiquidacionHistService(getActivity()).consultarCategoria(callbackCategoria);
    }

    private void indexarListaCategorias(List<AppLiquidacionHist> listaCategorias) {
        List<String> categorias = new ArrayList<>();
        for (AppLiquidacionHist appLiquidacionHist : listaCategorias) {
            if (appLiquidacionHist.getCategoria() != null){
                categorias.add(appLiquidacionHist.getCategoria());
            }
        }
        this.spCategoria.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                categorias
        ));
    }

    private void habilitarBoton() {
        if (!spCategoria.getText().toString().equals("")) {
            this.fabActivity.setEnabled(true);
            return;
        }
        this.fabActivity.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.fabActivity.show();
        asignarEventos();
    }
}
