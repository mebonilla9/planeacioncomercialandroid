package co.movistar.planeacioncomercial.negocio.actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;
import java.util.regex.Pattern;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.InformacionUsuarioDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppMenu;
import co.movistar.planeacioncomercial.modelo.entidades.AppUsuarios;
import co.movistar.planeacioncomercial.modelo.servicios.AppMenuService;
import co.movistar.planeacioncomercial.negocio.adapters.proyeccion.ContenedorInformacionProyeccionAdapter;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.MenuUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.PreferenciasUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 19/10/16.
 */

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbarPrincipal;
    private FrameLayout contenedorFragmentos;
    private FloatingActionButton fabBotonFlotante;
    private NavigationView nvCajaNavegacion;
    private DrawerLayout drawerlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initComponents();
        consultarInformacionMenu();
    }

    private void initComponents() {
        this.fabBotonFlotante = (FloatingActionButton) findViewById(R.id.fabBotonFlotante);
        this.contenedorFragmentos = (FrameLayout) findViewById(R.id.contenedorFragmentos);
        this.toolbarPrincipal = (Toolbar) findViewById(R.id.toolbarPrincipal);
        this.nvCajaNavegacion = (NavigationView) findViewById(R.id.nvCajaNavegacion);
        setSupportActionBar(this.toolbarPrincipal);
        this.drawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, this.drawerlayout, this.toolbarPrincipal, R.string.abrir_caja_navegacion, R.string.cerrar_caja_navegacion);
        this.drawerlayout.addDrawerListener(toggle);
        toggle.syncState();
        this.nvCajaNavegacion.setNavigationItemSelectedListener(this);
        this.fabBotonFlotante.hide();
        FragmentoUtil.obtenerFragmentoSeleccionado(R.id.contenedorFragmentos, null, getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            if (fm.getBackStackEntryCount() <= 1) {
                moveTaskToBack(true);
            } else if (fm.getBackStackEntryCount() == 1) {
                //String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount()).getName();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                restablecerDistribucionActividad();
            } else {
                fm.popBackStack();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.miCerrarSesion) {
            PreferenciasUtil.eliminarPreferencias("auth-token", this);
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            return true;
        }
        // Handle navigation view item clicks here.
        FragmentoUtil.obtenerFragmentoSeleccionado(item.getItemId(), null, getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
        this.drawerlayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public FloatingActionButton getFabBotonFlotante() {
        return fabBotonFlotante;
    }

    private void restablecerDistribucionActividad() {
        this.getSupportActionBar().setSubtitle("");
        this.getSupportActionBar().setTitle("Planeación Comercial");
        contenedorFragmentos.removeAllViews();
        this.fabBotonFlotante.hide();
    }

    private void consultarInformacionMenu() {
        Callback<InformacionUsuarioDTO> callback = new Callback<InformacionUsuarioDTO>() {
            @Override
            public void onResponse(Call<InformacionUsuarioDTO> call, Response<InformacionUsuarioDTO> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                InformacionUsuarioDTO infoUsuario = response.body();
                asignarInformacionUsuario(infoUsuario.getUsuario());
                activarOpcionesMenu(infoUsuario.getMenu());
                String tokenReciente = FirebaseInstanceId.getInstance().getToken();
                System.out.println("token servicio: "+ tokenReciente);
                FirebaseMessaging.getInstance().subscribeToTopic(infoUsuario.getUsuario().getCargo().getNomCargo());
            }

            @Override
            public void onFailure(Call<InformacionUsuarioDTO> call, Throwable t) {

            }
        };
        new AppMenuService(HomeActivity.this).consultarMenu(callback);
    }

    private void asignarInformacionUsuario(AppUsuarios usuario) {
        PreferenciasUtil.guardarPreferencias("id_usuario",usuario.getIdUsuario().toString(),HomeActivity.this);
        View headerView = this.nvCajaNavegacion.getHeaderView(0);
        TextView txtNombreUsuario = (TextView) headerView.findViewById(R.id.txtNombreUsuario);
        TextView txtCorreoUsuario = (TextView) headerView.findViewById(R.id.txtCorreoUsuario);
        ImageView imagenUsuario = (ImageView) headerView.findViewById(R.id.imagenUsuario);
        txtNombreUsuario.setText(usuario.getNombre());
        txtCorreoUsuario.setText(usuario.getCorreo());

        String iniciales = "";
        String[] partesNombre = usuario.getNombre().split(Pattern.quote(" "));
        iniciales += partesNombre[0].charAt(0);
        if (partesNombre.length > 3) {
            iniciales += partesNombre[2].charAt(0);
        } else {
            iniciales += partesNombre[1].charAt(0);
        }
        TextDrawable iconoIniciales = TextDrawable.builder()
                .beginConfig()
                .bold()
                //.width(imagenUsuario.getWidth()/2)
                //.height(imagenUsuario.getHeight()/2)
                .endConfig()
                .buildRound(iniciales, ContenedorInformacionProyeccionAdapter.GREEN_DARK);

        imagenUsuario.setImageDrawable(iconoIniciales);
    }

    private void activarOpcionesMenu(List<AppMenu> menu) {
        MenuUtil.activarOpcionesMenu(this.nvCajaNavegacion.getMenu(), menu);
    }
}
