package co.movistar.planeacioncomercial.negocio.subfragmentos.archivos;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;

/**
 * Created by Lord_Nightmare on 31/03/17.
 */

public class SubfragmentoVisorArchivos extends Fragment {

    private WebView wvNavegador;
    private String ruta;
    private String nombre;
    private ProgressDialog dialogo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_archivo, container, false);
        this.ruta = getArguments().getString("ruta");
        this.nombre = getArguments().getString("nombre");
        initComponents(vista);
        reDistribuirActivity();
        return vista;
    }

    private void initComponents(View vista) {
        this.wvNavegador = vista.findViewById(R.id.wvNavegador);
        this.wvNavegador.setWebViewClient(new WebViewClient());
        this.wvNavegador.getSettings().setLoadWithOverviewMode(true);
        this.wvNavegador.getSettings().setUseWideViewPort(true);
        this.wvNavegador.getSettings().setBuiltInZoomControls(true);
        this.wvNavegador.getSettings().setDisplayZoomControls(false);
        if (ruta.endsWith("pdf")) {
            dialogo = ProgresoUtil.getDialogo(getActivity(), getString(R.string.titulo_dialogo_data), getString(R.string.mensaje_dialogo_data));
            //ruta = "<iframe src='http://docs.google.com/viewer?url="+ruta+"' width='100%' height='100%' style='border: none;'></iframe>";
            ruta = "https://docs.google.com/gview?embedded=true&url=" + ruta;
            wvNavegador.getSettings().setJavaScriptEnabled(true);
            this.wvNavegador.getSettings().setUseWideViewPort(false);
            this.wvNavegador.getSettings().setBuiltInZoomControls(false);
            this.wvNavegador.getSettings().setDisplayZoomControls(false);
            this.wvNavegador.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                }
            });
            //wvNavegador.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        }
        this.wvNavegador.loadUrl(ruta);
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setSubtitle(this.nombre);
    }


}
