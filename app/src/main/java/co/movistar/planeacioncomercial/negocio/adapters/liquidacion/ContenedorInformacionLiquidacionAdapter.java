package co.movistar.planeacioncomercial.negocio.adapters.liquidacion;

import android.content.Context;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppLiquidacionHist;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

/**
 * Created by Lord_Nightmare on 10/02/17.
 */

public class ContenedorInformacionLiquidacionAdapter extends RecyclerView.Adapter<ContenedorInformacionLiquidacionAdapter.ViewHolder> {

    private Context contexto;
    private List<AppLiquidacionHist> listaDetalle;

    public ContenedorInformacionLiquidacionAdapter(Context contexto, List<AppLiquidacionHist> listaDetalle) {
        this.contexto = contexto;
        this.listaDetalle = listaDetalle;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_liquidacion, parent, false);
        return new ContenedorInformacionLiquidacionAdapter.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppLiquidacionHist alh = this.listaDetalle.get(position);
        holder.txtProductoItem.setText(alh.getVariable());
        holder.txtMetaLiquidacion.setText("Meta del mes: " + alh.getMeta() + "");
        holder.txtEjecutadoLiquidacion.setText("Ejecutado: " + FormularioUtil.convertirFormatoMiles(alh.getEjecutado().longValue()+""));
        holder.txtCumplimientoLiquidacion.setText("Cumplimiento: " + FormularioUtil.convertirCumplimientoPorcentaje(alh.getCumplimiento()));
        if (alh.getPesoVariable() != null) {
            holder.txtPesoVariableLiquidacion.setText("Peso variable: " + FormularioUtil.convertirCumplimientoPorcentaje(alh.getPesoVariable()));
        }
        if (alh.getPesoCat() != null) {
            holder.txtPesoCategoriaLiquidacion.setText("Peso categoria: " + FormularioUtil.convertirCumplimientoPorcentaje(alh.getPesoCat()));
        }
        if (alh.getComIP() != null) {
            holder.txtComponenteIpLiquidacion.setText("Componentes IP: " + FormularioUtil.convertirCumplimientoPorcentaje(alh.getComIP()));
        }
        holder.imgItemLiquidacion.setImageDrawable(DrawableCompat.wrap(VectorDrawableCompat.create(contexto.getResources(), R.drawable.airplay, null)));

    }

    @Override
    public int getItemCount() {
        return this.listaDetalle.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtProductoItem;
        private ImageView imgItemLiquidacion;
        private TextView txtMetaLiquidacion;
        private TextView txtEjecutadoLiquidacion;
        private TextView txtCumplimientoLiquidacion;
        private TextView txtPesoVariableLiquidacion;
        private TextView txtPesoCategoriaLiquidacion;
        private TextView txtComponenteIpLiquidacion;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtProductoItem = (TextView) itemView.findViewById(R.id.txtProductoItem);
            this.imgItemLiquidacion = (ImageView) itemView.findViewById(R.id.imgItemLiquidacion);
            this.txtMetaLiquidacion = (TextView) itemView.findViewById(R.id.txtMetaLiquidacion);
            this.txtEjecutadoLiquidacion = (TextView) itemView.findViewById(R.id.txtEjecutadoLiquidacion);
            this.txtCumplimientoLiquidacion = (TextView) itemView.findViewById(R.id.txtCumplimientoLiquidacion);
            this.txtPesoVariableLiquidacion = (TextView) itemView.findViewById(R.id.txtPesoVariableLiquidacion);
            this.txtPesoCategoriaLiquidacion = (TextView) itemView.findViewById(R.id.txtPesoCategoriaLiquidacion);
            this.txtComponenteIpLiquidacion = (TextView) itemView.findViewById(R.id.txtComponenteIpLiquidacion);
        }


    }
}
