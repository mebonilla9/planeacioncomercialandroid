package co.movistar.planeacioncomercial.negocio.aplicacion;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.servicios.GestorIdMensajeriaPush;
import co.movistar.planeacioncomercial.negocio.servicios.ServicioMensajeriaPushFcm;

/**
 * Created by Lord_Nightmare on 28/12/16.
 */

public class PlaneacionComercialApplication extends MultiDexApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(PlaneacionComercialApplication.this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!servicioEnEjecucion(getString(R.string.service_id))){
            startService(new Intent(this, GestorIdMensajeriaPush.class));
        }
        if (!servicioEnEjecucion(getString(R.string.service_messaging))){
            startService(new Intent(this, ServicioMensajeriaPushFcm.class));
        }
    }

    private boolean servicioEnEjecucion(String servicio) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if(service.service.getClassName().equals(servicio)) {
                return true;
            }
        }
        return false;
    }
}
