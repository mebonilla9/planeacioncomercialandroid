package co.movistar.planeacioncomercial.negocio.adapters.comisiones;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppCostoxAlta;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;

import java.util.List;

public class ContenedorDetalleComisionesAdapter extends RecyclerView.Adapter<ContenedorDetalleComisionesAdapter.ViewHolder> {

    private List<AppCostoxAlta> dataSet;

    public ContenedorDetalleComisionesAdapter(List<AppCostoxAlta> dataSet) {
        this.dataSet = dataSet;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comisiones_detalle, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        AppCostoxAlta data = this.dataSet.get(position);
        holder.txtSegmentoFinanciero.setText(data.getSegmento());
        holder.txtValorParticipacion.setText(FormularioUtil.convertirCumplimientoPorcentaje(data.getParticipacion()));
        holder.txtValorCostoxAlta.setText(FormularioUtil.convertirFormatoMiles(data.getCostoPorAlta() + ""));
        holder.txtValorVecesArpu.setText(data.getVecesArpu() + "");
    }

    public int getItemCount() {
        return this.dataSet.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSegmentoFinanciero;
        private TextView txtValorCostoxAlta;
        private TextView txtValorParticipacion;
        private TextView txtValorVecesArpu;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtSegmentoFinanciero = itemView.findViewById(R.id.txtSegmentoFinanciero);
            this.txtValorParticipacion = itemView.findViewById(R.id.txtValorParticipacion);
            this.txtValorCostoxAlta = itemView.findViewById(R.id.txtValorCostoxAlta);
            this.txtValorVecesArpu = itemView.findViewById(R.id.txtValorVecesArpu);
        }
    }
}
