package co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import co.movistar.planeacioncomercial.modelo.servicios.AppDisponibilidadService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lord_Nightmare on 2/12/16.
 */

public class SubfragmentoDisponibilidadDepartamento extends Fragment {

    private MaterialBetterSpinner spDepartamentos;
    private MaterialBetterSpinner spLocalidades;
    private Button btnConsultaDepartamentoLocalidad;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_disponibilidad_departamento, container, false);
        initComponents(vista);
        cargarListaInicial();
        asignarListeners();
        return vista;
    }

    private void initComponents(View vista) {
        this.btnConsultaDepartamentoLocalidad = (Button) vista.findViewById(R.id.btnConsultaDepartamentoLocalidad);
        this.spLocalidades = (MaterialBetterSpinner) vista.findViewById(R.id.spLocalidades);
        this.spDepartamentos = (MaterialBetterSpinner) vista.findViewById(R.id.spDepartamentos);
        this.spDepartamentos.setVisibility(View.GONE);
        this.spLocalidades.setVisibility(View.GONE);
        btnConsultaDepartamentoLocalidad.setEnabled(false);
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Consulta por departamento");
        home.getSupportActionBar().setSubtitle("Disponibilidad de Red");
    }

    private void cargarListaInicial() {
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaDepartamentos = response.body();
                indexarListaDepartamentosSpinner(listaDepartamentos);
                spDepartamentos.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {

            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionDepartamentos(callback);
    }

    private void indexarListaDepartamentosSpinner(List<AppDisponibilidad> listaDepartamentos) {
        List<String> departamentos = new ArrayList<>();
        for (AppDisponibilidad disponible : listaDepartamentos) {
            if (disponible.getRegional() != null) {
                departamentos.add(disponible.getRegional());
            }
        }
        spDepartamentos.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                departamentos
        ));
    }

    private void asignarListeners() {
        TextWatcher eventoCambio = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(spDepartamentos.getText().hashCode() == charSequence.hashCode()){
                    cargarListaLocalidades(charSequence.toString());
                }
                habilitarBoton();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //
            }
        };

        View.OnClickListener eventoClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder builder = new StringBuilder();
                builder.append("[]?[");
                builder.append(spDepartamentos.getText().toString());
                builder.append("]?[");
                builder.append(spLocalidades.getText().toString());
                builder.append("]?[]?[]?[]");
                Bundle bundle = new Bundle();
                bundle.putString("data-request",builder.toString());
                bundle.putString("tipoCaja","grupoCajas");
                FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(), bundle, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
            }
        };

        spDepartamentos.addTextChangedListener(eventoCambio);
        spLocalidades.addTextChangedListener(eventoCambio);
        btnConsultaDepartamentoLocalidad.setOnClickListener(eventoClick);
    }

    private void cargarListaLocalidades(String departamento){
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaLocalidades = response.body();
                indexarListaLocalidadesSpinner(listaLocalidades);
                spLocalidades.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {
                t.printStackTrace();
            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionLocalidades(departamento,callback);
    }

    private void indexarListaLocalidadesSpinner(List<AppDisponibilidad> listaLocalidades) {
        List<String> localidades = new ArrayList<>();
        for (AppDisponibilidad disponible : listaLocalidades) {
            if (disponible.getLocalidad() != null) {
                localidades.add(disponible.getLocalidad());
            }
        }
        spLocalidades.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                localidades
        ));
    }

    private void habilitarBoton(){
        if(spDepartamentos.getText().toString().equals("")){
            btnConsultaDepartamentoLocalidad.setEnabled(false);
            return;
        }
        btnConsultaDepartamentoLocalidad.setEnabled(true);
    }
}
