package co.movistar.planeacioncomercial.negocio.servicios;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;

/**
 * Created by Lord_Nightmare on 23/12/16.
 */

public class ServicioMensajeriaPushFcm extends FirebaseMessagingService {

    private static final String TAG = "Notificacion";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);
        if(remoteMessage.getFrom().equals("/topics/" + getString(R.string.tema_notificaciones))){
            String titulo = remoteMessage.getNotification().getTitle();
            String mensaje = remoteMessage.getNotification().getBody();
            AlertaUtil.mostrarNotificacion(this,titulo,mensaje);
        }
    }


}
