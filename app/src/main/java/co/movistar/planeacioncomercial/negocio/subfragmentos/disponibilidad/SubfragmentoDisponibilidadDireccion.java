package co.movistar.planeacioncomercial.negocio.subfragmentos.disponibilidad;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.dto.DireccionDTO;
import co.movistar.planeacioncomercial.modelo.entidades.AppCruceVial;
import co.movistar.planeacioncomercial.modelo.entidades.AppDisponibilidad;
import co.movistar.planeacioncomercial.modelo.servicios.AppCruceVialService;
import co.movistar.planeacioncomercial.modelo.servicios.AppDisponibilidadService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ViewUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lord_nightmare on 17/07/17.
 */

public class SubfragmentoDisponibilidadDireccion extends Fragment {

    private MaterialBetterSpinner spTipoViaPrincipal;
    private EditText txtNumeroViaPrincipal;
    private RadioGroup rbgPrincipal;
    private RadioGroup rbgGeneradora;
    private RadioButton rbPrincipal;
    private RadioButton rbSurPrincipal;
    private RadioButton rbEstePrincipal;
    private EditText txtViaGeneradora;
    private EditText txtUbicacion;
    private RadioButton rbGeneradora;
    private RadioButton rbSurGeneradora;
    private RadioButton rbEsteGeneradora;
    private MaterialBetterSpinner spDepartamento;
    private MaterialBetterSpinner spCiudad;
    private TextView txtDireccionFinal;
    private Switch swTipoDomicilio;
    private LinearLayout lyDireccionador;

    private DireccionDTO direccion;
    private Button btnConsultarCaja;
    private TextView textView14;
    private View view1;
    private TextView lblViaPrincipal;
    private android.widget.ImageButton btnLimpiarPrincipal;
    private View view2;
    private TextView lblRegional;
    private TextView textView7;
    private android.widget.ImageButton btnLimpiarGeneradora;
    private View view3;
    private TextView lblRegion;
    private View view4;
    private TextView lblDireccion;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.subfragmento_disponibilidad_direccion, container, false);
        initComponents(vista);
        habilitarCaracteristicas();
        return vista;
    }

    private void initComponents(View vista) {
        this.txtDireccionFinal = (TextView) vista.findViewById(R.id.txtDireccionFinal);
        this.spCiudad = (MaterialBetterSpinner) vista.findViewById(R.id.spCiudad);
        this.spDepartamento = (MaterialBetterSpinner) vista.findViewById(R.id.spDepartamento);
        this.rbEsteGeneradora = (RadioButton) vista.findViewById(R.id.rbEsteGeneradora);
        this.rbSurGeneradora = (RadioButton) vista.findViewById(R.id.rbSurGeneradora);
        this.txtUbicacion = (EditText) vista.findViewById(R.id.txtUbicacion);
        this.txtViaGeneradora = (EditText) vista.findViewById(R.id.txtViaGeneradora);
        this.rbEstePrincipal = (RadioButton) vista.findViewById(R.id.rbEstePrincipal);
        this.rbSurPrincipal = (RadioButton) vista.findViewById(R.id.rbSurPrincipal);
        this.txtNumeroViaPrincipal = (EditText) vista.findViewById(R.id.txtNumeroViaPrincipal);
        this.spTipoViaPrincipal = (MaterialBetterSpinner) vista.findViewById(R.id.spTipoViaPrincipal);
        this.rbgPrincipal = (RadioGroup) vista.findViewById(R.id.rbgPrincipal);
        this.rbgGeneradora = (RadioGroup) vista.findViewById(R.id.rbgGeneradora);
        this.btnConsultarCaja = (Button) vista.findViewById(R.id.btnConsultarCaja);
        this.swTipoDomicilio = (Switch) vista.findViewById(R.id.swTipoDomicilio);
        this.swTipoDomicilio.setChecked(true);
        this.lyDireccionador = (LinearLayout) vista.findViewById(R.id.lyDireccionador);
        this.btnLimpiarPrincipal = (ImageButton) vista.findViewById(R.id.btnLimpiarPrincipal);
        this.btnLimpiarGeneradora = (ImageButton) vista.findViewById(R.id.btnLimpiarGeneradora);
        this.direccion = new DireccionDTO();
    }

    @Override
    public void onResume() {
        super.onResume();
        habilitarCaracteristicas();
        asignarEventos();
        reDistribuirActividad();
        cargarListasIniciales();
    }

    private void habilitarCaracteristicas() {
        spDepartamento.setVisibility(View.INVISIBLE);
        spTipoViaPrincipal.setVisibility(View.INVISIBLE);
        spCiudad.setVisibility(View.INVISIBLE);
    }

    private void asignarEventos() {
        RadioGroup.OnCheckedChangeListener cambioRadio = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (group.getId()) {
                    case R.id.rbgPrincipal:
                        rbPrincipal = (RadioButton) getActivity().findViewById(checkedId);
                        break;
                    case R.id.rbgGeneradora:
                        rbGeneradora = (RadioButton) getActivity().findViewById(checkedId);
                        break;
                }
                modificarDireccionFinal();
            }
        };
        rbgPrincipal.setOnCheckedChangeListener(cambioRadio);
        rbgGeneradora.setOnCheckedChangeListener(cambioRadio);

        View.OnFocusChangeListener eventoFoco = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ((EditText) v).setHint("");
                } else {
                    ((EditText) v).setHint("0");
                }
                modificarDireccionFinal();
            }
        };

        txtNumeroViaPrincipal.setOnFocusChangeListener(eventoFoco);
        txtViaGeneradora.setOnFocusChangeListener(eventoFoco);
        txtUbicacion.setOnFocusChangeListener(eventoFoco);

        TextWatcher observador = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                modificarDireccionFinal();
            }
        };

        spTipoViaPrincipal.addTextChangedListener(observador);
        spCiudad.addTextChangedListener(observador);
        txtNumeroViaPrincipal.addTextChangedListener(observador);

        TextWatcher observadorDepartamento = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cargarListaLocalidades(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        spDepartamento.addTextChangedListener(observadorDepartamento);

        View.OnClickListener clickFlotante = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.btnConsultarCaja:
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("data-request-dire", direccion);
                        bundle.putString("tipoCaja", "caja");
                        FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(), bundle, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
                        break;
                    case R.id.btnLimpiarPrincipal:
                        rbEstePrincipal.setChecked(false);
                        rbSurPrincipal.setChecked(false);
                        modificarDireccionFinal();
                        break;
                    case R.id.btnLimpiarGeneradora:
                        rbEsteGeneradora.setChecked(false);
                        rbSurGeneradora.setChecked(false);
                        modificarDireccionFinal();
                        break;
                }
            }
        };

        this.btnConsultarCaja.setOnClickListener(clickFlotante);
        this.btnLimpiarGeneradora.setOnClickListener(clickFlotante);
        this.btnLimpiarPrincipal.setOnClickListener(clickFlotante);

        CompoundButton.OnCheckedChangeListener eventoSwitch = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    ViewUtil.disableAllViews(lyDireccionador);
                    return;
                }
                ViewUtil.enableAllViews(lyDireccionador);
            }
        };

        swTipoDomicilio.setOnCheckedChangeListener(eventoSwitch);
    }

    private void modificarDireccionFinal() {
        // Espacio para stringBuilder para formar la direccion final
        direccion.setCruceVial(spTipoViaPrincipal.getText().toString());
        direccion.setNumeroViaPrincipal(txtNumeroViaPrincipal.getText().toString());
        direccion.setSurPrincipal(rbSurPrincipal.isChecked());
        direccion.setEstePrincipal(rbEstePrincipal.isChecked());
        direccion.setNumeroViaGeneradora(txtViaGeneradora.getText().toString());
        direccion.setSeccionFinal(txtUbicacion.getText().toString().equals("") ? 0 : Long.parseLong(txtUbicacion.getText().toString()));
        direccion.setSurGeneradora(rbSurGeneradora.isChecked());
        direccion.setEsteGeneradora(rbEsteGeneradora.isChecked());
        direccion.setCiudad(spCiudad.getText().toString());
        txtDireccionFinal.setText(direccion.toString());
    }

    private void cargarListasIniciales() {
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaDepartamentos = response.body();
                indexarListaDepartamentosSpinner(listaDepartamentos);
                spDepartamento.setVisibility(View.VISIBLE);
                spDepartamento.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {

            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionDepartamentos(callback);


        Callback<List<AppCruceVial>> callbackVial = new Callback<List<AppCruceVial>>() {
            @Override
            public void onResponse(Call<List<AppCruceVial>> call, Response<List<AppCruceVial>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppCruceVial> listaVias = response.body();
                indexarListaViasSpinner(listaVias);
                spTipoViaPrincipal.setVisibility(View.VISIBLE);
                spTipoViaPrincipal.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<AppCruceVial>> call, Throwable t) {

            }
        };
        new AppCruceVialService(getActivity()).consultarVias(callbackVial);
    }

    private void indexarListaViasSpinner(List<AppCruceVial> listaVias) {
        List<String> vias = new ArrayList<>();
        for (AppCruceVial via : listaVias) {
            if (via.getTipoVia() != null) {
                vias.add(via.getTipoVia());
            }
        }
        spTipoViaPrincipal.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                vias
        ));
    }

    private void cargarListaLocalidades(String departamento) {
        Callback<List<AppDisponibilidad>> callback = new Callback<List<AppDisponibilidad>>() {
            @Override
            public void onResponse(Call<List<AppDisponibilidad>> call, Response<List<AppDisponibilidad>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                List<AppDisponibilidad> listaLocalidades = response.body();
                indexarListaLocalidadesSpinner(listaLocalidades);
                spCiudad.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<AppDisponibilidad>> call, Throwable t) {
                t.printStackTrace();
            }
        };
        new AppDisponibilidadService(getActivity()).consultarInformacionLocalidadesCoordenadas(departamento, callback);
    }

    private void indexarListaDepartamentosSpinner(List<AppDisponibilidad> listaDepartamentos) {
        List<String> departamentos = new ArrayList<>();
        for (AppDisponibilidad disponible : listaDepartamentos) {
            if (disponible.getRegional() != null) {
                departamentos.add(disponible.getRegional());
            }
        }
        spDepartamento.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                departamentos
        ));
    }

    private void indexarListaLocalidadesSpinner(List<AppDisponibilidad> listaLocalidades) {
        List<String> localidades = new ArrayList<>();
        for (AppDisponibilidad disponible : listaLocalidades) {
            if (disponible.getLocalidad() != null) {
                localidades.add(disponible.getLocalidad());
            }
        }
        spCiudad.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                localidades
        ));
    }

    private void reDistribuirActividad() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Consulta por Direccion");
        home.getSupportActionBar().setSubtitle("Disponibilidad de Red");
    }
}
