package co.movistar.planeacioncomercial.negocio.fragmentos.disponibilidad;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;

/**
 * Created by alejandro.jimenez on 28/11/2016.
 */

public class FragmentoDisponibilidad extends Fragment {

    private CardView btnConsultaResumen;
    private CardView btnConsultaDetalleCaja;
    private CardView btnConsultaDireccionCaja;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_disponibilidad, container, false);
        initComponents(vista);
        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        reDistribuirActivity();
        asignarEventos();
    }

    private void initComponents(View vista) {
        this.btnConsultaDireccionCaja = (CardView) vista.findViewById(R.id.btnConsultaDireccionCaja);
        this.btnConsultaResumen = (CardView) vista.findViewById(R.id.btnConsultaResumen);
        this.btnConsultaDetalleCaja = (CardView) vista.findViewById(R.id.btnConsultaDetalleCaja);
    }

    private void asignarEventos() {
        View.OnClickListener eventoClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(), null, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
            }
        };
        btnConsultaResumen.setOnClickListener(eventoClick);
        btnConsultaDetalleCaja.setOnClickListener(eventoClick);
        btnConsultaDireccionCaja.setOnClickListener(eventoClick);
    }

    private void reDistribuirActivity() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getSupportActionBar().setTitle("Disponibilidad de Red");
        home.getSupportActionBar().setSubtitle("");
        home.getFabBotonFlotante().hide();
    }


}
