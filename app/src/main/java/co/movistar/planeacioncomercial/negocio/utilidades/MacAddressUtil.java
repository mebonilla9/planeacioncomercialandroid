package co.movistar.planeacioncomercial.negocio.utilidades;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/**
 * Created by lord_nightmare on 22/05/17.
 */

public final class MacAddressUtil {

    public static String getMacAddress() {
        String macAddress = "";
        try {
            // get all the interfaces
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            //find network interface wlan0
            for (NetworkInterface networkInterface : all) {
                if (!networkInterface.getName().equalsIgnoreCase("wlan0")) continue;
                //get the hardware address (MAC) of the interface
                byte[] macBytes = networkInterface.getHardwareAddress();
                if (macBytes == null) {
                    return macAddress;
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    //gets the last byte of b
                    if (Integer.toHexString(b & 0xFF).equals("0")) {
                        res1.append("0");
                    }
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                macAddress = res1.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return macAddress;
    }
}
