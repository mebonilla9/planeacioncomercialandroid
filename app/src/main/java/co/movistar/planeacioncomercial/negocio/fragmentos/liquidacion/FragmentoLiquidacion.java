package co.movistar.planeacioncomercial.negocio.fragmentos.liquidacion;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatDelegate;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import co.movistar.planeacioncomercial.R;
import co.movistar.planeacioncomercial.modelo.entidades.AppLiquidacionHist;
import co.movistar.planeacioncomercial.modelo.servicios.AppLiquidacionHistService;
import co.movistar.planeacioncomercial.negocio.actividades.HomeActivity;
import co.movistar.planeacioncomercial.negocio.utilidades.AlertaUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.FormularioUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.FragmentoUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.NullableUtil;
import co.movistar.planeacioncomercial.negocio.utilidades.ProgresoUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gloria.garzon on 15/02/2017.
 */

public class FragmentoLiquidacion extends Fragment {

    private ProgressDialog dialogo;

    private MaterialBetterSpinner spMes;

    private TextView txtPorcentajeIpEjecutado;
    private TextView txtPorcentajeTotalAceleraciones;
    private TextView txtValorVariable;
    private TextView txtValorGarantizadoNuevos;
    private TextView txtValorMes;
    private TextView txtValorPagar;
    private TextView txtPorcentajeIpLiquidar;
    private TextView txtPorcentajeCurva;
    private TextView txtValorGarantizado;
    private TextView txtValorAjusteMesesAnt;
    private FloatingActionButton fabActivity;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragmento_liquidacion, container, false);
        initComponents(vista);
        dialogo = ProgresoUtil.getDialogo(getActivity(), "Obteniendo Información", "Estamos procesando su información, espere...");
        cargarListas();
        habilitarCaracteristicas();
        return vista;
    }

    private void initComponents(View vista) {
        this.spMes = (MaterialBetterSpinner) vista.findViewById(R.id.spMes);
        this.spMes.setVisibility(View.GONE);
        this.txtValorAjusteMesesAnt = (TextView) vista.findViewById(R.id.txtValorAjusteMesesAnt);
        this.txtValorGarantizado = (TextView) vista.findViewById(R.id.txtValorGarantizado);
        this.txtPorcentajeCurva = (TextView) vista.findViewById(R.id.txtPorcentajeCurva);
        this.txtPorcentajeIpLiquidar = (TextView) vista.findViewById(R.id.txtPorcentajeIpLiquidar);
        this.txtValorPagar = (TextView) vista.findViewById(R.id.txtValorPagar);
        this.txtValorMes = (TextView) vista.findViewById(R.id.txtValorMes);
        this.txtValorGarantizadoNuevos = (TextView) vista.findViewById(R.id.txtValorGarantizadoNuevos);
        this.txtValorVariable = (TextView) vista.findViewById(R.id.txtValorVariable);
        this.txtPorcentajeTotalAceleraciones = (TextView) vista.findViewById(R.id.txtPorcentajeTotalAceleraciones);
        this.txtPorcentajeIpEjecutado = (TextView) vista.findViewById(R.id.txtPorcentajeIpEjecutado);
        this.fabActivity = ((HomeActivity) getActivity()).getFabBotonFlotante();
        habilitarCaracteristicas();
    }

    private void habilitarCaracteristicas() {
        HomeActivity home = (HomeActivity) getActivity();
        home.getFabBotonFlotante().show();
        home.getSupportActionBar().setTitle("Liquidación");
        home.getSupportActionBar().setSubtitle("Resumen información de liquidación");
        this.fabActivity.setImageDrawable(DrawableCompat.wrap(VectorDrawableCompat.create(getResources(), R.drawable.account_card_details, null)));
    }

    private void asignarEventos() {
        TextWatcher cambioSpinner = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                cargarResumenLiquidacion(charSequence.toString());
                habilitarBoton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        View.OnClickListener clickFiltroDetalle = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle argumentos = new Bundle();
                argumentos.putString("subfragmento", "liquidacionMenu");
                argumentos.putString("periodo", spMes.getText().toString());
                FragmentoUtil.obtenerFragmentoSeleccionado(view.getId(), argumentos, getActivity().getSupportFragmentManager().beginTransaction(), R.id.contenedorFragmentos);
            }
        };

        this.fabActivity.setOnClickListener(clickFiltroDetalle);
        this.fabActivity.setEnabled(true);

        this.spMes.addTextChangedListener(cambioSpinner);
    }

    private void cargarListas() {
        Callback<List<AppLiquidacionHist>> callbackMes = new Callback<List<AppLiquidacionHist>>() {
            @Override
            public void onResponse(Call<List<AppLiquidacionHist>> call, Response<List<AppLiquidacionHist>> response) {
                if (!response.isSuccessful() || response.body().isEmpty() || response.body().size() == 0) {
                    fabActivity.hide();
                    AlertaUtil.mostrarAlerta("Liquidación", "El usuario no posee información que visualizar para este módulo", null, null, getActivity());
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                List<AppLiquidacionHist> listaMeses = response.body();
                indexarListaMeses(listaMeses);
                spMes.setVisibility(View.VISIBLE);
                cargarResumenLiquidacion(listaMeses.get(0).getPeriodo());
            }

            @Override
            public void onFailure(Call<List<AppLiquidacionHist>> call, Throwable t) {

            }
        };

        new AppLiquidacionHistService(getActivity()).consultarPeriodos(callbackMes);
    }

    private void cargarResumenLiquidacion(String periodo) {
        Callback<AppLiquidacionHist> callback = new Callback<AppLiquidacionHist>() {
            @Override
            public void onResponse(Call<AppLiquidacionHist> call, Response<AppLiquidacionHist> response) {
                if (!response.isSuccessful()) {
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    return;
                }
                AppLiquidacionHist resumenLiquidacion = response.body();
                /*if (NullableUtil.validarObjetoNulo(resumenLiquidacion)) {
                    fabActivity.hide();
                    ProgresoUtil.ocultarDialogoProgreso(dialogo);
                    AlertaUtil.mostrarAlerta("Liquidación", "El usuario no posee información que visualizar para este modulo", null, null, getActivity());
                    return;
                }*/
                cargarInformacionResumenLiquidacion(resumenLiquidacion);
                ProgresoUtil.ocultarDialogoProgreso(dialogo);
            }

            @Override
            public void onFailure(Call<AppLiquidacionHist> call, Throwable t) {

            }
        };
        new AppLiquidacionHistService(getActivity()).obtenerResumenLiquidacion(periodo, callback);
    }

    private void indexarListaMeses(List<AppLiquidacionHist> listaMeses) {
        List<String> meses = new ArrayList<>();
        for (AppLiquidacionHist appLiquidacionHist : listaMeses) {
            if (appLiquidacionHist.getPeriodo() != null) {
                meses.add(appLiquidacionHist.getPeriodo());
            }
        }
        this.spMes.setAdapter(new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                meses
        ));
        this.spMes.setText(meses.get(0));
    }

    private void cargarInformacionResumenLiquidacion(AppLiquidacionHist resumenLiquidacion) {
        this.txtValorAjusteMesesAnt.setText(FormularioUtil.convertirFormatoMiles(resumenLiquidacion.getAjusteMesesAnteriores().toString()));
        this.txtValorGarantizado.setText(FormularioUtil.convertirFormatoMiles(resumenLiquidacion.getGarantizado().toString()));
        this.txtPorcentajeCurva.setText(FormularioUtil.convertirCumplimientoPorcentaje(resumenLiquidacion.getCurvaAcelerador()));
        this.txtPorcentajeIpLiquidar.setText(FormularioUtil.convertirFormatoMiles(resumenLiquidacion.getIpLiquidar().toString()));
        this.txtValorPagar.setText(FormularioUtil.convertirFormatoMiles(resumenLiquidacion.getValorAPagar().toString()));
        this.txtValorMes.setText(FormularioUtil.convertirFormatoMiles(resumenLiquidacion.getValorMes().toString()));
        this.txtValorGarantizadoNuevos.setText(FormularioUtil.convertirFormatoMiles(resumenLiquidacion.getGarantizadoNvosCanales().toString()));
        this.txtValorVariable.setText(FormularioUtil.convertirFormatoMiles(resumenLiquidacion.getVariable100().toString()));
        this.txtPorcentajeTotalAceleraciones.setText(FormularioUtil.convertirCumplimientoPorcentaje(resumenLiquidacion.getTotalAcel()));
        this.txtPorcentajeIpEjecutado.setText(FormularioUtil.convertirCumplimientoPorcentaje(resumenLiquidacion.getIpEjecucionSinAcel()));
    }


    @Override
    public void onResume() {
        super.onResume();
        asignarEventos();
        habilitarCaracteristicas();
    }

    private void habilitarBoton() {
        if (!this.spMes.getText().toString().equals("")) {
            this.fabActivity.setEnabled(true);
            return;
        }
        this.fabActivity.setEnabled(false);
    }
}
